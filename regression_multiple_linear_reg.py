# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 19:24:32 2017

@author: L.G
"""

import read_VTK_VTP_CLEAN as read
import os

import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

#%% Extract legs

#path = "/Users/stephaniedolbec/Google Drive/Polytechnique/4e annee/Automne/Projet_GBM4900/04_RESSOURCES/DATA/Archived_Data/Clean (1)/"
path = "C:/Users/L.G/Google Drive/Projet_GBM4900/04_RESSOURCES/DATA/Archived_Data/Clean (1)/"
files = os.listdir(path) 
# Sort files in a natural alphabetical and numerical way: 
# files.sort(key=basic_function.natural_keys)

legs = []
#TODO change according to new dataset 
for file_no, file in enumerate(files):
        if "stlcenterline" in file: #or ".aop." in file or ".obj." in file or ".ply." in file:
#            print(path + file)
            legs.append(read.readKnee(path + file))

#%% Extract knee axis and features to test

features_list_temp, features_list = [], []
CoM, ClosestPointToCOMZ, ClosestPointToAverageZ, MaxX, MinX, DerivativeX, \
DerivativeY, DerivativeZ, AverageZ , Test_X= [], [], [], [], [], [], [], [], [],[]
MinRadiusAboveCOM, MinR_between_COMs = [], []
knee_axis = [] 

legs = read.getKneeAxis(legs)

for i in range(len(legs)):
    CoM.append(legs[i].CenterOfMass)
        
    ClosestPointToCOMZ.append(legs[i].ClosestPointToCOMZ)
    
    ClosestPointToAverageZ.append(legs[i].ClosestPointToAverageZ)   
   
    MaxX.append(legs[i].MaxX[1])
    MinX.append(legs[i].MinX[1])
    
    DerivativeX.append(legs[i].getMaximum(legs[i].DerivativeX[3]))
    DerivativeY.append(legs[i].getMaximum(legs[i].DerivativeY[3]))
    DerivativeZ.append(legs[i].getMaximum(legs[i].DerivativeZ[3]))
#    Test_X.append(legs[i].MaxXTest[1])
    
    AverageZ.append(legs[i].AverageZ)
     
    # Minimal radius above the center of mass
    MinRadiusAboveCOM.append(legs[i].getMinimum(legs[i].Radius[legs[i].ClosestPointToCOMZ:])[1])
    
    # Minimal radius between 2 COMs
    closest_pt_belowCOM = legs[i].getClosestPoint(legs[i].Points,legs[i].COMBelowZ,3)
    closest_pt_aboveCOM = legs[i].getClosestPoint(legs[i].Points,legs[i].COMAboveZ,3)
    rayon = legs[i].Radius[closest_pt_belowCOM:closest_pt_aboveCOM]
    MinR_between_COMs.append(legs[i].getMinimum(legs[i].Radius[closest_pt_belowCOM:closest_pt_aboveCOM])[1])

    # Knee axis
    knee_axis.append(legs[i].PtKneeAxis)
    
#features_list_temp.append(CoM)
features_list_temp.append(ClosestPointToCOMZ)
features_list_temp.append(ClosestPointToAverageZ)
#features_list_temp.append(MaxX)
#features_list_temp.append(MinX)
#features_list_temp.append(AverageZ)
#features_list_temp.append(MinRadiusAboveCOM)
features_list_temp.append(MinR_between_COMs)
#features_list_temp.append(Test_X)
features_array_temp = np.array(features_list_temp)
features_array_flipped = np.transpose(features_array_temp)
X = features_array_flipped
#%% Regression to find knee axis
        
# Percentage of data to validate
p_test = 10 # %

n_test = int((p_test/100.0) *264)
variance = []
  # Split the data into training/testing sets
X_train = X[:-n_test]
X_test = X[-n_test:]
    
# Split the targets into training/testing sets
y_train = knee_axis[:-n_test]
y_test = knee_axis[-n_test:]
    
# Create linear regression object
regr = linear_model.LinearRegression()
#    regr = linear_model.BayesianRidge()
#    regr = linear_model.HuberRegressor()	
    
# Train the model using the training sets
regr.fit(X_train, y_train)
    
# Make predictions using the testing set
y_pred = regr.predict(X_test)
y_pred = [int(y_pred[i]) for i in range(len(y_pred))] # ID is an integer

# The coefficients
print('Coefficients: \n', regr.coef_)
# The mean squared error
print("Mean squared error: %.2f"
      % mean_squared_error(y_test,y_pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % r2_score(y_test,y_pred))

variance.append(r2_score(y_test, y_pred))
    
plt.scatter(y_test, y_pred)
plt.title('y_pred vs y_test')
plt.xlabel('y_test')
plt.ylabel('y_pred')
plt.show()
