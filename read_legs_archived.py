# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 22:14:00 2017

@author: Jonathan
"""

import math
import numpy as np
import os
from importlib import reload
# The module we created
import basic_functions
# Reload to always use the last file saved
reload(basic_functions)

# BASIC FUNCTION
from basic_functions import Dist_point_line_3D, splitXY                      


#%% this class is a knee, it contains ID, POINTS (Coordinates), TURTOISITY, RADIUS, CURVUTURE, TANGENT
# NORMAL, BINORMAL, TORSION, Length, Type
class Knee:
    def __init__(self, iD, numPoints, points,length,tortuosity,radius,curvature,torsion,normal,tangent,binormal,kneeType,fileName,filePath):
        self.ID=iD
        self.NumPoints=numPoints
        self.Points = points
        self.Length = length
        self.Tortuosity= tortuosity
        self.Radius= radius
        self.Curvature = curvature
        self.Torsion = torsion
        self.Tangent = tangent
        self.Normal= normal
        self.Binormal = binormal
        self.KneeType=kneeType
        self.FileName=fileName
        self.FilePath=filePath
        
        #Relative to Knee point and interval of 5 cm and knee axis
        self.closest_pt_dist = 0
        self.closest_pt_id = 0
        self.closest_pt_intervall = []
        self.axis = Knee_axis()
        
        self.MinX=self.getMinimum(self.Points[:,0])
        self.MinY=self.getMinimum(self.Points[:,1])
        self.MaxX=self.getMaximum(self.Points[:,0])
        self.MaxY=self.getMaximum(self.Points[:,1])
        
        self.MinRadius=self.getMinimum(self.Radius)
        self.MinCurvature=self.getMinimum(self.Curvature)
        self.MinTorsion=self.getMinimum(self.Torsion)
        
        self.MaxRadius=self.getMaximum(self.Radius)
        self.MaxCurvature=self.getMaximum(self.Curvature)
        self.MaxTorsion=self.getMaximum(self.Torsion)
        
        self.CenterOfMass=self.getCenterOfMass(self.Radius,self.Points,self.NumPoints)
        
        self.DerivativeRadius=self.derivative(self.Radius)
        self.DerivativeCurvature=self.derivative(self.Curvature)
        self.DerivativeTorsion=self.derivative(self.Torsion)
        
        self.DerivativeX=self.derivative(self.Points[:,0])
        self.DerivativeY=self.derivative(self.Points[:,1])
        self.DerivativeZ=self.derivative(self.Points[:,2])
        
        self.AverageX=self.getAverage(self.Points[:,0])
        self.AverageY=self.getAverage(self.Points[:,1])
        self.AverageZ=self.getAverage(self.Points[:,2])
        
        self.ClosestPointToAverageZ=self.getClosestPoint(self.Points[:,2],self.AverageZ,1)
        self.ClosestPointToCOMZ=self.getClosestPoint(self.Points[:,2],self.CenterOfMass[2],1)
        
        self.AvXBelowAvZ=self.getAverage(self.Points[:self.ClosestPointToAverageZ,0])
        self.AvYBelowAvZ=self.getAverage(self.Points[:self.ClosestPointToAverageZ,1])
        
        self.AvXAboveAvZ=self.getAverage(self.Points[self.ClosestPointToAverageZ:,0])
        self.AvYAboveAvZ=self.getAverage(self.Points[self.ClosestPointToAverageZ:,1])
        
        self.COMBelowZ=self.getCenterOfMass(self.Radius[:self.ClosestPointToCOMZ],self.Points[:self.ClosestPointToCOMZ,:],len(self.Radius[:self.ClosestPointToCOMZ]))
        self.COMAboveZ=self.getCenterOfMass(self.Radius[self.ClosestPointToCOMZ:],self.Points[self.ClosestPointToCOMZ:,:],len(self.Radius[self.ClosestPointToCOMZ:]))

    def getMinimum(self, array):
        return (np.min(array),np.argmin(array))
    
    def getMaximum(self,array):
        return (np.max(array),np.argmax(array))
    
    def getCenterOfMass(self, radius, points, numPoints):
        sumR = np.sum(radius)
        cm_x = np.sum(np.reshape(points[:,0],(numPoints,1))*radius) / sumR
        cm_y = np.sum(np.reshape(points[:,1],(numPoints,1))*radius) / sumR
        cm_z = np.sum(np.reshape(points[:,2],(numPoints,1))*radius) / sumR
    
        return (cm_x, cm_y, cm_z)
    
    #def """ MAX DERIVATIVE(S) """
    def derivative(self, array):
        delta_radius, norm_points = [], []
    
        [norm_points.append(np.linalg.norm(self.Points[i+1]-self.Points[i]))\
         for i in range(self.NumPoints-1)]
    
        # calcul des dérivées discrètes pour chaque rayon d'une jambe
        [delta_radius.append((array[i+1]-array[i])/norm_points[i])\
         for i in range(self.NumPoints-1)]

        # dérivées maximale et minimale et leur position
        min_deriv = np.min(delta_radius)
        idx_min_deriv = np.argmin(delta_radius)
    
        max_deriv = np.max(delta_radius)
        idx_max_deriv =np.argmax(delta_radius)
    
        return min_deriv, idx_min_deriv, max_deriv, idx_max_deriv
    
    def getClosestPoint(self,array,point,dim):
        if dim==1:
            index=np.argmin(((array-point)**2)**(1/2.0))
        elif dim>=1:
            index=np.argmin((np.sum((array-point)**2,axis=1))**(1/2.0))          
        return (index)
    
    def getAverage(self,array):
        return np.average(array)

    def getTensor(self):
        knee_tensor = np.array([])
        max_x = -12000
        min_x = 2000
        
        max_y = -12000
        min_y = 2000
        
        max_z = -12000
        min_z = 2000
        
        max_tx = -12000
        min_tx = 2000
        
        max_ty = -12000
        min_ty = 2000
        
        max_tz = -12000
        min_tz = 2000
        
        max_Nx = -12000
        min_Nx = 2000
        
        max_Ny = -12000
        min_Ny = 2000
        
        max_Nz = -12000
        min_Nz = 2000
        
        max_Bx = -12000
        min_Bx = 2000
        
        max_By = -12000
        min_By = 2000
        
        max_Bz = -12000
        min_Bz = 2000
        for i in range(len(self.Points)):            
            if max_x < self.Points[i][0]:
                max_x = self.Points[i][0]
            if max_y < self.Points[i][1]:
                max_y = self.Points[i][1]
            if max_z < self.Points[i][2]:
                max_z = self.Points[i][2]
            if max_tx < self.Tangent[i][0]:
                max_tx = self.Tangent[i][0]
            if max_ty < self.Tangent[i][1]:
                max_ty = self.Tangent[i][1]
            if max_tz < self.Tangent[i][2]:
                max_tz = self.Tangent[i][2]
            if max_Nx < self.Normal[i][0]:
                max_Nx = self.Normal[i][0]
            if max_Ny < self.Normal[i][1]:
                max_Ny = self.Normal[i][1]
            if max_Nz < self.Normal[i][2]:
                max_Nz = self.Normal[i][2]
            if max_Bx < self.Binormal[i][0]:
                max_Bx = self.Binormal[i][0]
            if max_By < self.Binormal[i][1]:
                max_By = self.Binormal[i][1]
            if max_Bz < self.Binormal[i][2]:
                max_Bz = self.Binormal[i][2]
                
            if min_x > self.Points[i][0]:
                min_x = self.Points[i][0]
            if min_y > self.Points[i][1]:
                min_y = self.Points[i][1]
            if min_z > self.Points[i][2]:
                min_z = self.Points[i][2]
            if min_tx > self.Tangent[i][0]:
                min_tx = self.Tangent[i][0]
            if min_ty > self.Tangent[i][1]:
                min_ty = self.Tangent[i][1]
            if min_tz > self.Tangent[i][2]:
                min_tz = self.Tangent[i][2]
            if min_Nx > self.Normal[i][0]:
                min_Nx = self.Normal[i][0]
            if min_Ny > self.Normal[i][1]:
                min_Ny = self.Normal[i][1]
            if min_Nz > self.Normal[i][2]:
                min_Nz = self.Normal[i][2]
            if min_Bx > self.Binormal[i][0]:
                min_Bx = self.Binormal[i][0]
            if min_By > self.Binormal[i][1]:
                min_By = self.Binormal[i][1]
            if min_Bz > self.Binormal[i][2]:
                min_Bz = self.Binormal[i][2]
                
        for i in range(len(self.Points)):
            self.Points[i][0] = ((2*(self.Points[i][0] - min_x)) / (max_x - min_x))-1
            self.Points[i][1] = ((2*(self.Points[i][1] - min_y)) / (max_y - min_y))-1
            self.Points[i][2] = ((2*(self.Points[i][2] - min_z)) / (max_z - min_z))-1
            
            self.Normal[i][0] = ((2*(self.Normal[i][0] - min_Nx)) / (max_Nx - min_Nx))-1
            self.Normal[i][1] = ((2*(self.Normal[i][1] - min_Ny)) / (max_Ny - min_Ny))-1
            self.Normal[i][2] = ((2*(self.Normal[i][2] - min_Nz)) / (max_Nz - min_Nz))-1
            
            self.Tangent[i][0] = ((2*(self.Tangent[i][0] - min_tx)) / (max_tx - min_tx))-1
            self.Tangent[i][1] = ((2*(self.Tangent[i][1] - min_ty)) / (max_ty - min_ty))-1
            self.Tangent[i][2] = ((2*(self.Tangent[i][2] - min_tz)) / (max_tz - min_tz))-1
            
            self.Binormal[i][0] = ((2*(self.Binormal[i][0] - min_Bx)) / (max_Bx - min_Bx))-1
            self.Binormal[i][1] = ((2*(self.Binormal[i][1] - min_By)) / (max_By - min_By))-1
            self.Binormal[i][2] = ((2*(self.Binormal[i][2] - min_Bz)) / (max_Bz - min_Bz))-1
        self.Radius = ((2*(self.Radius - min(self.Radius))) / (max(self.Radius) - min(self.Radius)))-1
        self.Curvature = ((2*(self.Curvature - min(self.Curvature))) / (max(self.Curvature) - min(self.Curvature)))-1
        self.Radius = ((2*(self.Torsion - min(self.Torsion))) / (max(self.Torsion) - min(self.Torsion)))-1
       
        
        knee_tensor = np.concatenate((self.Points, self.Radius, self.Curvature, 
                                     self.Torsion, self.Normal, self.Tangent, 
                                      self.Binormal), axis=1)
        return knee_tensor
    
    def find_closest_pt(self):
        """Find the point of the leg axis which is the closest to the knee axis (FOR DATA_1 only)"""
        for pt_id, pt in enumerate(self.Points):
            dist = basic_functions.Dist_point_line_3D(pt=pt, start=self.axis.POINTS[0],
                                                     end=self.axis.POINTS[1], ite=10)
            if pt_id == 0: # assign a closest pt distance one the first loop
                self.closest_pt_dist = dist
            elif dist < self.closest_pt_dist: #  TODO verifier cette condition: and pt_id == 0:
                self.closest_pt_dist = dist
                self.closest_pt_id = pt_id
    
    def find_closest_pt_intervall(self):
        """Find the values that are included in the acceptable 1cm intervall"""
        # Therefore 0.5cm up and 0.5cm down
        self.closest_pt_intervall = [self.closest_pt_id]
        if self.closest_pt_id != 0:    #TODO verifier pourquoi un des points == 0    # est-ce que c'est seulement pour les jambes de type_1 et non pour celles de type_2
            #UP
            dist_up = 0
            closest_up = self.closest_pt_id
            while (dist_up <= 5):
                pt = self.Points[closest_up]
                closest_up += 1
                next_pt = self.Points[closest_up]
                dist_up += np.linalg.norm(np.array(pt) - np.array(next_pt))
                if dist_up <= 5: 
                    self.closest_pt_intervall.append(closest_up)
            #DOWN
            dist_down = 0
            closest_down = self.closest_pt_id
            while (dist_down <= 5):
                pt = self.Points[closest_down]
                closest_down -= 1
                next_pt = self.Points[closest_down]
                dist_down += np.linalg.norm(np.array(pt) - np.array(next_pt))
                if dist_up <= 5: 
                    self.closest_pt_intervall.append(closest_down)   
                    
    def get_one_hot(self, lower, upper):
        j = 0 
        onehot = np.zeros(upper-lower)
        for i in range(lower,upper):
            if j < len(self.closest_pt_intervall) and i == self.closest_pt_intervall[j] :
                onehot[i - lower] = 1
                j = j+1
        return onehot
            
                    
#%%
# Knee axis class from Alex' read function
class Knee_axis():     # inheritance of knee functions   (+ changer le nom pour classe ie   KneeAxis())
    def __init__(self):
        
        self.POINTS = []
        self.Angle = []
        self.Offset = []
        self.Normal = []
        self.Binormal = []
        self.side = ''
        self.filePath = ''
        self.caract_list = ['Angle', 'Offset', 'Normal', 'Binormal', 'POINTS']
        self.patient_no = ''
        
        
    def fill_data(self, file, base_path):
        """Extract the all the data in a single array that contain the caracteristics for all the files"""
        file_path = base_path + '/' + file
        
        self.patient_no = file.split('.')[0]
        if 'D' in self.patient_no:
            self.fill_caract_list(file_path)
            self.side = 'D'
        if 'G' in self.patient_no:
            self.fill_caract_list(file_path)
            self.side = 'G'
        print("knee_axis: ", file)
        
        
    def fill_caract_list(self, file_path):      # TODO !!!!!!! implémenter de l'inhéritance ou de la composition en fonction de la relation entre knee axis et knee, car cette technique en passant un objet est weird.... 
        """Fill the list of caracteristics for one file""" # TODO: car il s'agit d'un duplicat de la fonction qui est présente dans la classe Knee() 
        point_added = 0
        point_to_add = 0
        category_no = 0
        add_value = False
    
        with open(file_path) as knee_data_file:
            for line in knee_data_file:  # Read the vtk files line by line
                # stop at the end of the category we specified
                if category_no == len(self.caract_list): 
                    break
                
                line_split = line.split()
                if line_split == []:    # Go to next loop if paragraph jump
                    continue
        
                if line_split[0] == self.caract_list[category_no]:
                    if self.caract_list[category_no] == 'POINTS':
                        group_size = 3
                        point_to_add = float(line_split[1]) * group_size   # because by group of 3 coordinate
                    else: 
                        group_size = int(line_split[1])
                        point_to_add = float(line_split[2]) * group_size
        
                    point_added = 0
                    add_value = True
                    continue  
                
                for number in line.split():
                    if add_value:
                        getattr(self, self.caract_list[category_no]).append(float(number))
                        point_added += 1
                        # Add values until all value are added based on the title tag of the category
                        if point_added == point_to_add: 
                            add_value = False
                            # Split the data for each category in the specified format
                            if group_size > 1: 
                                data = getattr(self, self.caract_list[category_no])
                                
                                setattr(self, self.caract_list[category_no],
                                        [data[pos:pos+group_size] for pos in range(0, len(data), group_size)])
                            
                            category_no += 1  # Go to the next category
    
        self.filePath = file_path   # Add the name of the file_path at the end  (TODO: le faire a l'extérieur dans le main si possible)
        
    
#%%
# function that takes a file, opens and reads and populates the object knee

def readKnee(file):
    # VARIABLES
    # file is the name of filename
    # knee is an object from class Knee that will be created and returned
    #----------------------------------------------------------------------------------------------------------
    # PARAMETERS EXTRACTED AND PASSED ON IN OBJECT KNEE
    iD=''
    points=[]
    numPoints=[]
    length=[]
    tortuosity=[]
    radius=[]
    curvature=[]
    torsion=[]
    tangent=[]
    normal=[]
    binormal=[]
    kneeType=''
    fileName=''
    filePath=''
    #--------------------------------------------------------------------------------------------------------
    #Lets find out of this file contains a left knee or right knee
    
    #First lets determine if this file has a path of from a Mac (/) or Windows (\)
    # we will use rfind function which allows to look for specific sub_string from the end of the string
    #string.rfind(string2) will return index of start of string 2 if found, and -1 if it did not found
    if file.rfind("\\")!=-1:
        #it is a windows computer, lets find the path and name
        filePath=file[:file.rfind("\\")+1]
        fileName=file[file.rfind("\\")+1:]
        
        
        #Lets determine if this file is Left or Right, we will just compare the charachter after the last \ to D, 
        # or G, D=right G=left 
        if file[file.rfind("\\")+1]=="D" or file[file.rfind("\\")+1]=="R":
            # it is a right Leg, we write type as right
            kneeType='R'
        else:
            #it is a left leg, we write type as left
            kneeType='L'
            
    elif file.rfind("/")!=-1:
        #it is a mac computer, lets find the path and name
        filePath=file[:file.rfind("/")+1]
        fileName=file[file.rfind("/")+1:]
        
        #Lets determine if this file is Left or Right, we will just compare the charachter after the last / to D, 
        # or G,D=right G=left
        if file[file.rfind("/")+1]=="D" or file[file.rfind("/")+1]=="R":
            # it is a right Leg, we write the type as right
            kneeType='R'
        else:
            #it is a left leg, we write the type as left
            kneeType='L'    
    else:
        #We don't know which computer, but we now know the filename is in the same folder as this script
        fileName=file
        
        #Lets determine if this file is Left or Right, we will just compare the first character to D or G 
        # D=right G=left
        if file[0]=="D" or  file[0]=="R":
            # it is a right Leg, we write the type as right
            kneeType='R'
        else:
            #it is a left leg, we write the type as Left
            kneeType='L'


    #lets just find the id, we will use the fileName to do so
    iD=fileName[:fileName.find('.')]
    
    #-----------------------------------------------------------------------------------------------------------
    #Lets determine if this file is a VTK or VTP or TIFF, this will affect the way we read thefile and write the new one
    #we will use rfind function, do determine if the file is a .VTK or .VTP
    if file.find(".vtk")!=-1:
        #-----------------------------------------------------------------------------------------------------------
        # We open the old file in read mode and the new file in write mode
        fKnee=open(file,"r")
        #-----------------------------------------VTK FILE------------------------------------------------------
        #----------------------------PATCH FOR WHEN POINTS ISN'T FIRST ARRAY---------------------------------
        # we will read the old file and look for the points, then close it and re open it
        for line in fKnee:
            if not line.find("POINTS")==-1:
                firstSpace=line.find(" ")
                secondSpace=line.find(" ",firstSpace+1)
                numPoints=int(line[firstSpace+1:secondSpace])
                #read rest of file
                fKnee.read()
        #close file
        fKnee.close()
        # open file for next step
        fKnee=open(file,"r")
                  
        
        #we will read line by line untill end of file. we will be looking for specific substrings
        # the substring we will look for are "POINTS" , "length", "turtuosity", "Torsion", "Tangent","Normal","Binormal",
        #'radius', curvurture

        #we will use for line in fOld to read all the lines
        for line in fKnee:
            #------------------------COORDINATES, TANGENT, NORMAL, BINORMAL--------------------------------------------------------------
            if not line.find("POINTS")==-1 or not line.find("Tangent")==-1 or not line.find("Normal")==-1 or not line.find("Binormal")==-1:
                #this is the array format has 3 sets of points per line
                #lets save the line to identify the variable later
                tempLine=line
                tempData=[]
                for x in range(math.ceil(numPoints/3)):
                    tempStr=fKnee.readline()
                    #we put the add the line or points, we split when there are spaces, -2 because it spaces at
                    #end of line
                    tempData=tempData+[float(k) for k in tempStr[:-2].split(' ')]
            
                # we will reshape it now that we have all points (numPoints)*3
                # we need to identify which variable to assign it to
                
                if not tempLine.find("POINTS")==-1:
                    points=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Tangent")==-1:
                    tangent=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Normal")==-1:
                    normal=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Binormal")==-1:
                    binormal=np.reshape(tempData,(numPoints,3))
            
            #-------------------LENGTH--------------------------------------------------------------
            elif not line.find("Length")==-1:
                # we need to read the next line
                tempStr=fKnee.readline()
                #we add it to the variable
                length=float(tempStr[:-2])
            
            #----------------------TORTUOSITY--------------------------------------------------------------
            elif not line.find("Tortuosity")==-1:
                #we need to read the next line
                tempStr=fKnee.readline()
                #we add it to the variable
                tortuosity=float(tempStr[:-2])
                
            #----------------------RADIUS, CURVATURE------------------------------------------------------------------
            elif not line.find("Radius")==-1 or not line.find("Curvature")==-1 or not line.find("Torsion")==-1:
                #this array format has 9 points per line
                #lets save the line to identify the variable later
                tempLine=line
                tempData=[]
                for x in range(math.ceil(numPoints/9)):
                   tempStr=fKnee.readline()
                   #we put the add the line of points, we split when there are spaces, -2 because it spaces at
                   #end of line
                   tempData=tempData+[float(k) for k in tempStr[:-2].split(' ')]
                #we place in the variable
                if not tempLine.find("Radius")==-1:
                    radius=np.reshape(tempData,(numPoints,1))
                elif not tempLine.find("Curvature")==-1:
                    curvature=np.reshape(tempData,(numPoints,1))
                elif not tempLine.find("Torsion")==-1:
                    torsion=np.reshape(tempData,(numPoints,1))
            
           
                
        #We close the files because we are done
        fKnee.close()

    #---------------------------------------------------------------------------------------------------------    
    elif file.find(".vtp")!=-1:
        #---------------------------VTP FILE------------------------------------------------------------------
        #-----------------------------------------------------------------------------------------------------------
        # We open the old file in read mode 
        fKnee=open(file,"r")
        
        #----------------------------PATCH FOR WHEN POINTS ISN'T FIRST ARRAY---------------------------------
        # we will read the old file and look for the points, then close it and re open it
        for line in fKnee:
            if not line.find("NumberOfPoints")==-1:
                firstQuotation=line.find('"')
                secondQuotation=line.find('"',firstQuotation+1)
                numPoints=int(line[firstQuotation+1:secondQuotation])
                #read rest of file
                fKnee.read()
        #close file
        fKnee.close()
        # open file for next step
        fKnee=open(file,"r")
        
        
        
        #we will read line by line untill end of file. we will be looking for specific substrings
        # the substring we will look for are "POINTS" , "length", "turtuosity", "Torsion", "Tangent","Normal","Binormal",
        #'radius', curvurture

        for line in fKnee:
            #-----------------POINTS, TANGENT, NORMAL, BINORMAL----------------------------------------------
            if not line.find('Name="Points')==-1 or not line.find("Tangent")==-1 or not line.find("Normal")==-1 or not line.find("Binormal")==-1:
                #this is the array format has 2 sets of points per line
                #lets save the line to identify the variable later
                tempLine=line
                tempData=[]
                for x in range(math.ceil(numPoints/2)):
                    tempStr=fKnee.readline()
                    #we put the add the line or points, we split when there are spaces, -2 because it spaces at
                    #end of line
                    tempData=tempData+[float(k) for k in tempStr[10:].split(' ')]
            
                # we will reshape it now that we have all points (numPoints)*3
                # we need to identify which variable to assign it to
                if not tempLine.find('Name="Points')==-1:
                    points=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Tangent")==-1:
                    tangent=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Normal")==-1:
                    normal=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Binormal")==-1:
                    binormal=np.reshape(tempData,(numPoints,3))
            #-------------------LENGTH--------------------------------------------------------------
            elif not line.find("Length")==-1:
                # we need to read the next line
                tempStr=fKnee.readline()
                #we add it to the variable
                length=float(tempStr[10:])
            #----------------------TORTUOSITY--------------------------------------------------------------
            elif not line.find("Tortuosity")==-1:
                #we need to read the next line
                tempStr=fKnee.readline()
                #we add it to the variable
                tortuosity=float(tempStr[10:])
                
            #----------------------RADIUS, CURVATURE------------------------------------------------------------------
            elif not line.find("Radius")==-1 or not line.find("Curvature")==-1 or not line.find("Torsion")==-1:
                #this array format has 6 points per line
                #lets save the line to identify the variable later
                tempLine=line
                tempData=[]
                for x in range(math.ceil(numPoints/6)):
                   tempStr=fKnee.readline()
                   #we put the add the line of points, we split when there are spaces, -2 because it spaces at
                   #end of line
                   tempData=tempData+[float(k) for k in tempStr[10:].split(' ')]
                #we place in the variable
                if not tempLine.find("Radius")==-1:
                    radius=np.reshape(tempData,(numPoints,1))
                elif not tempLine.find("Curvature")==-1:
                    curvature=np.reshape(tempData,(numPoints,1))
                elif not tempLine.find("Torsion")==-1:
                    torsion=np.reshape(tempData,(numPoints,1))
            
           
                
        #We close the files because we are done
        fKnee.close()
    if points[0,2]-points[numPoints-1,2]<0:
        #we flip the data
        points=np.flip(points,0)
        radius=np.flip(radius,0)
        curvature=np.flip(curvature,0)
        torsion=np.flip(torsion,0)
        normal=np.flip(normal,0)
        tangent=np.flip(tangent,0)
        binormal=np.flip(binormal,0)
    # we return the knee that we will create
    return Knee(iD, numPoints, points,length,tortuosity,radius,curvature,torsion,normal,tangent,binormal,kneeType,fileName,filePath)
