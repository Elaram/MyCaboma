import numpy as np

def normalize_minus_one_to_one(array):
    return 2*((array-array.min()) / (array.ptp()) - 0.5)


def normalize_zero_to_one(
        array,
        min_bound:float=0,
        max_bound:float=1):
    """
    Normalize a numpy array to zero one value and get rid of extremes values if
    specified
    """
    # Unit test to make sure boundary are within 0 and 1
    if  not (0 <= min_bound <= 1) or not (0 <= max_bound <= 1):
        raise Exception('min of max boundary out of bounds [0, 1]')
    if min_bound >= max_bound:
        raise Exception('min bound greater than max bound')

    array_min = array.min()
    delta_array = array.ptp()
    # Get rid of the min an max extreme value by bounding array to a new min and max
    new_min = array_min + (min_bound*delta_array)
    array[array < new_min] = new_min
    new_max = array_min + (max_bound*delta_array)
    array[array > new_max] = new_max

    # Return the normalization between 0 and 1
    return (array-array.min()) / (array.max()-array.min())

#def normalize_zero_to_one(array):
#    return (array-array.min()) / (array.max()-array.min())



"Calcul the distance between a 3D line (knee_axis) and a point (points on the leg axis)"
def Dist_point_line_3D(pt, start, end, ite):
    # Convert to array
    pt = np.array(pt)
    start = np.array(start)
    end = np.array(end)

    vect_line = end - start
    vect_line = vect_line/ite

    for i in range(ite + 1):
        pt_on_line = start + i*vect_line
        # Distance between two points
        dist = np.linalg.norm(pt_on_line-pt)
        # Initialize the minimum distance
        if i == 0:
            min_dist = dist
        elif dist < min_dist:
            min_dist = dist
            #print(min_dist)
    return min_dist

def find_dist_3D_btw_two_pts(pt1, pt2):
    """
    Return the 3D distance between pt1 and pt2

    Arguments:
    ----------
    pt1, pt2 : np.array([x, y, z])

    Return:
    ------
    A float representing the distance between the two points in arguments
    """
    return np.linalg.norm(pt2 - pt1)

def find_CL_id_closest_to_random_3D_pt(center_line, rand_pt):
    """
    Find the id of the closest pt on the center line to a random point 3D

    Arguments
    ---------
    center_line : np.array(np.array([x,y,z]), ... np.array([x_n,y_n,z_n]))
        All the pts on the center line
    rand_pt : np.array([x, y, z])
        A random point in the 3D plane
    Return
    ------
    min_dist_id : int
        The ID on the centerline that is closest to the random point
    min_dist : float
        The distance from the closest point on the centerline and the random point
    """
    # Evaluate the min dist for the first pt in the array to have a comparative value
    min_dist = find_dist_3D_btw_two_pts(center_line[0], rand_pt)
    min_dist_id = 0
    # for all the pts in the center line
    for pt_no, pt in enumerate(center_line[1:]):
        dist_temp = find_dist_3D_btw_two_pts(pt, rand_pt)
        # look if there is a  pt with a shortest dist to the random pt
        if dist_temp < min_dist:
            # Keep track of its dist and id
            min_dist = dist_temp
            min_dist_id = pt_no

    return min_dist_id, min_dist



def splitXY(x, xOuY):
    '''Split the knee matrices and there relative one hot array for the learning process
        # Parameters: (x, xOuY) -
        # Returns: (list-xSplit) -
    '''
    xSplit = [x[i][xOuY] for i in range(len(x))]
    return xSplit
    # Note: preferable d'utiliser: np_utils.to_categorical


# From: unutbu
# Source: https://stackoverflow.com/questions/5967500/how-to-correctly-sort-a-string-with-a-number-inside/5967539#5967539
import re
def atof(text):
    try:
        retval = float(text)
    except ValueError:
        retval = text
    return retval


def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    float regex comes from https://stackoverflow.com/a/12643073/190597
    '''
    return [ atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text) ]


import os
import shutil
def transferFiles(directories, final_directory, bool_Right, bool_Left):
    if not os.path.exists(final_directory):
        os.mkdir(final_directory)

    for i in range(len(directories)):
        files=os.listdir(directories[i])
        for j, file in enumerate(files):
            if bool_Right:
                if file[0]=='D' or file[0]=='R':
                    shutil.copy(directories[i]+file, final_directory)
            elif bool_Left:
                if file[0]=='G' or file[0]=='L':
                    shutil.copy(directories[i]+file, final_directory)
