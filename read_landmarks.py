#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 19:55:35 2017

@author: stephaniedolbec
"""

import os
import pickle

def load_obj(name):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)


path = '/Users/stephaniedolbec/Desktop/Resources/'
scanname = path + "100L.stl" #TODO : boucle de lecture pour tous les fichiers

filename, extension = os.path.splitext(scanname)


if len(scanname) != 0 : 
    FileDestination_ = filename + "_landmarks"

f = load_obj(FileDestination_)
print(f)

LandmarkKneeLateral = f.get("LandmarkKneeLateral")
LandmarkKneeMedial = f.get("LandmarkKneeMedial")
print(LandmarkKneeLateral, LandmarkKneeMedial)