# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 20:44:15 2017

@author: Alexandre Marcotte

NOTE:
    The code regression_scikit_learn.py needs to be run first to have
    the plane of the prediction positionned at the right place in the
    3D volumes
"""

# TODO: (ALEX) add docstring to document each function



# Our implemented functions and classes
from importlib import reload
import read_legs
import basic_functions
reload(read_legs)
reload(basic_functions)
from basic_functions import find_CL_id_closest_to_random_3D_pt
from basic_functions import find_dist_3D_btw_two_pts
# General python modules
import numpy as np
from collections import namedtuple
import vtk

#%% Read all legs


# UNCOMMENT TO READ ALL THE LEGS:

path = r'C:\Users\alexa\Documents\CODING_A2017_POLY\Projet_4\Poly-AI\dataset_AI/'
legs, legs_axis = read_legs.read_training_data(path=path)

#%% CONSTANT
COLOR = namedtuple('Color', ['RED', 'BLUE', 'GREEN'])
RED = COLOR(1,0,0)
GREEN = COLOR(0,1,0)
BLUE = COLOR(0,0,1)
WHITE = COLOR(1,1,1)

#%% Function for creation of the different part of the visualisation
def create_leg(
        knee,
        pt_no,
        x,
        y,
        z,
        normalized_radius):
	 # source
    leg_source = vtk.vtkSphereSource()
    leg_source.SetPhiResolution(40)
    leg_source.SetThetaResolution(40)
    leg_source.SetCenter(x, y, z)
    leg_source.SetRadius(knee.Radius[pt_no])
    # mapper
    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(leg_source.GetOutput())
    else:
        mapper.SetInputConnection(leg_source.GetOutputPort())
    # actor
    leg_actor = vtk.vtkActor()
    leg_actor.SetMapper(mapper)
    c = normalized_radius[pt_no]
    leg_actor.GetProperty().SetColor(c,c,c)
    # assign actor to the renderer
    return leg_actor

def create_CL(
        pt_no,
        x,
        y,
        z,
        CL_boundary,
        id_in_centerline_to_show,
        center_line_shift):
    # create source
    CL_source = vtk.vtkSphereSource()
    CL_source.SetPhiResolution(3)
    CL_source.SetThetaResolution(3)
    CL_source.SetCenter(x, y+center_line_shift, z)
    # Highlight centerline point if specified
    if pt_no in id_in_centerline_to_show:
        CL_source.SetRadius(4)
    else:
        CL_source.SetRadius(0.9)
    # mapper
    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(CL_source.GetOutput())
    else:
        mapper.SetInputConnection(CL_source.GetOutputPort())
    # actor
    CL_actor = vtk.vtkActor()
    CL_actor.SetMapper(mapper)

    if CL_boundary.lower <= pt_no < CL_boundary.upper:
        CL_actor.GetProperty().SetColor(CL_boundary.color)
    else:
        CL_actor.GetProperty().SetColor(RED)
    return CL_actor


def create_text_for_CL_enumeration(
        pt_no,
        x,
        y,
        z,
        center_line_shift):
    atext = vtk.vtkVectorText()
    atext.SetText(str(pt_no))
    text_mapper = vtk.vtkPolyDataMapper()
    text_mapper.SetInputConnection(atext.GetOutputPort())
    text_actor = vtk.vtkActor()
    text_actor.SetMapper(text_mapper)
    text_actor.SetScale(2, 2, 2)
    text_actor.AddPosition(x, y+center_line_shift, z+4)
    return text_actor


def create_leg_scale(
        x,
        y,
        z,
        delta_z):
    leg_scale_source = vtk.vtkSphereSource()
    leg_scale_source.SetPhiResolution(3)
    leg_scale_source.SetThetaResolution(3)
    leg_scale_source.SetCenter(x, y, z+delta_z)
    leg_scale_source.SetRadius(2)
    # mapper
    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(leg_scale_source.GetOutput())
    else:
        mapper.SetInputConnection(leg_scale_source.GetOutputPort())
    # actor
    leg_scale_actor = vtk.vtkActor()
    leg_scale_actor.SetMapper(mapper)
    leg_scale_actor.GetProperty().SetColor(RED)

    # TEXT FOR SCALE____________
    atext = vtk.vtkVectorText()
    atext.SetText(str(delta_z))
    text_mapper = vtk.vtkPolyDataMapper()
    text_mapper.SetInputConnection(atext.GetOutputPort())
    text_actor = vtk.vtkActor()
    text_actor.SetMapper(text_mapper)
    text_actor.SetScale(5, 5, 5)
    text_actor.AddPosition(x, y, z+delta_z)

    return leg_scale_actor, text_actor


def create_cylinder_for_tolerance_range(
        x,
        y,
        z,
        z_tolerance):
    # source
    cylinder = vtk.vtkCylinderSource()
    cylinder.SetResolution(5)
    cylinder.SetHeight(z_tolerance)
    cylinder.SetRadius(1)
    # mapper
    cylinder_mapper = vtk.vtkPolyDataMapper()
    cylinder_mapper.SetInputConnection(cylinder.GetOutputPort())
    # actor
    cylinder_actor = vtk.vtkActor()
    cylinder_actor.SetMapper(cylinder_mapper)
    cylinder_actor.GetProperty().SetColor(BLUE)
    cylinder_actor.RotateX(90) # The rotation is around the world axis,
                              # therefor the shape need to be at (0,0,0)
                              # so that there is only translation
    cylinder_actor.SetPosition(x, y, z)

    return cylinder_actor


def create_line_btwn_knee_axis_pts(
        knee_axis):
    x0, y0, z0 = knee_axis[0] # pt 0
    x1, y1, z1 = knee_axis[1] # pt 1

    pts = vtk.vtkPoints() # Create the structure to store all the points

    pts.InsertNextPoint([x0, y0, z0])
    pts.InsertNextPoint([x1, y1, z1])

    # Setup the colors array
    colors = vtk.vtkUnsignedCharArray()
    colors.SetNumberOfComponents(3)
    colors.SetName('Colors')

    # Add the colors we created to the colors array
    colors.InsertNextTuple(WHITE)

    # Create the line between the knee axis points
    knee_axis_line = vtk.vtkLine()
    knee_axis_line.GetPointIds().SetId(0,0)
    knee_axis_line.GetPointIds().SetId(1,1)

    # Create a cell array to store the lines in and add the lines to it
    lines = vtk.vtkCellArray()
    lines.InsertNextCell(knee_axis_line)

    linesPolyData = vtk.vtkPolyData()
    linesPolyData.SetPoints(pts)
    linesPolyData.SetLines(lines)

    # Change the color of the lines
    linesPolyData.GetCellData().SetScalars(colors)

    # Visualize
    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(linesPolyData)
    else:
        mapper.SetInputData(linesPolyData)

    lineActor = vtk.vtkActor()
    lineActor.SetMapper(mapper)

    return lineActor


def create_Z_plane(
        plane_no,
        pos,
        name):
        # source
        plane_x_dim = 350
        plane_y_dim = 350
        plane_source = vtk.vtkPlaneSource()
        plane_source.SetPoint2(0, 0, plane_x_dim)
        plane_source.SetPoint1(0, plane_y_dim, 0)
        plane_source.SetCenter(0, 0, pos)
        plane_source.SetNormal(0, 0, 1)
        # mapper
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(plane_source.GetOutputPort())
        # actor
        plane_actor = vtk.vtkActor()
        plane_actor.SetMapper(mapper)
        plane_actor.GetProperty().SetOpacity(.3)

        # TEXT FOR SCALE____________
        atext = vtk.vtkVectorText()
        atext.SetText(f'{name}')
        text_mapper = vtk.vtkPolyDataMapper()
        text_mapper.SetInputConnection(atext.GetOutputPort())
        text_actor = vtk.vtkActor()
        text_actor.SetMapper(text_mapper)
        text_actor.SetScale(10, 10, 10)
        text_actor.AddPosition(-plane_x_dim/2 + 20, plane_y_dim/2 - plane_no*15 - 20, pos)

        return plane_actor, text_actor




#%% Function that regroup all the previous part of the visualization

def show_leg(
        knee,
        knee_axis,
        plane_pos,
        plane_name,
        CL_boundary,
        id_in_centerline_to_show=[None],
        z_tolerance=10,
        other_text=''):

    # change the camera angle at the beginning
    camera = vtk.vtkCamera();
    camera.SetPosition(0, -1300, 300);
    # create a rendering window and renderer
    ren = vtk.vtkRenderer()
    ren.SetActiveCamera(camera);

    normalized_radius = basic_functions.normalize_zero_to_one(knee.Radius,
                                                              min_bound=0)
    # LEG ___________________________________________________________________________
    for pt_no, (x,y,z) in enumerate(knee.Points):
        ren.AddActor(create_leg(knee, pt_no, x, y, z,
                                normalized_radius, ))
    # CENTERLINE_____________________________________________________________________
        center_line_shift = -120
        ren.AddActor(create_CL(pt_no, x, y, z, CL_boundary,
                               id_in_centerline_to_show, center_line_shift))
        # TEXT FOR CENTERLINE ENUMERATION___________
        ren.AddActor(create_text_for_CL_enumeration(pt_no, x, y, z, center_line_shift))

    # KNEE_AXIS _____________________________________________________________________
    scale_intervall = 20
    for x, y, z in knee_axis:
        for delta_z in range(-300, 300, scale_intervall):  # PT RED (scale)
            ren.AddActor(create_leg_scale(x, y, z, delta_z)[0])
            ren.AddActor(create_leg_scale(x, y, z, delta_z)[1])
        # CYLINDER (to indicate tolerance range)_____________________________________
        ren.AddActor(create_cylinder_for_tolerance_range(x, y, z, z_tolerance))

    # LINE BETWEEN KNEE_AXIS POINTS _________________________________________________
    ren.AddActor(create_line_btwn_knee_axis_pts(knee_axis))

    # PLANE _________________________________________________________________________
    for plane_no, (pos, name) in enumerate(zip(plane_pos, plane_name)):
        # create the planes
        ren.AddActor(create_Z_plane(plane_no, pos, name)[0])
        # create the text
        ren.AddActor(create_Z_plane(plane_no, pos, name)[1])

    # AXIS __________________________________________________________________________
    axes = vtk.vtkAxesActor()
    transform = vtk.vtkTransform()
    axes.SetUserTransform(transform)
    axes.SetConeRadius(0.1)
    axes.SetTotalLength(300, 300, 400)
    ren.AddActor(axes)
    # AXIS END ______________________________________________________________________

    # ADD text ______________________________________________________________________
    txt = vtk.vtkTextActor()
    txt.SetInput(knee.FileName + other_text)
    txtprop=txt.GetTextProperty()
    txtprop.SetFontSize(18)
    txt.SetDisplayPosition(20, 70)
    # ADD text END __________________________________________________________________

    # enable user interface interactor
    renWin = vtk.vtkRenderWindow()
    renWin.AddRenderer(ren)
    renWin.SetSize(1500, 1000)
    # create a renderwindowinteractor
    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(renWin)
    # assign actor to the renderer
    ren.AddActor(txt)
    # show the rendering
    renWin.Render()

    # show the rendering
    iren.Start()



#%% Implementation of the visualization module

# **** CLICK ON 'Q' TO SEE THE NEXT KNEE ****
all_dist_btw_pts = []
all_radius = []
all_dist_btw_pts_nomalized = []
for i in range(213, 214):
    CL = legs[i].Points
    COM_below_z_id, _ = find_CL_id_closest_to_random_3D_pt(CL, legs[i].COMBelowZ)
    COM_above_z_id, _ = find_CL_id_closest_to_random_3D_pt(CL, legs[i].COMAboveZ)
    pt_min_radius = legs[i].pt_min_radius_under_knee_ball

    # Prenons plutôt la distance 3D entre les points en parcourant tous les points entres les deux points a l'étude
    # Diviser cette distance avec le rayon en ce point comme j'assume qu'a cette poisition c'est principalement de l'os et c'est donc surement un bon indicateur de la longueur totale de la jambe de la personne
    closest_id_to_axis, closest_pt_to_axis, _ = \
        legs[i].find_closest_pt_to_axis_3D(legs_axis[i])

    smallest_radius_under_knee = CL[pt_min_radius]
                                                                                     # TODO: (ALEX) - Would it be better to find the distance by adding all the segments btw all the points between the two points??????????????????????????
    dist_btwn_pts = find_dist_3D_btw_two_pts(closest_pt_to_axis,
                                            smallest_radius_under_knee)
    all_dist_btw_pts.append(dist_btwn_pts)
    normalisation_fact = legs[i].Radius[pt_min_radius][0]
    all_dist_btw_pts_nomalized.append(dist_btwn_pts / normalisation_fact)
    all_radius.append(legs[i].Radius[pt_min_radius][0])


    dist_btwn_real_predicted_axis = \
        find_dist_3D_btw_two_pts(closest_pt_to_axis,
                                 CL[legs[i].axis_predicted_id])
    other_txt = \
    f"""
    \ndist_btwn_pts: {dist_btwn_pts:0.3g}
    normalisation_fact: {normalisation_fact:0.3g}
    Dist btwn real axis and predicted axis: {dist_btwn_real_predicted_axis:0.4g} mm
    """

    CL_boundary = namedtuple('CL_borne', ['upper', 'lower', 'color'])
    CL_boundary1 = CL_boundary(lower=closest_id_to_axis-15,
                               upper=closest_id_to_axis+15,
                               color=BLUE)

    show_leg(
            knee=legs[i],
            knee_axis=legs_axis[i],

            plane_pos=[legs[i].COMBelowZ[2],
                       legs[i].COMAboveZ[2],
                       smallest_radius_under_knee[2],
                       closest_pt_to_axis[2],
                       CL[legs[i].axis_predicted_id][2]
                       ],

            plane_name=['COMBelowZ',
                        'COMAboveZ',
                        'smallest_radius_under_knee',
                         'closest_pt_to_axis',
                        'axis_predicted_id'],

            id_in_centerline_to_show=[closest_id_to_axis,
                                      legs[i].axis_predicted_id],
            other_text=str(other_txt),
            CL_boundary = CL_boundary1
            )



























