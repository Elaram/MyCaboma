#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
from skimage import draw
import tifffile as tiff
import pymrt as mrt
import pymrt.geometry
from collections import namedtuple
# VISUALISATION
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
# READ FILES
from importlib import reload
import basic_functions
reload(basic_functions)



class Knee:
    def __init__(self):

        # features in the file
        self.POINTS = []
        self.POINTS_corr = []
        self.LengthArray = []
        self.TortuosityArray = []
        self.RadiusArray = []
        self.EdgeArray = []
        self.EdgePCoordArray = []
        self.CurvatureArray = []
        self.TorsionArray = []
        self.TangentArray = []
        self.NormalArray = []
        self.BinormalArray = []
        # other features
        self.side = ''
        self.patient_no = ''
        self.closest_pt_dist = 0
        self.closest_pt_id = 0
        self.closest_pt_intervall = []
        self.filePath = ''
        self.axis = Knee_axis()
        self._2D = np.array([])   # TODO renommer cette valeur a img_2D
        self.caract_list = ['POINTS', 'LengthArray', 'TortuosityArray',
                           'RadiusArray', 'EdgeArray', 'EdgePCoordArray',
                           'CurvatureArray', 'TorsionArray', 'TangentArray',
                           'NormalArray', 'BinormalArray']

    def fill_data(self, file, base_path): # OK
        """Extract the all the data in a single array that contain the caracteristics for all the files"""
        file_path = base_path + '/' + file
        # append the information read into the file_path to the the right type of array
        self.patient_no = file.split('.')[0]
        if 'D' in self.patient_no:
            self.fill_caract_list(file_path)
            self.side = 'D'
        elif 'G' in self.patient_no:
            self.fill_caract_list(file_path)
            self.side = 'G'
        else:
            print('this file is not of a standard name formating')
        print ("leg_axis: ", file)

    def fill_caract_list(self, file_path): # OK     # implémenter de l'inhéritance ou de la composition en fonction de la relation entre knee axis et knee, car cette technique en passant un objet est weird....
        """
        Fill the list of caracteristics for one file
        """
        point_added = 0
        point_to_add = 0
        category_no = 0
        add_value = False

        with open(file_path) as knee_data_file:
            for line in knee_data_file:  # Read the vtk files line by line
                # stop at the end of the category we specified
                if category_no == len(self.caract_list):
                    break

                line_split = line.split()
                if line_split == []:    # Go to next loop if paragraph jump
                    continue

                if line_split[0] == self.caract_list[category_no]:
                    if self.caract_list[category_no] == 'POINTS':
                        group_size = 3
                        point_to_add = float(line_split[1]) * group_size   # because by group of 3 coordinate
                    else:
                        group_size = int(line_split[1])
                        point_to_add = float(line_split[2]) * group_size

                    point_added = 0
                    add_value = True
                    continue

                for number in line.split():
                    if add_value:
                        getattr(self, self.caract_list[category_no]).append(float(number))
                        point_added += 1
                        # Add values until all value are added based on the title tag of the category
                        if point_added == point_to_add:
                            add_value = False
                            # Split the data for each category in the specified format
                            if group_size > 1:
                                data = getattr(self, self.caract_list[category_no])

                                setattr(self, self.caract_list[category_no],
                                        [data[pos:pos+group_size] for pos in range(0, len(data), group_size)])

                            category_no += 1  # Go to the next category

        self.filePath = file_path   # Add the name of the file_path at the end  (TODO: le faire a l'extérieur dans le main si possible)

    def find_closest_pt(self): # OK
        """
        Find the point of the leg axis which is the closest to the knee axis
        (FOR DATA_1 only)
        """
        for pt_id, pt in enumerate(self.POINTS):
            dist = basic_functions.Dist_point_line_3D(pt=pt, start=self.axis.POINTS[0],
                                                     end=self.axis.POINTS[1], ite=10)
            if pt_id == 0: # assign a closest pt distance one the first loop
                self.closest_pt_dist = dist
            elif dist < self.closest_pt_dist: #  TODO verifier cette condition: and pt_id == 0:
                self.closest_pt_dist = dist
                self.closest_pt_id = pt_id

    def find_closest_pt_intervall(self):
        """
        Find the values that are include in the acceptable 1cm intervall
        """
        # Therefore 0.5cm up and 0.5cm down
        self.closest_pt_intervall = [self.closest_pt_id]
        if self.closest_pt_id != 0:    #TODO verifier pourquoi un des points == 0    # est-ce que c'est seulement pour les jambes de type_1 et non pour celles de type_2
            #UP                        # TODO: (ALEXM) est-ce qu'on veut l'intervalle seulement en hauteur ou on veut l'inveralle qui relie les points (le deuxième est ce qui est fait actuellement)
            dist_up = 0
            closest_up = self.closest_pt_id
            while (dist_up <= 5):
                pt = self.POINTS[closest_up]
                closest_up += 1
                next_pt = self.POINTS[closest_up]
                dist_up += np.linalg.norm(np.array(pt) - np.array(next_pt))
                if dist_up <= 5:
                    self.closest_pt_intervall.append(closest_up)
            #DOWN
            dist_down = 0
            closest_down = self.closest_pt_id
            while (dist_down <= 5):
                pt = self.POINTS[closest_down]
                closest_down -= 1
                next_pt = self.POINTS[closest_down]
                dist_down += np.linalg.norm(np.array(pt) - np.array(next_pt))
                if dist_up <= 5:
                    self.closest_pt_intervall.append(closest_down)

    def create_3D_matrix(self, knee_no, knees, knee_axis, RESIZE_FACT=1,
                         save_on=False, SHOW_CLOSEST_PT=False, show_data_axis=False):
        """
        Create the 3D volumes to feed to the neural network
        """
        sphere_pos = namedtuple('sphere_pos', ['x', 'y', 'z'])

        # Initialize the 3D matrice size based on the output of the
        # zero_out_array function
        spare_space = 20
        pt_min_x, pt_min_y, pt_min_z = knees.pt_min
        pt_max_x, pt_max_y, pt_max_z = knees.pt_max
        # height
        new_height = (pt_max_z - pt_min_z + spare_space) // RESIZE_FACT              # Note: I was using math.ceil before and I switch to //
        # x
        new_x = (pt_max_x - pt_min_x + 2*knees.R_max + spare_space) // RESIZE_FACT
        # y
        new_y = (pt_max_y - pt_min_y + 2*knees.R_max + spare_space) // RESIZE_FACT

        # Convert the position coord. to 3D array
        matrix3D = np.zeros((new_height, new_x, new_y))
        for pt_no, (pt_x, pt_y, pt_z) in enumerate(self.POINTS):
            x = round((pt_x - pt_min_x + knees.R_max)/RESIZE_FACT)
            y = round((pt_y - pt_min_y + knees.R_max)/RESIZE_FACT)
            height = round((pt_z - pt_min_z) / RESIZE_FACT)

            self.POINTS_corr.append([x, y, height]) # for future references
            """
            # Create circle from that point
            radius = math.floor(self.RadiusArray[pt_no] / RESIZE_FACT)
            if SHOW_CLOSEST_PT:
                if (self.closest_pt_id[knee_no] - 5 < pt_no < \
                    self.closest_pt_id[knee_no] + 5):
                    radius = 120

            rr, cc = draw.circle(x, y, radius=radius)
            matrix3D[height][rr, cc] = 255
            """
            # INSTEAD: create a sphere from that point
            radius = math.floor(self.RadiusArray[pt_no] / RESIZE_FACT)
            sphere = mrt.geometry.sphere(shape=radius*2, position=0.5,
                                         radius=radius)
            s_pos = sphere_pos(int(x), int(y), int(height))

            print('matrix3D.shape', matrix3D.shape)
            print(matrix3D[s_pos.x - radius : s_pos.x + sphere.shape[0] - radius,
                     s_pos.y - radius : s_pos.y + sphere.shape[1] - radius,
                     s_pos.z - radius : s_pos.z + sphere.shape[2] - radius].shape) #\
#                     = matrix3D[s_pos.x - radius : s_pos.x + sphere.shape[0] - radius,
#                                s_pos.y - radius : s_pos.y + sphere.shape[1] - radius,
#                                s_pos.z - radius : s_pos.z + sphere.shape[2] - radius].astype(bool) | sphere.astype(bool)

        if save_on:
            tiff.imsave('Leg_3D.tif', matrix3D.astype(np.uint8))

        a_s = 2 # axis_size
        if show_data_axis:
            for pt in self.axis.POINTS:
                x = round((pt[0] - knees.pt_min[0] + knees.R_max)/RESIZE_FACT)       # TODO: (ALEX) Commenter
                y = round((pt[1] - knees.pt_min[1] + knees.R_max)/RESIZE_FACT)
                height = round((pt[2] - knees.pt_min[2])/RESIZE_FACT)

                matrix3D[(x - a_s):(x + a_s), (y - a_s):(y + a_s),
                         (height-a_s):(height+a_s)] = 1000                           #TODO: (ALEX) Commenter

        matrix3D = np.flip(matrix3D, 0)

        return matrix3D

    def show_knee(self, show_knee_axis): # UGLY (better to use leg_visualization.py)
        """
        Show the knees in a matplotlib plot
        """
        fig = plt.figure()
        ax = Axes3D(fig)
        ax.set_aspect('equal')
        # Create the leg axis:
        " To have an equal aspect ratio of the axis "
        x, y, z = np.array(self.POINTS).transpose()           # TODO verifier que encore fonctionnel (et est-ce que c'est plus compréhensible que la façon ci-dessous en commentaire)  (mettre tous les attributs sous forme de numpy array ? implique que je ne peux plus faire des append...)
        # Chose the axis with the biggest span:
        min_val = min(z)
        max_val = max(z)
        ax.set_xlim(min_val, max_val)
        ax.set_ylim(min_val, max_val)
        ax.set_zlim(min_val, max_val)
        ax.scatter(x, y, z, s=np.array(self.RadiusArray)*2)

        """Create the knee axis"""
        if show_knee_axis:
            x, y, z = np.array(self.axis.POINTS).transpose()
            ax.scatter(x, y, z)
            # Plot the point closest to the knee axis
            cp = self.closest_pt_id
            ax.scatter(self.POINTS[cp][0], self.POINTS[cp][1],
                       self.POINTS[cp][2], s=240)

        plt.show()


class Knee_axis():     # inheritance of knee functions   (+ changer le nom pour classe ie   KneeAxis())
    def __init__(self):

        self.POINTS = []
        self.Angle = []
        self.Offset = []
        self.Normal = []
        self.Binormal = []
        self.side = ''
        self.filePath = ''
        self.caract_list = ['Angle', 'Offset', 'Normal', 'Binormal', 'POINTS']
        self.patient_no = ''


    def fill_data(self, file, base_path):
        """
        Extract the all the data in a single array that contain the
        caracteristics for all the files
        """
        file_path = base_path + '/' + file

        self.patient_no = file.split('.')[0]
        if 'D' in self.patient_no:
            self.fill_caract_list(file_path)
            self.side = 'D'
        if 'G' in self.patient_no:
            self.fill_caract_list(file_path)
            self.side = 'G'
        print("knee_axis: ", file)


    def fill_caract_list(self, file_path):      # TODO !!!!!!! implémenter de l'inhéritance ou de la composition en fonction de la relation entre knee axis et knee, car cette technique en passant un objet est weird....
        """Fill the list of caracteristics for one file""" # TODO: car il s'agit d'un duplicat de la fonction qui est présente dans la classe Knee()
        point_added = 0
        point_to_add = 0
        category_no = 0
        add_value = False

        with open(file_path) as knee_data_file:
            for line in knee_data_file:  # Read the vtk files line by line
                # stop at the end of the category we specified
                if category_no == len(self.caract_list):
                    break

                line_split = line.split()
                if line_split == []:    # Go to next loop if paragraph jump
                    continue

                if line_split[0] == self.caract_list[category_no]:
                    if self.caract_list[category_no] == 'POINTS':
                        group_size = 3
                        point_to_add = float(line_split[1]) * group_size   # because by group of 3 coordinate
                    else:
                        group_size = int(line_split[1])
                        point_to_add = float(line_split[2]) * group_size

                    point_added = 0
                    add_value = True
                    continue

                for number in line.split():
                    if add_value:
                        getattr(self, self.caract_list[category_no]).append(float(number))
                        point_added += 1
                        # Add values until all value are added based on the title tag of the category
                        if point_added == point_to_add:
                            add_value = False
                            # Split the data for each category in the specified format
                            if group_size > 1:
                                data = getattr(self, self.caract_list[category_no])

                                setattr(self, self.caract_list[category_no],
                                        [data[pos:pos+group_size] for pos in range(0, len(data), group_size)])

                            category_no += 1  # Go to the next category

        self.filePath = file_path   # Add the name of the file_path at the end  (TODO: le faire a l'extérieur dans le main si possible)


class Knees:
    def __init__(self):

        # Features in the files
        self.closest_pt = []
        self.closest_pt_intervall = []
        self.legs = []
        # New features
        self.pt_max = [0,0,0]
        self.pt_min = [0,0,0]
        self.R_max = 0

    """"put the position value in an array starting from (0,0,0) and in a positive intervall"""
    def zero_out_array(self):
        """Find the maximum radius in all knees"""
        for knee in self.legs:
            for rayon in knee.RadiusArray:
                if rayon > self.R_max:
                    self.R_max = rayon
            """Find the moximum and minimum position of the points in all knees"""
            for pt in knee.POINTS:
                for coord in range(3):
                    if pt[coord] > self.pt_max[coord]:
                        self.pt_max[coord] = math.ceil(pt[coord])
                    if pt[coord] < self.pt_min[coord]:
                        self.pt_min[coord] = math.floor(pt[coord])

