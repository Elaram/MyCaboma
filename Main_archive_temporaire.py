from importlib import reload
import basic_functions
import CNN_tensorflow_2D_LR_archive_temporaire
import leg
# Reload to always use the last file saved
reload(basic_functions)
reload(CNN_tensorflow_2D_LR_archive_temporaire)
reload(leg)

# Import function that are used in each of these file
# STATISTIC
#from statistic import visualisation_statistic, show_histogram
# BASIC FUNCTION
from basic_functions import Dist_point_line_3D, splitXY

"""Class"""
from leg import Knee, Knees, Knee_axis

# Module généraux
import numpy as np
import matplotlib.pyplot as plt
import math
import tensorflow as tf
import os

#def main():
base_path = r"C:\Users\alexa\Google Drive\Poly_A2017\MyCaboma_CNN_D_G\dataset_AI"
all_knees = Knees()
all_knees_axis = []

files=os.listdir(base_path)
# Sort the file in a natural alphabetical and numerical way:
files.sort(key=basic_functions.natural_keys)



#%% Read all the files (knee and knee_axis)

for i, file in enumerate(files):
    if "knee_axis" in file:
        knee_axis = Knee_axis()
        knee_axis.fill_data(file, base_path)
        all_knees_axis.append(knee_axis)
    elif ".stl_2." in file or ".aop_2." in file or ".obj_2." in file or ".ply_2." in file:
        knee = Knee()
        knee.fill_data(file, base_path)
        all_knees.legs.append(knee)

all_knee_D = Knees()
all_knee_G = Knees()
for knee_no, knee in enumerate(all_knees.legs):
    if all_knees_axis[knee_no].patient_no == knee.patient_no: # this knee axis correspond to this knee
        knee.axis = all_knees_axis[knee_no]
        knee.find_closest_pt()
    else :
        print('****Warning there is a mismatch between the knee axis and the knee****')   #TODO verifier pourquoi dans la dernière boucle passe par ici
    knee.find_closest_pt_intervall()
    if knee.side == 'D':
        all_knee_D.legs.append(knee)
    elif knee.side == 'G':
        all_knee_G.legs.append(knee)



#%% SHOW all legs 3D

visualisation3D = False
if visualisation3D:
    for knee_no, myKnee in enumerate(all_knees.legs):
        print("knee_no", knee_no)
        print("number of points: ", len(myKnee.POINTS))
        myKnee.show_knee(show_knee_axis=True)
# END of show all legs 3D =====================================================

#    "Statistique visualisation"  #(ALEXM: need to fix it because something happen when I pushed it)
#    visualisation_statistic(data_2, all_knee_closest_pt)

## 2D REPRESENTATION OF LEGS
all_knees.zero_out_array()



#%% Convert the information to 2D images:
# Right knee
RESIZE_FACT = 1
for knee_no, knee in enumerate(all_knees.legs[0:2]):
    print('knee_no', knee_no)
    matrix3D = knee.create_3D_matrix(knee_no, all_knees, knee.axis,
                                     RESIZE_FACT=RESIZE_FACT, save_on=False,
                                     SHOW_CLOSEST_PT=False, show_data_axis=False)

    # add all the value along two different axis to create a 2D image from the 3D
    # then concatenate them togheter to have the front(left) and side(right)
    knee._2D = np.concatenate((np.sum(matrix3D, axis=1), np.sum(matrix3D, axis=2)),axis=1)
# END of conversion to 2D images =============================================



#%% SHOW visualisation 2D:
visualisation2D = True;
if visualisation2D:
    try:
        for knee_no, knee in enumerate(all_knees.legs):
            axis_height = knee.POINTS_corr[knee.closest_pt_id][2]
            print('knee.side: ',  knee.side, 'axis_height: ', axis_height)
            plt.imshow(knee._2D, cmap='jet')
            plt.axhline(y=axis_height, color='r')
            plt.show()

    except TypeError as TE:
        print(knee_no, TE)                              # TODO: tous les gauches contiennent des points mais leur image est de dimension 0 il semble.
  # TODO ne pas laisser ça comme ça, cette valeur doit être enlevé et on doit comprendre pourquoi l'image a une taille de 0!!
        print("Cette image cause un typeError")    # TODO résoudre ce problème
# END OF 2D REPRESENTATION OF LEGS  ===========================================



#%% CNN2D:
# VARIABLES for the network
all_kneeD_2D = [knee._2D for knee in all_knees.legs if knee.side == 'D']
all_kneeG_2D = [knee._2D for knee in all_knees.legs if knee.side == 'G']
train_x, train_y, test_x, test_y = link_data_and_onehot(all_kneeD_2D, all_kneeG_2D)

BATCH_SIZE = 10
N_CLASSES = 2
NUM_FEATURES = 64

#y_pred = 0
LEARNING_RATE = 0.001
POOLING_SIZE = 4
KEEP_RATE = 0.9

SIZE_X = train_x[0].shape[0]
SIZE_Y = train_x[0].shape[1]
S_X_AFTER_POOLING = math.ceil(math.ceil(SIZE_X/POOLING_SIZE)/POOLING_SIZE)
S_Y_AFTER_POOLING = math.ceil(math.ceil(SIZE_Y/POOLING_SIZE)/POOLING_SIZE)

FINAL_SIZE = S_X_AFTER_POOLING * S_Y_AFTER_POOLING * NUM_FEATURES

x = tf.placeholder('float', shape=[None, SIZE_X, SIZE_Y] )
y = tf.placeholder('float', shape=[None, N_CLASSES])         # it's a one hot of [0,0]

# Learning
doTrain = False
if doTrain:
    tensorflow_2D_G_D.train_neural_network(x, y, FINAL_SIZE, N_CLASSES, SIZE_Y,
                                           SIZE_X, KEEP_RATE, NUM_FEATURES,
                                           POOLING_SIZE, LEARNING_RATE, BATCH_SIZE,
                                           train_x, train_y, test_x, test_y)
# END CNN2D ================================================================


#%% TEST basic classification with average

id_val = [knee.closest_pt_id/len(knee.POINTS) for knee in all_knees.legs]
avg_id = sum(id_val)/float(len(all_knees.legs))
avg_classification = [avg_id*knee.closest_pt_id  for knee in all_knees.legs]

# TEST:
import matplotlib.pyplot as plt
import numpy as np
import plotly.plotly as py
plt.hist(avg_classification, bins=50)
plt.show()

classification = []
avg_classification = [round(id_classification) for id_classification in avg_classification]
for i, knee in enumerate(all_knees.legs):
    print('avg_classification[i]', avg_classification[i], 'knee.closest_pt_intervall', knee.closest_pt_intervall)
    print(avg_classification[i] in  knee.closest_pt_intervall)
    classification.append(avg_classification[i] in  knee.closest_pt_intervall)

percentage_accuracy = sum(classification) / len(all_knees.legs)
print('The classification accuracy: ', percentage_accuracy)


## RUN MAIN:
#main()










# %% TEST TEST
import pymrt as mrt
import pymrt.geometry
import numpy as np
from collections import namedtuple
import matplotlib.pyplot as plt
from random import random

add_sphere_pos = namedtuple('add_sphere_pos', ['x', 'y', 'z'])
space = np.zeros((1000, 1000, 1000))

for _ in range(1):
    rand_x = random()
    rand_y = random()
    rand_z = random()
    rand_radius = random()

    sphere = np.array(mrt.geometry.sphere(shape=200, position=0.5,
                                          radius=100*rand_radius))

    sphere1 = add_sphere_pos(int(800*rand_x), int(800*rand_y), int(800*rand_z))

    space = space.astype(bool)
    space[sphere1.x:(sphere1.x + sphere.shape[0]),
          sphere1.y:(sphere1.y + sphere.shape[1]),
          sphere1.z:(sphere1.z + sphere.shape[2])] |=  sphere.astype(bool)


# smash the 3D array to a 2D array()
space_dim0 = np.sum(space, axis=1)
plt.imshow(space_dim0)
plt.show()





#%%
sphere_pos = namedtuple('sphere_pos', ['x', 'y', 'z'])

radius = ...
sphere = np.array(mrt.geometry.sphere(shape=radius*2, position=0.5,
                                      radius=radius))

s_pos = sphere_pos(int(pos1), int(pos2), int(pos3))

space[s_pos.x:(s_pos.x + sphere.shape[0]),
      s_pos.y:(s_pos.y + sphere.shape[1]),
      s_pos.z:(s_pos.z + sphere.shape[2])] |= sphere.astype(bool)



