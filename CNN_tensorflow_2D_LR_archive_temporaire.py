# -*- coding: utf-8 -*-
"""
Created on Thu Oct 19 12:14:03 2017

@author: alexa
"""
import tensorflow as tf
from random import shuffle 
import numpy as np


def conv2d(x, W, stride=1):
    return tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding='SAME')


# Pooling is a simplification of an image (conv2D is an extraction of feature)
def maxpool2d(x, k): # k = pooling_size
    #                 size of the window  mvt of the window
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], 
                          strides=[1, k, k, 1], padding='SAME')



def convolutional_neural_network(x, FINAL_SIZE, N_CLASSES, SIZE_Y, SIZE_X, KEEP_RATE, NUM_FEATURES, POOLING_SIZE):
    weights = {'wc1': tf.Variable(tf.random_normal([3, 3, 1, 32])),
               'wc2': tf.Variable(tf.random_normal([3, 3, 32, NUM_FEATURES])),
               'wfc': tf.Variable(tf.random_normal([FINAL_SIZE, 256])),
               'out': tf.Variable(tf.random_normal([256, N_CLASSES]))}

    biases = {'bc1': tf.Variable(tf.random_normal([32])),
              'bc2': tf.Variable(tf.random_normal([64])),
              'bfc': tf.Variable(tf.random_normal([256])),
              'out': tf.Variable(tf.random_normal([N_CLASSES]))}

    x = tf.reshape(x, shape=[-1, SIZE_X, SIZE_Y, 1])
    print(x.shape)

    conv1 = tf.nn.relu(conv2d(x, weights['wc1']) + biases['bc1'])
    print('conv1 before pooling: ', conv1.shape)
    conv1 = maxpool2d(conv1, POOLING_SIZE)
    print('conv1 afterPool', conv1.shape)

    conv2 = tf.nn.relu(conv2d(conv1, weights['wc2']) + biases['bc2'])
    print('conv2', conv2.shape)
    conv2 = maxpool2d(conv2, POOLING_SIZE)
    print('after max pool conv2', conv2.shape)
    
    fc = tf.reshape(conv2, [-1, FINAL_SIZE])
    fc = tf.nn.relu(tf.matmul(fc, weights['wfc']) + biases['bfc'])
    fc = tf.nn.dropout(fc, KEEP_RATE)

    output = tf.matmul(fc, weights['out']) + biases['out']
    return output


def train_neural_network(x, y, FINAL_SIZE, N_CLASSES, SIZE_Y, SIZE_X, KEEP_RATE, NUM_FEATURES, 
                         POOLING_SIZE, LEARNING_RATE, BATCH_SIZE, train_x, train_y, test_x, test_y):
    accuracy_list = []
    
    prediction = convolutional_neural_network(x, FINAL_SIZE, N_CLASSES, SIZE_Y, SIZE_X, KEEP_RATE, NUM_FEATURES, POOLING_SIZE)
    
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y))
    
    optimizer = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(cost)
    
    hm_epochs = 60

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        
        for epoch in range(hm_epochs):
            epoch_loss = 0
            i = 0
            print(i) 
            if epoch % 2 == 0:
                if epoch != 0:
                    correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
                    accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
                    accuracy_val = accuracy.eval({x: test_x, y: test_y})
                    print('Accuracy:', accuracy_val)
                    accuracy_list.append(accuracy_val)
                
            while i < len(train_x):
                start = i
                end = i + BATCH_SIZE
                 # Create the bacth
                batch_x = np.array(train_x[start:end])
                batch_y = np.array(train_y[start:end])
                # Run the session of learning on the batches
                _, c = sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y})
                
                epoch_loss += c
                i+=1;

            print('Epoch', epoch, 'completed out of', hm_epochs, 'loss:', epoch_loss)
    
    print(accuracy_list)




