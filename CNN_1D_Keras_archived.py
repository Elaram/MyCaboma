# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 14:13:26 2017

@author: alma
"""

# MY_FUNCTION:::
from random import randint
#"Make sure that all my function can be reload so that every change is reload"
# The module we created
from importlib import reload
import basic_functions
import CNN_tensorflow_2D_LR_archive_temporaire
import read_legs_archived
import keras
import numpy as np
import os

np.random.seed(123) #for reproductibility

# Reload to always use the last file saved
reload(basic_functions)
reload(CNN_tensorflow_2D_LR_archive_temporaire)
reload(read_legs_archived)

# Import function that are used in each of these file
# STATISTIC
"""Class"""
from read_legs_archived  import Knee, readKnee, Knee_axis
from basic_functions import splitXY

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Conv2D, Conv1D
from keras.layers.pooling import MaxPooling1D
from keras.utils import np_utils
from keras.optimizers import SGD
from keras import backend as K
from keras.utils import plot_model



from matplotlib import pyplot as plt

path = r"C:\Users\Samuel Gagne\Desktop\Files\dataset_AI"
files=os.listdir(path)

""" 
Creating two factors that summed up give the total value assigned
    
Parameters:
----------
tot : int
    Length of targeted interval
        
Returns: 
-------
m : int
    part one that sums up to tot
k : int
    part two that sums up to tot
"""

def sequence(tot):
    
    find = False
    while find == False:
      m = randint(1, tot)
      k = randint(1, tot)
      if m + k == tot and m>=3 and k >=3:
         find = True
         return [m,k]

""" 
Fonction ne fonctionne pas. généraLISE LA NORMALISATION EN NORMALISANT LES JAMBES PAR RAPPORT À L'ENSEMBLE
    
Parameters:
----------

        
Returns: 
-------

"""
     
def getTensor_gen_norm(tot, knees, knees_axis):
    knee_tensors = []
    all_knees_COM_intervals = []
    max_x = -12000
    min_x = 2000
    
    max_y = -12000
    min_y = 2000
    
    max_z = -12000
    min_z = 2000
    
    max_tx = -12000
    min_tx = 2000
    
    max_ty = -12000
    min_ty = 2000
    
    max_tz = -12000
    min_tz = 2000
    
    max_Nx = -12000
    min_Nx = 2000
    
    max_Ny = -12000
    min_Ny = 2000
    
    max_Nz = -12000
    min_Nz = 2000
    
    max_Bx = -12000
    min_Bx = 2000
    
    max_By = -12000
    min_By = 2000
    
    max_Bz = -12000
    min_Bz = 2000
    for j in range(len(knees)):
        for i in range(len(knees[j].Points)):            
            if max_x < knees[j].Points[i][0]:
                max_x = knees[j].Points[i][0]
            if max_y < knees[j].Points[i][1]:
                max_y = knees[j].Points[i][1]
            if max_z < knees[j].Points[i][2]:
                max_z = knees[j].Points[i][2]
            if max_tx < knees[j].Tangent[i][0]:
                max_tx = knees[j].Tangent[i][0]
            if max_ty < knees[j].Tangent[i][1]:
                max_ty = knees[j].Tangent[i][1]
            if max_tz < knees[j].Tangent[i][2]:
                max_tz = knees[j].Tangent[i][2]
            if max_Nx < knees[j].Normal[i][0]:
                max_Nx = knees[j].Normal[i][0]
            if max_Ny < knees[j].Normal[i][1]:
                max_Ny = knees[j].Normal[i][1]
            if max_Nz < knees[j].Normal[i][2]:
                max_Nz = knees[j].Normal[i][2]
            if max_Bx < knees[j].Binormal[i][0]:
                max_Bx = knees[j].Binormal[i][0]
            if max_By < knees[j].Binormal[i][1]:
                max_By = knees[j].Binormal[i][1]
            if max_Bz < knees[j].Binormal[i][2]:
                max_Bz = knees[j].Binormal[i][2]
                
            if min_x > knees[j].Points[i][0]:
                min_x = knees[j].Points[i][0]
            if min_y > knees[j].Points[i][1]:
                min_y = knees[j].Points[i][1]
            if min_z > knees[j].Points[i][2]:
                min_z = knees[j].Points[i][2]
            if min_tx > knees[j].Tangent[i][0]:
                min_tx = knees[j].Tangent[i][0]
            if min_ty > knees[j].Tangent[i][1]:
                min_ty = knees[j].Tangent[i][1]
            if min_tz > knees[j].Tangent[i][2]:
                min_tz = knees[j].Tangent[i][2]
            if min_Nx > knees[j].Normal[i][0]:
                min_Nx = knees[j].Normal[i][0]
            if min_Ny > knees[j].Normal[i][1]:
                min_Ny = knees[j].Normal[i][1]
            if min_Nz > knees[j].Normal[i][2]:
                min_Nz = knees[j].Normal[i][2]
            if min_Bx > knees[j].Binormal[i][0]:
                min_Bx = knees[j].Binormal[i][0]
            if min_By > knees[j].Binormal[i][1]:
                min_By = knees[j].Binormal[i][1]
            if min_Bz > knees[j].Binormal[i][2]:
                min_Bz = knees[j].Binormal[i][2]
                
    for j in range(len(knees)):      
        intermediate = np.array([])
        for i in range(len(knees[j].Points)):
            knees[j].Points[i][0] = (((knees[j].Points[i][0] - min_x)) / (max_x - min_x))
            knees[j].Points[i][1] = (((knees[j].Points[i][1] - min_y)) / (max_y - min_y))
            knees[j].Points[i][2] = (((knees[j].Points[i][2] - min_z)) / (max_z - min_z))
            
            knees[j].Normal[i][0] = (((knees[j].Normal[i][0] - min_Nx)) / (max_Nx - min_Nx))
            knees[j].Normal[i][1] = (((knees[j].Normal[i][1] - min_Ny)) / (max_Ny - min_Ny))
            knees[j].Normal[i][2] = (((knees[j].Normal[i][2] - min_Nz)) / (max_Nz - min_Nz))
        
            knees[j].Tangent[i][0] = (((knees[j].Tangent[i][0] - min_tx)) / (max_tx - min_tx))
            knees[j].Tangent[i][1] = (((knees[j].Tangent[i][1] - min_ty)) / (max_ty - min_ty))
            knees[j].Tangent[i][2] = (((knees[j].Tangent[i][2] - min_tz)) / (max_tz - min_tz))
            
            knees[j].Binormal[i][0] = (((knees[j].Binormal[i][0] - min_Bx)) / (max_Bx - min_Bx))
            knees[j].Binormal[i][1] = (((knees[j].Binormal[i][1] - min_By)) / (max_By - min_By))
            knees[j].Binormal[i][2] = (((knees[j].Binormal[i][2] - min_Bz)) / (max_Bz - min_Bz))
        knees[j].Radius = (((knees[j].Radius - min(knees[j].Radius))) / (max(knees[j].Radius) - min(knees[j].Radius)))
        knees[j].Curvature = (((knees[j].Curvature - min(knees[j].Curvature))) / (max(knees[j].Curvature) - min(knees[j].Curvature)))
        knees[j].Torsion= (((knees[j].Torsion - min(knees[j].Torsion))) / (max(knees[j].Torsion) - min(knees[j].Torsion)))
       
        
        intermediate = np.concatenate((knees[j].Points, knees[j].Radius, knees[j].Curvature, 
                                     knees[j].Torsion, knees[j].Normal, knees[j].Tangent, 
                                      knees[j].Binormal), axis=1)
        knees[j].axis = knees_axis[j]
        knees[j].find_closest_pt()
        print(j)
        print(knees[j].axis.POINTS[1])
        print(knees[j].closest_pt_id)
        
        steps = sequence(tot)
        point_id_lower = knees[j].closest_pt_id -steps[0]
        point_id_upper = knees[j].closest_pt_id +steps[1] 
            
        if point_id_lower < 0:
            point_id_upper = point_id_upper - point_id_lower
            point_id_lower = 0
        
        knee_tensors.append(intermediate[(point_id_lower):(point_id_upper)])
        all_knees_COM_intervals.append([point_id_lower, point_id_upper])
    
    return [knee_tensors, all_knees_COM_intervals]

""" 
Creating the tensor with all caracteristics concatenated in one np.array 
for all knees
    
Parameters:
----------
tot : int
    Length of targeted interval
files : string
    files names in AI dataset path
        
Returns: 
-------
knees : list
    List of individual legs from sample data (integral and original data)

knee_tensors : list
    Tensors containing the used features for the targetted interval

all_knees_COM_intervals : list
    Intervals containing the knee within 1cm of a given length (tot)
    
all_knees_axis : list
    Position of the knee axis for each individual leg
"""


def tensor_generate(tot, files):
    knees = []
    knee_tensors = []
    all_knees_axis = []
    all_knees_COM_intervals = []
    for i, file in enumerate(files):
        if "knee_axis" in file and "D" in file:
            knee_axis = Knee_axis()
            knee_axis.fill_data(file, path)
            all_knees_axis.append(knee_axis) 
    j = 0
    for i, file in enumerate(files):
        if "knee_axis" not in file and "_2" in file and "D" in file:
            file_path = path + '\\' + file
            knee = readKnee(file_path)
            
            COMUp = knee.COMAboveZ
            for pt_id, pt in enumerate(knee.Points):
                dist = basic_functions.Dist_point_line_3D(pt=pt, start=COMUp,
                                                         end=COMUp, ite=10)
                if pt_id == 0: # assign a closest pt distance one the first loop
                    closest_point_dist = dist
                elif dist < closest_point_dist: #  TODO verifier cette condition: and pt_id == 0:
                    closest_point_dist = dist
                    point_id_upper = pt_id
            COMDown = knee.COMBelowZ
            for pt_id, pt in enumerate(knee.Points):
                dist = basic_functions.Dist_point_line_3D(pt=pt, start=COMDown,
                                                         end=COMDown, ite=10)
                if pt_id == 0: # assign a closest pt distance one the first loop
                    closest_point_dist = dist
                elif dist < closest_point_dist: #  TODO verifier cette condition: and pt_id == 0:
                    closest_point_dist = dist
                    point_id_lower = pt_id
                    
            knee.axis = all_knees_axis[j] 
            knee.find_closest_pt()
            print(knee.axis.POINTS[1])
            print(knee.closest_pt_id)
            
            steps = sequence(tot)
            point_id_lower = knee.closest_pt_id -steps[0]
            point_id_upper = knee.closest_pt_id +steps[1] 
            
            if point_id_lower < 0:
                point_id_upper = point_id_upper - point_id_lower
                point_id_lower = 0
    
            knees.append(readKnee(file_path))
            knee_tensors.append(knee.getTensor_ind_norm()[(point_id_lower):(point_id_upper)])
            all_knees_COM_intervals.append([point_id_lower, point_id_upper])
            j = j+1
    return [knees,knee_tensors,all_knees_COM_intervals,all_knees_axis]

""" 
Setup the information necessary to create output onehot of deep learning network
    
Parameters:
----------
knees : list
    List of individual legs from sample data (integral and original data)

all_knees_COM_intervals : list
    Intervals containing the knee within 1cm of a given length (tot)
    
all_knees_axis : list
    Position of the knee axis for each individual leg
        
Returns: 
-------
y_labels : list
    Target knee axis position index

y_intervals : list
    acceptable interval of detection (1+ or - 1cm)

"""
def output_initiate(all_knees_COM_intervals, knees, all_knees_axis):
    y_labels = np.empty([len(knees)])
    y_intervals = []                                 
    for knee_no in range(len(knees)): 
        Lower = all_knees_COM_intervals[knee_no][0]
        Upper = all_knees_COM_intervals[knee_no][1]
        print(Lower)
        print(Upper)
        knees[knee_no].axis = all_knees_axis[knee_no]        
        knees[knee_no].find_closest_pt() 
        knees[knee_no].find_closest_pt_intervall() 
        
        for i in range(len(knees[knee_no].closest_pt_intervall)):
            knees[knee_no].closest_pt_intervall[i] = knees[knee_no].closest_pt_intervall[i] - Lower
        
        y_labels[knee_no] = knees[knee_no].closest_pt_id - Lower
        y_intervals.append(knees[knee_no].closest_pt_intervall)   
    return [y_labels,y_intervals]

""" 
Setup the information necessary to create output onehot of deep learning network
    
Parameters:
----------
y_labels : list
    Target knee axis position index

y_intervals : list
    acceptable interval of detection (1+ or - 1cm)
    
    
class_number : int
    Number of classes for output onehot vectors. Corresponds to the number of points for one element from the knee tensor list
        
Returns: 
-------
y_test : list
    Output list for validation test

y_train : list
    Output list for system training

x_train : list
    Input list for validation test

x_test : list
    Input list for validation test

"""
def finalize_input_output(class_number, y_labels, y_intervals, knee_tensors):  
    #Create train and test sets for x and y
    Input = knee_tensors
    Output = y_labels
    x_y_val = [[Input[i], Output[i]] for i in range(len(Input))]
    threshold = int((0.2*len(x_y_val)))
    train_data = x_y_val[:-threshold]
    test_data = x_y_val[-threshold:]
    
    temp_train_x = splitXY(train_data, 0)
    temp_train_y = splitXY(train_data, 1)
    temp_test_x = splitXY(test_data, 0)
    temp_test_y = splitXY(test_data, 1)
    
    #Formattage Spécial
    test_x = np.empty([len(temp_test_x), len(temp_test_x[1]), len(temp_test_x[1][1])])
    train_x = np.empty([len(temp_train_x), len(temp_train_x[1]), len(temp_train_x[1][1])])
    
    #Put everything in one vector for x
    for i in range(len(temp_train_x)):
        train_x[i] = temp_train_x[i]
            
    for i in range(len(temp_test_x)):
        test_x[i] = temp_test_x[i]
        
    y_train = np_utils.to_categorical(temp_train_y, class_number)
    y_test = np_utils.to_categorical(temp_test_y, class_number)
    
    for i in range(len(y_intervals)):
        for j in range(len(y_intervals[i])):
            print(i)
            if i <= len(y_train) and y_intervals[i][j] <= class_number:
                y_train[i - 1][y_intervals[i][j] - 1] = 1
            elif i >= len(y_train) and y_intervals[i][j] <= class_number:
                y_test[i-len(y_train)][y_intervals[i][j] - 1] = 1
    
    return [y_test,y_train,test_x,train_x]


[knees,knee_tensors_ind,all_knees_COM_intervals_ind,all_knees_axis] = tensor_generate(40, files)
#[knee_tensors_gen,all_knees_COM_intervals_gen] = getTensor_gen_norm(40, knees, all_knees_axis)
[y_labels,y_intervals] = output_initiate(all_knees_COM_intervals_ind, knees, all_knees_axis)
[y_test,y_train,x_test,x_train] = finalize_input_output(40, y_labels, y_intervals, knee_tensors_ind)

# Model definition
# -- Initializing the values for the convolution neural network
nb_classes = 40
nb_epoch = 400
batch_size = 64
nb_filters = 30
nb_pool = 4
nb_conv1 = 3
nb_conv2 = 3
nb_conv3 = 3
shape_ord = (nb_classes, 15)

# Vanilla SGD
#sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
# Structure of a 2 level CNN
model = Sequential()
model.add(Conv1D(nb_filters, (nb_conv1), padding='same', 
                 input_shape=shape_ord))  # note: the very first layer **must** always specify the input_shape
model.add(Activation('relu'))
model.add(MaxPooling1D(pool_size = nb_pool))
model.add(Dropout(0.9))

model.add(Conv1D(nb_filters, (nb_conv2), padding='same', 
                 input_shape=shape_ord)) 
model.add(Activation('relu'))
model.add(Dropout(0.8))

model.add(Conv1D(nb_filters, (nb_conv3), padding='same', 
                 input_shape=shape_ord)) 
model.add(Activation('relu'))
model.add(Dropout(0.5))

model.add(Flatten())
model.add(Dense(nb_classes))
model.add(Activation('softmax'))
# Creating the graph of the model from the parameter that we set
model.compile(loss='categorical_crossentropy',
              optimizer=adam,
              metrics=['accuracy'])
#%% Training the model

hist = model.fit(x_train, y_train, batch_size=batch_size, 
                 epochs=nb_epoch, verbose=1, 
                 validation_data=(x_test, y_test))
##%% Showing the results
# Plot of the loss
plt.figure()
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.plot(hist.history['loss'])
plt.plot(hist.history['val_loss'])
plt.legend(['Training', 'Validation'])
# plot of the accuracy
plt.figure()
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.plot(hist.history['acc'])
plt.plot(hist.history['val_acc'])
plt.legend(['Training', 'Validation'], loc='lower right')\

train_probs = model.predict(x_train) 
test_probs = model.predict(x_test) 

#Knee only

    
#Padding
# basic example to understand: np.lib.pad([[1,1,1], [9,9,9]], ((2,3), (0,0)), 'constant', constant_values=0)
#Padding goes here but I took it out for now
#knee_1D_same_length = [] 
#knee_1D_labels_same_length = []
## Find the maximum knee_length to pad
#knee_max_length = max([knee_1D.shape[0] for knee_1D in knee_tensors])
## pad based on this information (up and down the same number of 0)
#for no, knee_1D in enumerate(knee_tensors):
#    fraction_compensation = 0
#    delta_length = knee_max_length - knee_1D.shape[0] # length of the current knee
#    # => fractionnal division
#    #print(delta_length)
#    # Padding 
#    knee_1D_same_length.append(np.lib.pad(knee_1D, 
#                                          ((0, delta_length), (0,0)), 
#                                          'constant', constant_values=(0,0)))
#                                            # ^ means we only pad up and down in this dimension



# Converting the Label to one_hot
#individual vectors (matrice of matrices)
#nb_classes = 2
#train_y = []
#text_y = []
#for i in range(len(temp_train_y)-1):
#    train_y.append(np_utils.to_categorical(temp_train_y[i], nb_classes))
#test_y = []
#for i in range(len(temp_test_y)-1):
#    test_y.append(np_utils.to_categorical(temp_test_y[i], nb_classes))
    
#for some reason your classification function did not work in this specific
# case so I made my own. I know for loops are horrible sorry team
#nb_classes = 2
#class_1 =[]
#class_2 =[]
#l = 0
#for i, y_label in enumerate(temp_train_y):
#    l = len(y_label) +l
#    for j in range(len(y_label)):
#        if y_label[j] == 1:
#            class_1 = np.concatenate((class_1 ,np.asarray([0])), axis=0)
#            class_2 = np.concatenate((class_2 ,np.asarray([1])), axis=0)
#        if y_label[j] == 0:
#            class_1 = np.concatenate((class_1 ,np.asarray([1])), axis=0)
#            class_2 = np.concatenate((class_2 ,np.asarray([0])), axis=0)
#
#train_y = np.zeros((len(class_1),2))
#for i in  range(len(class_1)):
#    if class_1[i] == 1:
#      train_y[i,0] = 1  
#    if class_2[i] == 1:
#      train_y[i,1] = 1  
#      
#nb_classes = 2
#class_1 =[]
#class_2 =[]
#for i, y_label in enumerate(temp_test_y):
#    print(len(y_label))
#    for j in range(len(y_label)):
#        if y_label[j] == 1:
#            class_1 = np.concatenate((class_1 ,np.asarray([0])), axis=0)
#            class_2 = np.concatenate((class_2 ,np.asarray([1])), axis=0)
#        if y_label[j] == 0:
#            class_1 = np.concatenate((class_1 ,np.asarray([1])), axis=0)
#            class_2 = np.concatenate((class_2 ,np.asarray([0])), axis=0)
#
#test_y = np.zeros((len(class_1),2))
#for i in  range(len(class_1)):
#    if class_1[i] == 1:
#      test_y[i,0] = 1  
#    if class_2[i] == 1:
#      test_y[i,1] = 1  
            
#train_y = np_utils.to_categorical(temp_train_y, nb_classes)
#test_y = np_utils.to_categorical(temp_test_y, nb_classes)

#Make sure the x input values are 3 dimensionnal, code doesnt work if not
#train_x = train_x.reshape(train_x.shape[0],train_x.shape[1],1)
#test_x = test_x.reshape(test_x.shape[0],test_x.shape[1],1)

##set parameters
#max_features = 260
#maxlen = 15
#batch_sizes = 32
#embedding_dims = 25
#filters = 250
#kernel_size = 3
#hidden_dims = 250
#epochs = 2
#
##Build model
#model = Sequential()
#model.add(Embedding(max_features, embedding_dims, input_length = maxlen))
#model.add(Dropout(0.2))
#model.add(Conv1D(filters, kernel_size, padding = 'valid', activation = 'relu', strides = 1))
#model.add(GlobalMaxPooling1D())
#
##vanilla hidden layer
#model.add(Dense(hidden_dims))
#model.add(Dropout(0.2))
#model.add(Activation('relu'))
#
##project unto 1 and sigmoid
#model.add(Dense(1))
#model.add(Activation('sigmoid'))
#
#model.compile(loss = 'binary_crossentropy', optimizer = 'adam', matrices = ['accuracy'])
#model.fit(train_x[1], train_y[1], batch_size = batch_sizes, epochs = epochs, validation_data = [test_x[1], test_y[1]])