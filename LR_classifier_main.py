# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 19:40:34 2017

@author: Jonathan
"""
from classifierFunctions import *
    
#trainingSetFolder=r"\\QNAP-SERVER001\Home Net Drive\Jonathan\School\University\Semester7\Projet4\Data\Data_regression\Clean"
trainingSetFolder = '/Users/stephaniedolbec/Desktop/Clean'
#ModelFile=r"\\QNAP-SERVER001\Home Net Drive\Jonathan\School\University\Semester7\Projet4\Data\Data_regression\Clean\classifierLR.pkl"
ModelFile = '/Users/stephaniedolbec/Desktop/classifierLR.pkl'
#testSetFolder=r"\\QNAP-SERVER001\Home Net Drive\Jonathan\School\University\Semester7\Projet4\Data\Data_regression\Reponse"
testSetFolder = '/Users/stephaniedolbec/Desktop/Reponse'
boolTrainModel = False
flipRightKnees = False
flipLeftKnees = True
#logFile=r"\\QNAP-SERVER001\Home Net Drive\Jonathan\School\University\Semester7\Projet4\Data\Data_regression\Reponse\logFileClassifierLR.log"
logFile = '/Users/stephaniedolbec/Desktop/Reponse/logFileClassifierLR.log'


if boolTrainModel:
    trainModel(trainingSetFolder, ModelFile)

predictions = classifyKnees(testSetFolder, ModelFile)

analyzeResults(predictions, flipLeftKnees, flipRightKnees, logFile)


