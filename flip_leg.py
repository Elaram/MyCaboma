# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 22:14:00 2017

@author: Jonathan
"""

import math
import numpy as np
import os
from PIL import Image

# function that takes a file, opens and reads it and then flips it and rewrites the knee. 2 
# Flip knees as as vtk or vtp

def flipKnee(file):
    #variables:
    # file is the name of filename
    # newFile, is the name of newFile
    
    #----------------------------------------------------------------------------------------------------------
    #The name of the new file will be written once we determine if the current file is a left or right leg
    
    #----------------------------------------------------------------------------------------------------------
    #Lets find out of this file contains a left knee or right knee
    
    #First lets determine if this file has a path of from a Mac (/) or Windows (\)
    # we will use rfind function which allows to look for specific sub_string from the end of the string
    #string.rfind(string2) will return index of start of string 2 if found, and -1 if it did not found
    if file.rfind("\\")!=-1:
        #it is a windows computer
        
        #Lets determine if this file is Left or Right, we will just compare the charachter after the last \ to D, 
        # or G, D=right G=left 
        if file[file.rfind("\\")+1]=="D":
            # it is a right Leg, lets change the name of new file for a left one
            # we will replace the D by a G and add _flipped
            newFile=file[:file.rfind("\\")+1]+"Flipped\\D"+file[file.rfind("\\")+2:]
        else:
            #it is a left leg
            # we will replace the G by a D
            newFile=file[:file.rfind("\\")+1]+"Flipped\\G"+file[file.rfind("\\")+2:]
        if not os.path.exists(newFile[:newFile.rfind("\\")]):
            os.makedirs(newFile[:newFile.rfind("\\")])
            
    elif file.rfind("/")!=-1:
        #it is a mac computer
        
        #Lets determine if this file is Left or Right, we will just compare the charachter after the last / to D, 
        # or G,D=right G=left
        if file[file.rfind("/")+1]=="D":
            # it is a right Leg, lets change the name of new file for a left one
            # we will replace the D by a G
            newFile=file[:file.rfind("/")+1]+"Flipped/D"+file[file.rfind("/")+2:]
        else:
            #it is a left leg
            # we will replace the G by a D
            newFile=file[:file.rfind("/")+1]+"Flipped/G"+file[file.rfind("/")+2:]
        if not os.path.exists(newFile[:newFile.rfind("/")]):
            os.makedirs(newFile[:newFile.rfind("/")])
    else:
            #We don't know which computer, but we now know the filename is in the same folder as this script
        
        #Lets determine if this file is Left or Right, we will just compare the first character to D or G 
        # D=right G=left
        if file[0]=="D":
            # it is a right Leg, lets change the name of new file for a left one
            # we will replace the D by a G
            newFile="Flipped/D"+file[1:]
        else:
            #it is a left leg
            # we will replace the G by a D
            newFile="Flipped/G"+file[1:]
        if not os.path.exists('Flipped'):
            os.makedirs('Flipped')

            
    
    #-----------------------------------------------------------------------------------------------------------
    #Lets determine if this file is a VTK or VTP or TIFF, this will affect the way we read thefile and write the new one
    #we will use rfind function, do determine if the file is a .VTK or .VTP
    if file.find(".vtk")!=-1:
        #-----------------------------------------------------------------------------------------------------------
        # We open the old file in read mode and the new file in write mode
        fOld=open(file,"r")
        fNew=open(newFile,"w")
        #-----------------------------------------VTK FILE------------------------------------------------------
        #----------------------------PATCH FOR WHEN POINTS ISN'T FIRST ARRAY---------------------------------
        # we will read the old file and look for the points, then close it and re open it
        for line in fOld:
            if not line.find("POINTS")==-1:
                firstSpace=line.find(" ")
                secondSpace=line.find(" ",firstSpace+1)
                numPoints=int(line[firstSpace+1:secondSpace])
                #read rest of file
                fOld.read()
        #close file
        fOld.close()
        # open file for next step
        fOld=open(file,"r")
               
        
        
        #we will read line by line untill end of file. we will be looking for specific substrings
        # the substring we will look for are "POINTS" , "Torsion", "Tangent","Normal","Binormal",
        #'name="Points"', the rest of file remains as is
        #we will use for line in fOld to read all the lines
        for line in fOld:
            #------------------------COORDINATES--------------------------------------------------------------
            if not line.find("POINTS")==-1:
                #we found the header for points array, lets write it to the file
                fNew.write(line)
                #Now we find the find the nunber of points, so we find first space and second space because file has
                # POINTS XX
                firstSpace=line.find(" ")
                secondSpace=line.find(" ",firstSpace+1)
                numPoints=int(line[firstSpace+1:secondSpace])
                
                #this is the array of the coordinates, this format has 3 set point per line, we are flipping on
                #minimum x point as mirros axis, we set min=inf
                # we will also find : NAME L2_NORM_RANGE LOCATION vtkDataArray, this is the norm of the coordinates
                
                minimumY=np.inf
                maxNorm=-np.inf
                minNorm=np.inf
                coordinates=[]
                for x in range(math.ceil(numPoints/3)):
                    tempStr=fOld.readline()
                    #we put the add the line of coordinates, we split when there are spaces, -2 because it spaces at
                    #end of line
                    coordinates.append([float(k) for k in tempStr[:-2].split(' ')])
                    #we then see if the first coordinate has minimum x
                    if coordinates[x][1]<minimumY:
                        minimumY=coordinates[x][1]
                    #lets check second coordinate and possibly third
                    if len(coordinates[x])>6:
                        if coordinates[x][4]<minimumY:
                            minimumY=coordinates[x][4]
                        if coordinates[x][7]<minimumY:
                            minimumY=coordinates[x][7]
                    elif len(coordinates[x])>3:
                        if coordinates[x][4]<minimumY:
                            minimumY=coordinates[x][4]
                #we now have coordinates and axes to flip, minimumX, To flip we will do 2*axisX- coordinatex
                # we will then write in the file
                for x in range(len(coordinates)):
                    #we flip first coordinate
                    coordinates[x][1]=2*minimumY-coordinates[x][1]
                    #we also check the norm
                    norm=(coordinates[x][0]**2+coordinates[x][1]**2+coordinates[x][2]**2)**(0.5)
                    if norm<minNorm:
                        minNorm=norm
                    if norm>maxNorm:
                        maxNorm=norm
                    
                    #we flip second and third coordinate if they exist
                    if len(coordinates[x])>6:
                        coordinates[x][4]=2*minimumY-coordinates[x][4]
                        #check the norm
                        norm=(coordinates[x][3]**2+coordinates[x][4]**2+coordinates[x][5]**2)**(0.5)
                        if norm<minNorm:
                            minNorm=norm
                        if norm>maxNorm:
                            maxNorm=norm
                            
                        coordinates[x][7]=2*minimumY-coordinates[x][7]
                        #check the norm
                        norm=(coordinates[x][6]**2+coordinates[x][7]**2+coordinates[x][8]**2)**(0.5)
                        if norm<minNorm:
                            minNorm=norm
                        if norm>maxNorm:
                            maxNorm=norm
                    elif len(coordinates[x])>3:
                        coordinates[x][4]=2*minimumY-coordinates[x][4]
                        #check the norm
                        norm=(coordinates[x][3]**2+coordinates[x][4]**2+coordinates[x][5]**2)**(0.5)
                        if norm<minNorm:
                            minNorm=norm
                        if norm>maxNorm:
                            maxNorm=norm
                    #now we write the file
                    tempStr=''
                    for y in range(len(coordinates[x])):
                        #here we create the string
                        if y==0:
                           tempStr=tempStr+str(coordinates[x][y])
                        else:
                            tempStr=tempStr+' '+str(coordinates[x][y])
                    # we add an enter and write the string in the file
                    tempStr=tempStr+'\n'
                    fNew.write(tempStr)
                    
                    #we write next 4 lines
                for x in range(4):
                    fNew.write(fOld.readline())
                #now we write norm line, lets read the line to stay up to data
#                    fOld.readline()
#                    fNew.write("DATA 2 "+str(minNorm)+' '+str(maxNorm)+' \n')
                
            #-----------------------TORSION-----------------------------------------------------------------
            elif not line.find("Torsion")==-1:
                #lets write this header
                fNew.write(line)
                #now we have torsion array, this needs to be mupltiplied by negative 1
                for x in range(math.ceil(numPoints/9)):
                    tempStr=fOld.readline()
                    #convert line in float, 
                    data=[float(k) for k in tempStr[:-2].split(' ')]
                    #format string to be written in new file
                    tempStr=''
                    for y in range(len(data)):
                        #here we multiply each number by -1 before adding to the string
                        if y==0:
                            tempStr=tempStr+str(data[y]*-1)
                        else:
                            tempStr=tempStr+' '+str(data[y]*-1)
                    # we add an enter and write the string in the file
                    tempStr=tempStr+'\n'
                    fNew.write(tempStr)
                    
            #---------------------TANGENT-------------------------------------------------------------------
            elif not line.find("Tangent")==-1:
                #lets write this header
                fNew.write(line)
                #now we have the tangent array, this has 3 points per line, we need to multiply the second and third 
                #component by -1
                for x in range(math.ceil(numPoints/3)):
                    tempStr=fOld.readline()
                    #convert line in float
                    data=[float(k) for k in tempStr[:-2].split(' ')]
                    # format the string
                    tempStr=''
                    for y in range(len(data)):
                        if y==0:
                            #multiply by -1
                            tempStr=tempStr+str(data[y]*-1)
                        elif not y==1 and not y==4 and not y==7:
                            #we multiple by -1
                            tempStr=tempStr+' '+str(data[y]*-1)
                        else:
                            tempStr=tempStr+' '+str(data[y])
                    # add an enter and write the string in file
                    tempStr=tempStr+'\n'
                    fNew.write(tempStr)
            #-----------------NORMAL, BINORMAL---------------------------------------------------------------
            elif not line.find("Normal")==-1 or not line.find("Binormal")==-1:
                #lets write header
                fNew.write(line)
                # now we write normal array, we just multiply second compenent by -1, 3 points per line
                for x in range(math.ceil(numPoints/3)):
                    tempStr=fOld.readline()
                    #convert line in float
                    data=[float(k) for k in tempStr[:-2].split(' ')]
                    #format the string
                    tempStr=''
                    for y in range(len(data)):
                        if y==0:
                            tempStr=tempStr+str(data[y])
                        elif y==1 or y==4 or y==7:
                            #multiply by -1
                            tempStr=tempStr+' '+str(data[y]*-1)
                        else:
                            tempStr=tempStr+' '+str(data[y])
                    #add an enter and write the strin in file
                    tempStr=tempStr+'\n'
                    fNew.write(tempStr)
            
            #-----------------------For rest of file we just write the line
            else:
                fNew.write(line)
                
        #We close the files because we are done
        fOld.close()
        fNew.close()
                   
    #---------------------------------------------------------------------------------------------------------    
    elif file.find(".vtp")!=-1:
        #---------------------------VTP FILE------------------------------------------------------------------
        #-----------------------------------------------------------------------------------------------------------
        # We open the old file in read mode and the new file in write mode
        fOld=open(file,"r")
        fNew=open(newFile,"w")
        
        
        #----------------------------PATCH FOR WHEN POINTS ISN'T FIRST ARRAY---------------------------------
        # we will read the old file and look for the points, then close it and re open it
        for line in fOld:
            if not line.find("NumberOfPoints")==-1:
                firstQuotation=line.find('"')
                secondQuotation=line.find('"',firstQuotation+1)
                numPoints=int(line[firstQuotation+1:secondQuotation])
                #read rest of file
                fOld.read()
        #close file
        fOld.close()
        # open file for next step
        fOld=open(file,"r")
        
        
        
        #we will read line by line untill end of file. we will be looking for specific substrings
        # the substring we will look for are "NumberOfPoints" , "Torsion", "Tangent","Normal","Binormal",
        #'name="Points"', the rest of file remains as is
        #we will use for line in fOld to read all the lines
        for line in fOld:
            #--------------------------------Determine number of Points---------------------------------------
            if not line.find("NumberOfPoints")==-1:
                #we are at the line with number the number of points, lets print it
                fNew.write(line)
                #Now lets find the number of points, the file will read NumberOfPoints="XX"
                #we will find the first ", and then " and convert the string in between into a number
                firstQuotation=line.find('"')
                secondQuotation=line.find('"',firstQuotation+1)
                numPoints=int(line[firstQuotation+1:secondQuotation])
                
            #----------------------------TORSION--------------------------------------------------------------
            elif not line.find("Torsion")==-1:
                #we found line containing the torsion array header, lets print it
                # we need to multiply by -1 the max and min
                # lets find the RangeMin
                firstQuotationMin=line.find('RangeMin="')+len('RangeMin="')
                secondQuotationMin=line.find('"',firstQuotationMin)
                minRange=float(line[firstQuotationMin:secondQuotationMin])
                #lets find the RangeMax
                firstQuotationMax=line.find('RangeMax="')+len('RangeMax="')
                secondQuotationMax=line.find('"',firstQuotationMax)
                maxRange=float(line[firstQuotationMax:secondQuotationMax])
                #now lets write the header
                fNew.write(line[:firstQuotationMin]+str(maxRange*-1)+line[secondQuotationMin:firstQuotationMax]+str(minRange*-1)+line[secondQuotationMax:])
                #now lets multiply by negative all the next numPoints/6 lines
                for y in range(math.ceil(numPoints/6)):
                    #get line from old file
                    tempStr=fOld.readline()
                    #convert line in float, start at 10 because there are 10 spaces, split str by the space
                    data=[float(k) for k in tempStr[10:].split(' ')]
                    #format string to be written in new file
                    tempStr='         '
                    for x in range(len(data)):
                        #here we multiply each number by -1 before adding to the string
                        tempStr=tempStr+' '+str(data[x]*-1)
                    # we add an enter and write the string in the file
                    tempStr=tempStr+'\n'
                    fNew.write(tempStr)
            
            #--------------------------TANGENT---------------------------------------------------------------
            elif not line.find("Tangent")==-1:
                #Now it gets even trickier. we need to put negative all the first and third compoenents of 
                # the tangent array
                #we found line containing the tangent array header, lets print it
                fNew.write(line)
                #now lets multiply by negative all the next numPoints/2 lines because there are 2 points per line
                for y in range(math.ceil(numPoints/2)):
                    #get line from old file
                    tempStr=fOld.readline()
                    #convert line in float, start at 10 because there are 10 spaces, split str by the space
                    data=[float(k) for k in tempStr[10:].split(' ')]
                    #format string to be written in new file
                    tempStr='         '
                    for x in range(len(data)):
                        #here we multiply each number by -1 the second and third component before adding to the string
                        if x!=1 and x!=4:
                            tempStr=tempStr+' '+str(data[x]*-1)
                        else:
                            tempStr=tempStr+' '+str(data[x])
                    # we add an enter and write the string in the file
                    tempStr=tempStr+'\n'
                    fNew.write(tempStr)
                    
            #-----------------------NORMAL, BINORMAL----------------------------------------------------------
            elif not line.find("Normal")==-1 or not line.find("Binormal"):
                #here we do samething for the normal and binormal array
                #Now we put negative the second component
                #lets write the header of the array
                fNew.write(line)
                #now lets multiply by negative all the next numPoints/2 lines because there are 2 points per line
                for y in range(math.ceil(numPoints/2)):
                    #get line from old file
                    tempStr=fOld.readline()
                    #convert line in float, start at 10 because there are 10 spaces, split str by the space
                    data=[float(k) for k in tempStr[10:].split(' ')]
                    #format string to be written in new file
                    tempStr='         '
                    for x in range(len(data)):
                        #here we multiply each number by -1 the first and third component before adding to the string
                        if x!=1 and x!=4:
                            tempStr=tempStr+' '+str(data[x])
                        else:
                            tempStr=tempStr+' '+str(data[x]*-1)
                    # we add an enter and write the string in the file
                    tempStr=tempStr+'\n'
                    fNew.write(tempStr)
            
            #--------------------COORDINATES------------------------------------------------------------------
            elif not line.find('Name="Points')==-1:
                #we found the coordinates, lets store the header, we need to minRange and MaxRange
                #lets set them temporarilt to minRange=inf, maxRange=-inf
                header=line
                minRange=np.inf
                maxRange=-np.inf
                #lets find position in header of minRange and MaxRange
                firstQuotationMin=header.find('RangeMin="')+len('RangeMin="')
                secondQuotationMin=header.find('"',firstQuotationMin)
                #lets find the RangeMax
                firstQuotationMax=header.find('RangeMax="')+len('RangeMax="')
                secondQuotationMax=header.find('"',firstQuotationMax)
                
                #now lets read all the next lines and put the data in a table
                #we find minimum of y coordinates, for net we set min=inf 
                minimumY=np.inf
                coordinates=[]
                for y in range(math.ceil(numPoints/2)):
                    tempStr=fOld.readline()
                    coordinates.append([float(k) for k in tempStr[10:].split(' ')])
                    #lets compare first x in the line to the minimum, if smaller it is new minimum
                    if coordinates[y][1]<minimumY:
                        minimumY=coordinates[y][1]
                    # now lets compare second y in the line to the min, if exist
                    # lets determine if exist, if the len of the data is more than 3 points we know it exist
                    if len(coordinates[y])>3:
                        if coordinates[y][4]<minimumY:
                            minimumY=coordinates[y][4]

                #we now have all the data, and the minimum x (the axis of to the mirror image)
                #now we flip 
                for y in range(len(coordinates)):
                    #we do first coordinate, we found out if it is smaller or larger than mirror axis 
                    # if smaller, we will add distance between point and axis to axis, if bigger we will subtract
                    # distance between point and axis from the axis
                    coordinates[y][1]=2*minimumY-coordinates[y][1]
                    #check for maxRange and minRange, it is the normalised point of coordinate
                    norm=(coordinates[y][0]**2+coordinates[y][1]**2+coordinates[y][2]**2)**(0.5)
                    if norm<minRange:
                        minRange=norm
                    if norm>maxRange:
                        maxRange=norm
    
                    # now we do if for second point if it exist in the line,
                    if len(coordinates[y])>3:
                        coordinates[y][4]=2*minimumY-coordinates[y][4]
                        norm=(coordinates[y][3]**2+coordinates[y][4]**2+coordinates[y][5]**2)**(0.5)
                        if norm<minRange:
                            minRange=norm
                        if norm>maxRange:
                            maxRange=norm
                
                #we print the header:
                fNew.write(header[:firstQuotationMin]+str(minRange)+header[secondQuotationMin:firstQuotationMax]+str(maxRange)+header[secondQuotationMax:])
                
                #now we write rest of data
                for y in range(len(coordinates)):
                    tempStr='         '
                    for x in range(len(coordinates[y])):
                        #here we create the string
                        tempStr=tempStr+' '+str(coordinates[y][x])
                    # we add an enter and write the string in the file
                    tempStr=tempStr+'\n'
                    fNew.write(tempStr)
                    
            #-----------------REST OF FILE--------------------------------------------------------------------
            else:
                #this is not a line with info that changes so we just reprint it
                fNew.write(line)
        #We close the files because we are done
        fOld.close()
        fNew.close()
    #--------------------------------------------------------------------------------------------------------- 
    elif file.find(".tiff")!=-1:
        #open old image
        imageOld = Image.open(file)
        # flip it
        imageNew = imageOld.transpose(Image.FLIP_LEFT_RIGHT)
        # save it
        imageNew.save(newFile)

    #-----------------------------------------------------------------------------------------------------------
    
    
#------------------------main----------------------------------------------------------
# path = r"C:\Users\Jonathan\Google Drive\Projet_GBM4900\04_RESSOURCES\DATA\Gauche_Droite"
# path=r"G:\Projet4\dataset_AI"
# files=os.listdir(path) 

# for i, file in enumerate(files):
   # if not "knee_axis" in file:
    # file_path = path + '\\' + file
    # print("flipping file:" + file_path)
    # flipKnee(file_path)
