#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 22:12:16 2017

@author: stephaniedolbec
"""
from LR_classifier_functions import *
from flip_leg import *
from basic_functions import *
import os

#%% PATH TO POLY-AI FOLDER
path_poly = '/Users/stephaniedolbec/Poly-AI/demo_dec_2017/'

#%%
trainingSetFolder = path_poly + 'Legs/'
ModelFile = path_poly + 'classifierLR.pkl'
testSetFolder = path_poly + 'Legs/'
boolTrainModel = True
logFile = path_poly + 'Legs/logFileClassifierLR.log'

if boolTrainModel:
    trainModel(trainingSetFolder, ModelFile, True)


path = path_poly + 'Legs/'
files = os.listdir(path) 

legs = []
#TODO change according to new dataset 
for file_no, file in enumerate(files):
        if "G" in file and ".vtk" in file: 
            flipKnee(path+file)

directory = [path_poly + 'Legs/']
final_directory = path_poly + 'Legs/Flipped/'
transferFiles(directory, final_directory, bool_Right=True, bool_Left=False)

#
#import read_legs
#import os
#import matplotlib.pyplot as plt
#import numpy as np
#from sklearn import linear_model
#from sklearn.metrics import mean_squared_error, r2_score
#import pickle
#from regression_knee_axis_main import toArray
#
#    
##%% Extract legs
#path_to_save = path_poly
#file_name = 'regression.pkl'
#
#path = path_poly + 'Legs/Flipped/'
#files = os.listdir(path) 
## Sort files in a natural alphabetical and numerical way: 
## files.sort(key=basic_function.natural_keys)
#
#legs = []
##TODO change according to new dataset 
#for file_no, file in enumerate(files):
#        if "stlcenterline" in file:
##            print(path + file)
#            legs.append(read_legs.readKnee(path + file))
#
##%% Extract knee axis and features to test
#
#features_list_temp, features_list = [], []
#CoM, ClosestPointToCOMZ, ClosestPointToAverageZ, MaxX, MinX, DerivativeX, \
#DerivativeY, DerivativeZ, AverageZ , Test_X= [], [], [], [], [], [], [], [], [],[]
#MinRadiusAboveCOM, MinR_between_COMs = [], []
#DerivativeRadius = []
#knee_axis = [] 
#
#legs = read_legs.getKneeAxis(legs)
#
#for i in range(len(legs)):
#    CoM.append(legs[i].CenterOfMass)   
#    ClosestPointToCOMZ.append(legs[i].ClosestPointToCOMZ)
#    ClosestPointToAverageZ.append(legs[i].ClosestPointToAverageZ)    
#    MaxX.append(legs[i].MaxX[1])
#    MinX.append(legs[i].MinX[1])
#    DerivativeX.append(legs[i].getMaximum(legs[i].DerivativeX[3]))
#    DerivativeY.append(legs[i].getMaximum(legs[i].DerivativeY[3]))
#    DerivativeZ.append(legs[i].getMaximum(legs[i].DerivativeZ[3]))
##    Test_X.append(legs[i].MaxXTest[1])
#    AverageZ.append(legs[i].AverageZ)
#    
#    # Minimal radius above the center of mass
#    MinRadiusAboveCOM.append(legs[i].getMinimum(legs[i].Radius[legs[i].ClosestPointToCOMZ:])[1])
#    
#    # Minimal radius between 2 COMs
#    closest_pt_belowCOM = legs[i].getClosestPoint(legs[i].Points,legs[i].COMBelowZ,3)
#    closest_pt_aboveCOM = legs[i].getClosestPoint(legs[i].Points,legs[i].COMAboveZ,3)
#    rayon = legs[i].Radius[closest_pt_belowCOM:closest_pt_aboveCOM]
#    MinR_between_COMs.append(legs[i].getMinimum(legs[i].Radius[closest_pt_belowCOM:closest_pt_aboveCOM])[1])
#    
#    DerivativeRadius.append(legs[i].DerivativeRadius[1])
#
#    # Knee axis
#    knee_axis.append(legs[i].PtKneeAxis)
#    
##features_list_temp.append(CoM)
#features_list_temp.append(ClosestPointToCOMZ)
##features_list_temp.append(ClosestPointToAverageZ)
##features_list_temp.append(MaxX)
##features_list_temp.append(MinX)
##features_list_temp.append(AverageZ)
##features_list_temp.append(MinRadiusAboveCOM)
##features_list_temp.append(MinR_between_COMs)
##features_list_temp.append(Test_X)
##features_list_temp.append(DerivativeRadius)
#
#for feature in features_list_temp:
#    if type(feature[0]) is not tuple:
#        features_list.append(toArray(feature))
#    else:
#        features_list.append(feature)
#        
##%% Regression to find knee axis        
#        
## Percentage of data to validate
#p_test = 20 # %
#
#n_test = int(p_test/100.0 * len(legs))
#n_features = len(features_list)
#coeff, variance = [], []
#for feature in range(n_features):
#    # Use 1 feature
#    X = features_list[feature]
#    
#    # Split the data into training/testing sets
#    X_train = X[:-n_test]
#    X_test = X[-n_test:]
#    
#    # Split the targets into training/testing sets
#    y_train = knee_axis[:-n_test]
#    y_test = knee_axis[-n_test:]
#    
#    # Create linear regression object
#    regr = linear_model.LinearRegression()
##    regr = linear_model.BayesianRidge()
##    regr = linear_model.HuberRegressor()	
#    
#    # Train the model using the training sets
#    regr.fit(X_train, y_train)
#    
#    # Make predictions using the testing set
#    y_pred = regr.predict(X_test)
#    y_pred = [int(y_pred[i]) for i in range(len(y_pred))] # ID is an integer
#    
#     # The coefficients
#    print('\nCoefficients:', regr.coef_)
#    coeff.append(regr.coef_)
#
#    # The mean squared error
#    print("Mean squared error: %.2f" % mean_squared_error(y_test, y_pred))
#    
#    # Explained variance score: 1 is perfect prediction
#    print('R2 score: %.2f' % r2_score(y_test, y_pred))
#    variance.append(r2_score(y_test, y_pred))
#    
#    plt.scatter(y_test, y_pred)
#    plt.title('y_pred vs y_test')
#    plt.xlabel('y_test')
#    plt.ylabel('y_pred')
#    plt.show()
#    
#    err = []
#    max_err = 50
#    good = 0
#    for i in range(n_test):
#        diff = abs(legs[len(legs)-n_test+i].Points[y_test[i],2] - legs[len(legs)-n_test+i].Points[y_pred[i],2])
#        err.append(diff)
#        if diff < max_err :
#            good = good + 1
#    score = good/n_test
#    
#plt.plot(err)
#plt.title("Absolute Error")
#plt.xlabel("Leg")
#plt.ylabel("Error (mm)")
#plt.show()
#
#save_coeff(path_to_save, file_name, regr)
#
#print("Percentage of legs with <", max_err, "mm distance to real knee axis :",\
#      int(score*100), "%")
#print("Max distance :", np.max(err), "mm")
#
## Show variance between several features    
##plt.plot(variance)
##plt.title("Variance (1 = perfect prediction)")
##plt.show()
#
