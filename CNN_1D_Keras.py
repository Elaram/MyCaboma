
#%% Processing of the vector so that we can feed them into the neural network
def create_tensor_for_all_knees(
        knees,
        do_normalize=False):
    """
    Creating the tensor with all caracteristics concatenated in one np.array
    for all knees

    Parameters:
    ----------
    knees : list
        List of all knee objects

    Returns:
    -------
    knee_caract_1D : list
        List of all the caracteristics for the corresponding knee
    """
    knee_caract_1D = []
    for knee in knees:
        knee_caract_1D.append(knee.get_tensor(do_normalize=do_normalize))
    return knee_caract_1D


def pad_knee_caract_and_actuate_closest_pt_position(
        knee_caract_1D,
        closest_pt,
        subsection_len=10):
    """
    Padding the knee caracteristic list with 0 to be sure all the knees are the same
    length as the longest knee list
    AND keeping the closest_pt at the rigth position based on the padding length

    Parameters:
    ----------
    knee_caract_1D : list
        List of all the caracteristics for the corresponding knee
    closest_pt : list
        A list of the closest pt id from the knee to the knee axis
    subsection_len : int, optionnal
        Extra padding to make sure we can select the last pixel as a
        sub_centerline of dimension sub_centerline_len on both side of this pixel
        Note: we add one to the subsection len so that we can have odd length of
              subsections so that there is a central value (which is the value on
              which is based the classification)

    Returns:
    -------
    knee_caract_same_length : list
        List of all the knee caract padded to all be as long as the longest knee
    closest_pt_same_len : list
        List of the actuated value of the closest pt on the centerline to the
        knee axis
    knee_1D_pos_after_padding : list of list -> [knee_len, subsection_len]
        Contain the information to know the position of the knee in the new
        padded list

    Example:
    -------
    >>> np.lib.pad([[1,1,1], [9,9,9]], ((1,2), (0,0)), 'constant', constant_values=0)
    >>> array([[0, 0, 0],
               [1, 1, 1],
               [9, 9, 9],
               [0, 0, 0],
               [0, 0, 0]])
    """
    # Find the maximum knee_length so that we can pad all the knees up to this size
    knee_max_length = max([knee_1D.shape[0] for knee_1D in knee_caract_1D])

    # Pad based on this information (up and down the same number of 0)
    closest_pt_same_len = []
    knee_1D_same_len = []
    knee_1D_pos_after_padding = []
    for knee_1D, one_closest_pt in zip(knee_caract_1D, closest_pt):
        """ Padding a knee"""
        fraction_compensation = 0
        curent_knee_len = knee_1D.shape[0]
        # Round the odd len division by two to the closest floor value (all the .5)
        delta_len = (knee_max_length - curent_knee_len)
        # Padding up and down with zeros
        knee_1D_same_len.append(
                np.lib.pad(knee_1D,
                ((subsection_len,
                delta_len + (subsection_len + 1) + fraction_compensation), (0, 0)),
                'constant', constant_values=(0, 0)))
        """keeping the closest_pt id at the right position based on the padding"""
        closest_pt_same_len.append(one_closest_pt + subsection_len)

        knee_1D_pos_after_padding.append([curent_knee_len, subsection_len])

    return knee_1D_same_len, closest_pt_same_len, knee_1D_pos_after_padding


def create_subsection_of_a_1D_caracteristic_list(
        knee_caract,
        subsection_len=10):
    """
    Create a subsection of the caracteristics for a knee. The subsection length is
    `subsection_len` and will be classify depending on its center point

    Parameters:
    ----------
    knee_caract : list
        A list of all the caracteristics in a knee
    subsection_len : int, optionnal
        Extra padding to make sure we can select the last pixel as a sub_centerline
        of dimension sub_centerline_len on both side of this pixel

    Returns:
    -------
    knee_caract_in_section : nparray
        divide the caracteristique list in subsection for all the values
    """
    knee_caract = np.array(knee_caract)
    subsec_half_len = math.ceil(subsection_len/2)
    knee_caract_in_section = [knee_caract[pos-subsec_half_len:pos+subsec_half_len+1] \
              for pos in range(subsection_len, len(knee_caract) - subsection_len)]

    return knee_caract_in_section


def create_train_and_test_set(
        closest_pt_actuacted,
        knee_1D_pos_after_padding,
        num_test_val=1,
        acceptable_range=0,
        random_choice=True):
    """
    Create the full train and test set by appending all the subsection to
    X_train_and_test and by appending their class
    (knee_axis in the center=1 or not=0)

    Parameters:
    ----------
    closest_pt_actuacted : list
        List of the actuated value of the closest pt based on the extra padding
        to make all centerlines the same length and on the subsection_len we want
    knee_1D_pos_after_padding : list of list -> [knee_len, subsection_len]
        Contain the information to know the position of the knee in the new padded
        list
    num_test_val : int or str (Use 'ALL' to select them all)
        The number of test subsection to take in the centerline for the test set
    random_choice: bool
        if you want to select the subsections that are not on the knee_axis id in a
        random manner

    Returns:
    -------
    X_train_and_test : np.array
        Array of All the subsection of the knee caracteristic list availible
    y_train_and_test : np.array
        Array of All the classification (knee_axis in the center=1 or not=0) or each
        elements in the array X_train_and_test
    """
#    knee_no = 0
#    knee_len, subsection_len = knee_1D_pos_after_padding[knee_no]                    # THIS WAS HERE IN THE FIRST TEST AND SHOULD BE IN THE LOOP
    X_train_and_test, y_train_and_test = [], []

    for knee_no, (knee, closest_pt) in enumerate(zip(knee_1D_same_length,
                                                     closest_pt_actuacted)):

        knee_len, subsection_len = knee_1D_pos_after_padding[knee_no]

#        not_axis = list(range(knee.shape[0]))
#        in_knee_not_axis = not_axis[subsection_len : (knee_len + subsection_len)]
#        del(not_axis[subsection_len + closest_pt])

#        if num_test_val == 'ALL':
#            not_axis_picked_val = in_knee_not_axis                                    # TODO: USE THE AXIS INTERVALL OF TOLERANCE TO BE ABLE TO PICK MORE POINTS (ADD OTHER CLASSES BASED ON THE PROXIMITY?, OR PENALISE FOR CLASSIFICATION MORE DISTANT TO .....)
#        elif random_choice:
#            not_axis_picked_val = random.sample(population=in_knee_not_axis,
#                                                k=num_test_val)

        knees_subsection = \
            create_subsection_of_a_1D_caracteristic_list(
                    knee, knee_1D_pos_after_padding[knee_no][1])

        for subsection_no, subsection in enumerate(knees_subsection):
            # Tagging the subsection if they have a knee_axis at their center or not
#            if subsection_no in range(closest_pt - acceptable_range,
#                                      closest_pt + acceptable_range + 1):
#                print('AXIS: ', subsection_no)
#                X_train_and_test.append(subsection)
#                y_train_and_test.append(1)

            # For the one leg test
            if num_test_val == 'ALL': # elif so that it doesn't take the
                                      # subsections that are in the axis region       # CEST ICI QU'IL FAUT QUE J'ARRANGE (COMBIEN DE POINTS JE VEUX PRENDRE, IMPORTANT DE CHANGER LE NOMBRE DE POINTS PRIS SI JE CHOISI D'EN PRENDRE UN AUTRE NOMBRE QUE 3)
                if subsection_no < closest_pt:
                    X_train_and_test.append(subsection)
                    y_train_and_test.append(0)
                elif subsection_no >= closest_pt:
                    X_train_and_test.append(subsection)
                    y_train_and_test.append(1)

#            else: # This else is for the classification of 3 classes
#                # Pt up of the knee axis
#                if subsection_no in range(closest_pt-2 - acceptable_range,
#                                          closest_pt-2 + acceptable_range + 1):
#                    X_train_and_test.append(subsection)
#                    y_train_and_test.append(0)
#                    print('UP: ', subsection_no)
#                # Pt down of the knee axis
#                elif subsection_no in range(closest_pt+2 - acceptable_range,
#                                            closest_pt+2 + acceptable_range + 1):
#                    print('DOWN: ', subsection_no)
#                    X_train_and_test.append(subsection)
#                    y_train_and_test.append(2)
#
#            else: # else not in the training set
#                  # this ELSE is for binary classification
#                # if is in the pick val list and is not only zeros
#                if subsection_no in not_axis_picked_val:
#                    X_train_and_test.append(subsection)
#                    # Take only a subsample for the training
#                    y_train_and_test.append(0)

    return np.array(X_train_and_test), np.array(y_train_and_test)


def split_train_and_test_set(
        X,
        y,
        percent_in_train=0.8):
    """
    Split the train and test set at a certain treshold (percent_in_train)

    Parameters:
    ----------
    X : np.array
        An array with all the knee caract arrays subsections
    y : np.array
        The array with all the classifications for all the subsections
    percent_in_train : float
        The percentage of element we want in the training set (will be used as a
        threshold to split the train and test set)

    Returns:
    -------
    X_train : np.array
        An array with the training knee_caracteristics subsections
    X_test : np.array
        An array with the testing knee_caracteristics
    y_train : np.array
        An array with the classification for every subsections in the train set
    y_test : np.array
        An array with the classification for every subsections in the test set
    """
    number_of_train_val = round(percent_in_train*X.shape[0])
    X_train = X[:number_of_train_val]
    X_test = X[number_of_train_val:]
    y_train = y[:number_of_train_val]
    y_test = y[number_of_train_val:]

    return X_train, X_test, y_train, y_test




#%%
# =============================================================================
# TEST on only one centerline
# =============================================================================
# TRY ON A LEG THAT WAS IN THE TRAINING SET:
#def test_one_leg(knee, closest_pt, DO_SHOW=False):
#    """
#
#    """
#    print('closest_pt', closest_pt)
#
#    knee_caract_1D = create_tensor_for_all_knees(knee)
#
#    knee_1D_same_length, closest_pt_actuacted, knee_1D_pos_after_padding = \
#        pad_knee_caract_and_actuate_closest_pt_position(knee_caract_1D,
#                                                        closest_pt,
#                                                        subsection_len=80)
#
#    X_train_and_test, y_train_and_test = \
#        create_train_and_test_set(closest_pt_actuacted,
#                                  knee_1D_pos_after_padding,
#                                  num_test_val='ALL')
#
#    X_train, _, y_train, _ = split_train_and_test_set(X_train_and_test,
#                                                     y_train_and_test,
#                                                     percent_in_train=1)
#
#    y_train = np_utils.to_categorical(y_train)
#    predictions_proportion = model.predict(X_train)
#    predictions = predictions_proportion.argmax(-1)
#
##    if DO_SHOW:
##        # Show all the prediction with the image of the matrices of the subsection
##        # of the leg
##        for i, prediction in enumerate(predictions):
##            plt.figure(figsize=(4, 2))
##            plt.imshow(X_train[i], interpolation='nearest', vmin=0, vmax=2)
##            plt.text(0, 0,
##                     'i: {}, prediction_proportion: {}, prediction: {}, real_val: {}'.\
##                     format(i, predictions_proportion[i], prediction, y_train[i][1]),
##                     color='black', bbox=dict(facecolor='white', alpha=1))
##            plt.axis('off')
##            plt.show()
#
#    return predictions_proportion, predictions, y_train



path_one_leg = r"C:\Users\alexa\Documents\CODING_A2017_POLY\Projet_4\Poly-AI\TEST_leg/"

knees_TEST, knees_axis_TEST = read_legs.read_training_data(path_one_leg)

closest_PTs_TEST = []
for knee, knee_axis in zip(knees_TEST, knees_axis_TEST):
    closest_PTs_TEST.append(knee.find_closest_pt_to_axis_3D(knee_axis)[0])

prediction_proportion_TEST = []
prediction_TEST = []
all_y_train_TEST = []

#for knee, closest_PT in zip(knees_TEST, closest_PTs_TEST):
#    pred_prop, pred, TEST_y_train = test_one_leg(knee=[knee], closest_pt=[closest_PT])


#    knee = [knee]
#    closest_pt = [closest_PT]

#
#print('closest_pt', closest_pt)

knee_caract_1D = create_tensor_for_all_knees(knees_TEST)

knee_1D_same_length, closest_pt_actuacted, knee_1D_pos_after_padding = \
    pad_knee_caract_and_actuate_closest_pt_position(knee_caract_1D,
                                                    closest_PTs_TEST,
                                                    subsection_len=80)

X_train_and_test, y_train_and_test = create_train_and_test_set(closest_pt_actuacted, knee_1D_pos_after_padding, num_test_val='ALL')

X_train, _, y_train, _ = split_train_and_test_set(X_train_and_test,
                                                  y_train_and_test,
                                                  percent_in_train=1)

y_train_one_hot = np_utils.to_categorical(y_train)
predictions_proportion = model.predict(X_train)
predictions = predictions_proportion.argmax(-1)

#    DO_SHOW = True
#    if DO_SHOW:
#        # Show all the prediction with the image of the matrices of the subsection
#        # of the leg
#        for i, prediction in enumerate(predictions):
#            plt.figure(figsize=(4, 2))
#            plt.imshow(X_train[i], interpolation='nearest', vmin=0, vmax=2)
#            plt.text(0, 0,
#                     'i: {}, prediction_proportion: {}, prediction: {}, real_val: {}'.\
#                     format(i, predictions_proportion[i], prediction, y_train[i][1]),
#                     color='black', bbox=dict(facecolor='white', alpha=1))
#            plt.axis('off')
#            plt.show()



#    print('pred_prop', pred_prop.shape)
#    print('pred', pred.shape)
#    print('TEST_Y_train', TEST_y_train.shape)
#    prediction_proportion_TEST.append(pred_prop)
#    print('prediction_proportion_TEST', prediction_proportion_TEST[0].shape)
#    prediction_TEST.append(pred)
#    print('prediction_TEST', prediction_TEST[0].shape)
#    all_y_train_TEST.append(TEST_y_train.argmax(-1))
#    print('all_y_train_TEST', all_y_train_TEST[0].shape)
#    allo = np.vstack((all_y_train_TEST[0], prediction_TEST[0]))
#%%






























# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 20:44:15 2017

@author: alexa
"""
# Our implemented functions and classes
from importlib import reload
import read_legs
import basic_functions
reload(read_legs)
reload(basic_functions)

#Import the required libraries
import numpy as np
import matplotlib.pyplot as plt
import random
import os
import math

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Conv1D
from keras.layers.pooling import MaxPooling1D
from keras.utils import np_utils
from keras.optimizers import SGD, Adagrad, Adadelta, Adam, Adamax, Nadam
from keras import backend as K
from keras.layers import BatchNormalization
from keras.models import load_model


#%%
# =============================================================================
# FLIP THE KNEE TO TRAIN ONLY ON ONE SIDE
# =============================================================================







# %% Main:

path = r"C:\Users\alexa\Documents\CODING_A2017_POLY\Projet_4\Poly-AI\dataset_AI/"

knees, knees_axis = read_legs.read_training_data(path=path)


closest_pt = [knee.find_closest_pt_to_axis_3D(knee_axis)[0] \
              for knee, knee_axis in zip(knees, knees_axis)]

knee_caract_1D = create_tensor_for_all_knees(knees, do_normalize=True)

knee_1D_same_length, closest_pt_actuacted, knee_1D_pos_after_padding = \
    pad_knee_caract_and_actuate_closest_pt_position(knee_caract_1D,
                                                    closest_pt,
                                                    subsection_len=80)
ACCEPTABLE_RANGE = 0
X_train_and_test, y_train_and_test = \
    create_train_and_test_set(closest_pt_actuacted,
                              knee_1D_pos_after_padding,
                              num_test_val= 'ALL',     #2*ACCEPTABLE_RANGE+1,
                              acceptable_range=ACCEPTABLE_RANGE)                      # NOTE: on ne devrait pas prendre toutes les valeurs pour l'entrainement
                                                                                      # Choisir lesquels seraient préférables a prendre selon notre oeil
X_train, X_test, y_train, y_test = \
    split_train_and_test_set(X_train_and_test,
                             y_train_and_test)



"nb_classes = 10" # pour tester le CNN avec les MNIST data set
nb_classes = 2  # ???: TEST
y_train = np_utils.to_categorical(y_train, nb_classes)
y_test = np_utils.to_categorical(y_test, nb_classes)
# Define the shape of the input that will be fed to the neural network
shape_ord = (X_train.shape[1], X_train.shape[2])

#%% Initialisation of the model parameters and structure
                                                                                      # NOTE: IL Y A DES CHANGEMENTS MAJEURS DE RÉSULTATS EN FONCTION DE LA VALEUR DU SEED RANDOM QUI EST DONNÉ....? EST-CE QUE C'EST CAR OVERFIT RAPIDEMENT SUR QUELQUE CHOSE MAIS IL Y A POSSIBILITÉ QUE CLASSE BIEN OU C'EST JUSTE DE LA MERDE?
# Model definition
def model_train(
        nb_classes,
        y_train,
        y_test,
        shape_ord):
    """
    """
    nb_epoch = 10
    batch_size = 60
    nb_filters_1 = 20
#    nb_filters_2 = 20
    nb_pool = 2
    nb_conv1 = 3
#    nb_conv2 = 15

    # OPTIMIZERS
#    optimizer = SGD(lr=0.0001, decay=1e-6, momentum=0.9, nesterov=True)
#    optimizer = Adagrad(lr=0.00001, epsilon=1e-08, decay=0.0)
#    optimizer = Adadelta(lr=1.0, rho=0.95, epsilon=1e-08, decay=0.0)
    optimizer = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

    # Structure of a 2 level CNN
    model = Sequential()
    model.add(Conv1D(nb_filters_1, (nb_conv1), padding='valid',
                     input_shape=shape_ord))
    model.add(Activation('relu'))
    model.add(MaxPooling1D(pool_size=(nb_pool)))
    model.add(Dropout(0.9))

#    model.add(Conv1D(nb_filters_2, (nb_conv2), padding='valid'))
#    model.add(Activation('relu'))
#    model.add(Dropout(0.9))

    model.add(Flatten())
    model.add(Dense(nb_classes))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])

    #%Training the model
    hist = model.fit(X_train, y_train, batch_size=batch_size,
                     epochs=nb_epoch, verbose=1,
                     validation_data=(X_test, y_test))
    return hist, model


hist, model = model_train(nb_classes=nb_classes,
                          y_train=y_train,
                          y_test=y_test,
                          shape_ord=shape_ord)

# SAVE MODEL
#model.save('model1.h5')
#model_load_1 = keras.models.load_model('model1.h5')
#slice = 10
#allo = model_load_1.predict(X_test[:slice])


#%% Showing the training results
# Plot of the loss
plt.figure()
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.plot(hist.history['loss'])
plt.plot(hist.history['val_loss'])
plt.legend(['Training', 'Validation'])
# plot of the accuracy
plt.figure()
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.plot(hist.history['acc'])
plt.plot(hist.history['val_acc'])
plt.legend(['Training', 'Validation'], loc='lower right')

#%%
# Evaluating the model on the test data
loss, accuracy = model.evaluate(X_test, y_test, verbose=0)
print('Test Loss:', loss)
print('Test Accuracy:', accuracy)

#%%
slice = 100
predictions = model.predict(X_test[:slice]).argmax(-1)
prediction_proportion = model.predict(X_test[:slice])

for i, prediction in enumerate(predictions):
    plt.figure(figsize=(4, 2))
    plt.imshow(X_test[i], interpolation='nearest', vmin=0, vmax=2)
    plt.text(0, 0,
             'i: {}, prediction_proportion: {}, prediction: {}, real_val: {}'.\
             format(i, prediction_proportion[i], prediction, y_test[i][1]),
             color='black', bbox=dict(facecolor='white', alpha=1))
    plt.axis('off')
    plt.show()










