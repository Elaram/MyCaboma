# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 19:40:34 2017

@author: Jonathan
"""

from read_VTK_VTP import *
from sklearn import neighbors
import matplotlib.pyplot as plt
import random
import numpy as np

#------------------------main----------------------------------------------------------
path=r"C:\Users\Jonathan\Google Drive\Projet_GBM4900\04_RESSOURCES\DATA\Gauche_Droite"
#path=r"C:\Users\Jonathan\Google Drive\Projet_GBM4900\04_RESSOURCES\DATA\Archived_Data\dataset_AI"
files=os.listdir(path) 
knees=extractKnees(path)

#extract select fearures
data=np.zeros((len(knees),3))
for i in range(len(knees)):
    if knees[i].KneeType=='R':
        data[i,:]=[knees[i].DifferenceMaxMinY,knees[i].DifferenceCOMY,0]
    else:
        data[i,:]=[knees[i].DifferenceMaxMinY,knees[i].DifferenceCOMY,1]


index = [[i] for i in range(len(knees))]
MaxTests=100
MaxNeighbours=100
results=np.zeros((MaxTests, MaxNeighbours))

for t in range(MaxTests):
    shuffle(index) #randomize data set
    indexCut=int(len(knees)*0.80) #80% for training, 20% validation
    indTrain=np.array(index[:indexCut]) # indexes for trainig
    indTest=np.array(index[indexCut:]) #index for validation
    trainingSet = data[indTrain[:,0],:2] 
    trainingTarget = data[indTrain[:,0],2]
    testSet=data[indTest[:,0],:2]
    testTarget=data[indTest[:,0],2]
    print( 'Test'+str(t))
    for n in range(MaxNeighbours):
        if n:
            clf=neighbors.KNeighborsClassifier(n,weights='distance')
            clf.fit(trainingSet,trainingTarget) #train
            predictions=clf.predict(testSet) #predict
            successRate=(len(predictions)-sum(abs(testTarget-predictions)))/len(predictions) #calculate succes rate
            print('Neighbours:'+str(n)+' Success Rate:' + str(successRate))
            results[t,n]=successRate #store in resluts matrix
AverageR=np.average(results,axis=0) #calculate aveage

print('we should use model with '+ str(np.argmax(AverageR))+' neighbours. It has '+ str(AverageR[np.argmax(AverageR)])+'% succes on average')

