#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 19:34:36 2017

@author: stephaniedolbec
"""
from regression_knee_axis_main import *
from LR_classifier_functions import *
from basic_functions import *
import os

#%% PATH TO POLY-AI FOLDER
path_poly = '/Users/stephaniedolbec/Poly-AI/demo_dec_2017/'

#%% Classifier    
ModelFile = path_poly + '/classifierLR.pkl'
testSetFolder = path_poly + 'Legs_final'
flipRightKnees = False
flipLeftKnees = True
logFile = path_poly + 'Legs_final/logFileClassifierLR.log'

predictions = classifyKnees(testSetFolder, ModelFile)
analyzeResults(predictions, flipLeftKnees, flipRightKnees, logFile)

directory=[path_poly + "Legs_final/"]
final_directory=path_poly+"Legs_final/Flipped/"
transferFiles(directory, final_directory, bool_Right=True, bool_Left=False)

#%% Find Knee Axis
# Extract model
path_to_model = path_poly + "regression.pkl"
regr_test = import_coeff(path_to_model)

# Extract legs
path_to_legs = path_poly + "Legs_final/Flipped/"
leg_v = extract_leg(path_to_legs)

# extract features and answer (real knee axis)
#TODO write function to extract features
feature1, answer, leg_ID = [], [], []
features_list_temp, features_list = [], []
for i in range(len(leg_v)):   
    feature1.append(leg_v[i].ClosestPointToCOMZ)
    features_list_temp.append(feature1)
    answer.append(leg_v[i].PtKneeAxis)
    leg_ID.append(leg_v[i].ID)

for feature in features_list_temp:
    if type(feature[0]) is not tuple:
        features_list.append(toArray(feature))
    else:
        features_list.append(feature)
        
for feature in range(len(features_list)):
    X_v = features_list[feature]

y_v = regr_test.predict(X_v)
y_v = [int(y_v[i]) for i in range(len(y_v))] # ID is an integer
dist_to_real_ka =[(answer[i]-y_v[i]) for i in range(len(answer))]
relative_err = [abs(dist_to_real_ka[i])/answer[i] * 100 for i in range(len(answer))]

# save results in log file
logFile = path_poly + "Legs_final/logFileKneeAxisFinder.log"
logF = open(logFile,'w')
for i in range(len(y_v)):
    logF.write("Results for leg " + str(leg_ID[i]))
    print("\nResults for leg", leg_ID[i])
    
    logF.write("\n\tPredicted ID : " + str(y_v[i]) + "\n\tReal ID : " +  str(answer[i]))
    print("\tPredicted ID :", y_v[i], "\n\tReal ID : ",  answer[i])
    
    logF.write("\n\tDistance to real knee axis : " + str(dist_to_real_ka[i]) + " mm")
    print("\tDistance to real knee axis :", dist_to_real_ka[i], "mm")
    
    logF.write("\n\tRelative error : " + str(int(relative_err[i])) + " %\n\n")
    print("\tRelative error :", relative_err[i], "%")
    
print("\n Saved data to log file :", logFile)
logF.close()

