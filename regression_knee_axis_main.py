#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 13:58:35 2017

@author: stephaniedolbec
"""
import read_legs
import os
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
import pickle

#%% PATH TO POLY-AI FOLDER
path_poly = "/Users/stephaniedolbec/Poly-AI/demo_dec_2017/"

#%% Useful functions

def extract_leg(path):
    """
    Extraction of knee axis from all legs in a defined path to folder. 
    Only used for model training.

    Parameters:
    ----------
    path : string
        path where all leg files are

    Returns:
    -------
    read_legs.getKneeAxis(leg_v) : integer
        Knee axis position for each leg
    """
    files = os.listdir(path) 
    #TODO change according to new dataset 
    leg_v = []
    for file_no, file in enumerate(files):
            if "stlcenterline" in file:
                leg_v.append(read_legs.readKnee(path + file))
    # get knee axis
    leg_v = read_legs.getKneeAxis(leg_v)
    return read_legs.getKneeAxis(leg_v)


def toArray(x):
    """
    Transforms 1D features to 2D features.

    Parameters:
    ----------
    x : feature (list or integer)
        Feature to transform

    Returns:
    -------
    np.array(x).reshape(-1,1) : np.array
        Transformed feature
    """
    return(np.array(x).reshape(-1,1))
    
    
def save_coeff(path, file_name, obj_to_save):
    """
    Save coefficient values in a file

    Parameters:
    ----------
    path : string
        Path to save file
    file_name : string
        File name
    obj_to_save : linear model
        Model to save
    """
    with open(path+file_name, 'wb') as f:
        pickle.dump(obj_to_save, f)


def import_coeff(path):
    """
    Read and import coefficient values from a file

    Parameters:
    ----------
    path : string 
        Path to model file

    Returns:
    -------
    pickle.load(f)  : linear model
        Imported linear model
    """
    with open(path_poly + file_name, 'rb') as f:
        return pickle.load(f)  
    
    
#%% Extract legs
file_name = 'regression.pkl'

path = path_poly + "/Legs/Flipped/"
files = os.listdir(path) 

legs = []
#TODO change according to new dataset 
for file_no, file in enumerate(files):
        if "stlcenterline" in file: #or ".aop." in file or ".obj." in file or ".ply." in file:
#            print(path + file)
            legs.append(read_legs.readKnee(path + file))

#%% Extract knee axis and features to test

features_list_temp, features_list = [], []
CoM, ClosestPointToCOMZ, ClosestPointToAverageZ, MaxX, MinX, DerivativeX, \
DerivativeY, DerivativeZ, AverageZ , Test_X= [], [], [], [], [], [], [], [], [],[]
MinRadiusAboveCOM, MinR_between_COMs = [], []
DerivativeRadius = []
knee_axis = [] 

legs = read_legs.getKneeAxis(legs)

for i in range(len(legs)):
    CoM.append(legs[i].CenterOfMass)   
    ClosestPointToCOMZ.append(legs[i].ClosestPointToCOMZ)
    ClosestPointToAverageZ.append(legs[i].ClosestPointToAverageZ)    
    MaxX.append(legs[i].MaxX[1])
    MinX.append(legs[i].MinX[1])
    DerivativeX.append(legs[i].getMaximum(legs[i].DerivativeX[3]))
    DerivativeY.append(legs[i].getMaximum(legs[i].DerivativeY[3]))
    DerivativeZ.append(legs[i].getMaximum(legs[i].DerivativeZ[3]))
#    Test_X.append(legs[i].MaxXTest[1])
    AverageZ.append(legs[i].AverageZ)
    
    # Minimal radius above the center of mass
    MinRadiusAboveCOM.append(legs[i].getMinimum(legs[i].Radius[legs[i].ClosestPointToCOMZ:])[1])
    
    # Minimal radius between 2 COMs
    closest_pt_belowCOM = legs[i].getClosestPoint(legs[i].Points,legs[i].COMBelowZ,3)
    closest_pt_aboveCOM = legs[i].getClosestPoint(legs[i].Points,legs[i].COMAboveZ,3)
    rayon = legs[i].Radius[closest_pt_belowCOM:closest_pt_aboveCOM]
    MinR_between_COMs.append(legs[i].getMinimum(legs[i].Radius[closest_pt_belowCOM:closest_pt_aboveCOM])[1])
    
    DerivativeRadius.append(legs[i].DerivativeRadius[1])

    # Knee axis
    knee_axis.append(legs[i].PtKneeAxis)
    
#features_list_temp.append(CoM)
features_list_temp.append(ClosestPointToCOMZ)
#features_list_temp.append(ClosestPointToAverageZ)
#features_list_temp.append(MaxX)
#features_list_temp.append(MinX)
#features_list_temp.append(AverageZ)
#features_list_temp.append(MinRadiusAboveCOM)
#features_list_temp.append(MinR_between_COMs)
#features_list_temp.append(Test_X)
#features_list_temp.append(DerivativeRadius)

for feature in features_list_temp:
    if type(feature[0]) is not tuple:
        features_list.append(toArray(feature))
    else:
        features_list.append(feature)
        
#%% Regression to find knee axis
def main():
    # Percentage of data to validate
    p_test = 20 # %
    
    n_test = int(p_test/100.0 * len(legs))
    n_features = len(features_list)
    coeff, variance = [], []
    for feature in range(n_features):
        # Use 1 feature
        X = features_list[feature]
        
        # Split the data into training/testing sets
        X_train = X[:-n_test]
        X_test = X[-n_test:]
        
        # Split the targets into training/testing sets
        y_train = knee_axis[:-n_test]
        y_test = knee_axis[-n_test:]
        
        # Create linear regression object
        regr = linear_model.LinearRegression()
#        regr = linear_model.BayesianRidge()
#        regr = linear_model.HuberRegressor()	
        
        # Train the model using the training sets
        regr.fit(X_train, y_train)
        
        # Make predictions using the testing set
        y_pred = regr.predict(X_test)
        y_pred = [int(y_pred[i]) for i in range(len(y_pred))] # ID is an integer
        
         # The coefficients
        print('\nCoefficients:', regr.coef_)
        coeff.append(regr.coef_)
    
        # The mean squared error
        print("Mean squared error: %.2f" % mean_squared_error(y_test, y_pred))
        
        # Explained variance score: 1 is perfect prediction
        print('R2 score: %.2f' % r2_score(y_test, y_pred))
        variance.append(r2_score(y_test, y_pred))
        
        plt.scatter(y_test, y_pred)
        plt.title('y_pred vs y_test')
        plt.xlabel('y_test')
        plt.ylabel('y_pred')
        plt.show()
        
        err = []
        max_err = 50
        good = 0
        for i in range(n_test):
            diff = abs(legs[len(legs)-n_test+i].Points[y_test[i], 2] 
                     - legs[len(legs)-n_test+i].Points[y_pred[i], 2])
            err.append(diff)
            if diff < max_err :
                good = good + 1
        score = good/n_test
        
    #TODO save results in log file
    plt.plot(err)
    plt.title("Absolute Error")
    plt.xlabel("Leg")
    plt.ylabel("Error (mm)")
    plt.show()
    
    save_coeff(path_poly, file_name, regr)
    
    print("Percentage of legs with <", max_err, 
          "mm distance to real knee axis :", int(score*100), "%")
    print("Max distance :", np.max(err), "mm")
    
    # Show variance between several features    
    #plt.plot(variance)
    #plt.title("Variance (1 = perfect prediction)")
    #plt.show()

if __name__ == "__main__":
    main()    
        
