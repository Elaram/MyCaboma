# -*- coding: utf-8 -*-
"""
Created on Sat Oct 21 09:55:29 2017

@author: Alexandre Marcotte

"""
import numpy as np
import math
from skimage import draw
import cst
from importlib import reload
# Our functions
"Make sure that all my function can be reload so that every change is reload"
import display_leg, read_vtk, basic_function, create_3D_tensor, tensorflow_3D
reload(display_leg)
reload(read_vtk)
reload(basic_function)
reload(create_3D_tensor)
reload(tensorflow_3D)
"Import function that are used in each of these file"
# visualisation
from display_leg import show_knee
# read files
from read_vtk import fill_data
# statistic
from statistic import visualisation_statistic
# basic functions
from basic_function import Dist_point_line_3D, find_closest_pt, splitXY
# create_3D_tensor
from create_3D_tensor import zero_out_array, create_3D_matrix
# tensorflow_3D
from tensorflow_3D import link_data_and_onehot, train_neural_network


def find_axis_height(
        one_knee_data,
        pt_max_val,
        pt_min_val,
        max_R,
        RESIZE_FACT,
        knee_closest_pt,
        save_on=False,
        SHOW_CLOSEST_PT=False,
        knee_no=0,
        show_data_axis=False,
        data_axis=[]):
    """
    We know the point values but not the height of the knee interval this function
    extracts the heights
    """
    # Initialize the 3D matrice size based on the output of the zero_out_array
    # function -- # the +1 on the next line is necessary to conteract the rounding
    pt_min_x, pt_min_y, pt_min_z = pt_min_val
    pt_max_x, pt_max_y, pt_max_z = pt_max_val

    new_x = math.ceil((pt_max_x - pt_min_x + 2*max_R) / RESIZE_FACT)
    new_y = math.ceil((pt_max_y - pt_min_y + 2*max_R)/ RESIZE_FACT)
    new_height = math.ceil((pt_max_z - pt_min_z) / RESIZE_FACT) + 1

    # Convert the position coord. to 3D array
    # The coordinates of knee_3D are [height, x, y]
    j=0
    knee_axis_height = knee_closest_pt[knee_no]
    for pt_no, (pt_x, pt_y, pt_z) in enumerate(one_knee_data[cst.POINTS][cst.VAL]):
        x =      round((pt_x - pt_min_x + max_R)/RESIZE_FACT)
        y =      round((pt_y - pt_min_y + max_R)/RESIZE_FACT)
        height = round((pt_z - pt_min_z)/RESIZE_FACT)

        # If the poit corresponds to the area of the knee axis
        if j < len(knee_closest_pt[knee_no]):
            if knee_closest_pt[knee_no][j] == pt_no:
                knee_axis_height[j] = height
                j = j+1
    return knee_axis_height


def segment_knee(
        knee_2D,
        window_size,
        knee_axis_heights,
        knee_no,
        window_step):
    """
    Segment whole knee image into segments of window_size and increment by window
    step to convolve the whole leg
    """
    # Initialize values
    start = 0
    end = start + window_size
    window_max = len(knee_2D[knee_no])- window_size - window_step
    knee_windows = []
    knee_state = []
    # While the window has not reached its maximum position in 1 dimension
    while start < window_max:
        find = False
        segment = knee_2D[knee_no][start:end]
        knee_windows.append(segment)
        start = start+window_step
        end = start + window_size
        state = [1,0]
        # Check if the heights for the knee axis are present and label accordingly
        for i in range(0, len(knee_axis_heights[knee_no]) - 1):
            if (knee_axis_heights[knee_no][i] > start
                and knee_axis_heights[knee_no][i] < end
                and find == False):
                state = [0,1]
                find = True
        knee_state.append(state)
    return knee_windows, knee_state


def data_setup(
        data_type_1,
        data_type_1_labels,
        data_type_2,
        data_type_2_labels):
    """
    Setup the data so as to have a training and validation set for input and
    output
    """"
    x_y_val_D = []
    x_y_val_G = []
    for j in range(len(data_type_1)):
        for i in range(len(data_type_1[j])):
            x_y_val_D.append([data_type_1[j][i], data_type_1_labels[j][i]])
    for j in range(len(data_type_2)):
        for i in range(len(data_type_2[j])):
            x_y_val_G.append([data_type_2[j][i], data_type_2_labels[j][i]])
    x_y_val = x_y_val_D+x_y_val_G

    threshold = int((0.2*len(x_y_val)))
    train_data = x_y_val[:-threshold]
    test_data = x_y_val[-threshold:]

    # Then dissociate them
    train_x = splitXY(train_data, 0)
    train_y = splitXY(train_data, 1)

    test_x = splitXY(test_data, 0)
    test_y = splitXY(test_data, 1)

    return train_x, train_y, test_x, test_y


def main():
    # the path to the dataset of knees
    base_path = \
        r'C:\Users\alexa\Documents\CODING_A2017_POLY\Projet_4\Poly-AI\dataset_AI/'

    (data_1_D, data_2_D,
     data_1_G, data_2_G,
     data_axis_D, data_axis_G) = fill_data(base_path)
    # Make sure the leg are in the right direction and position
    data_2 = data_2_D + data_2_G
    data_1 = data_1_D + data_1_G
    data_axis = data_axis_D + data_axis_G

    all_kneeD_closest_pt, all_knee_closest_pt_intervall = find_closest_pt(data_2_D,
                                                                          data_axis)
    all_kneeG_closest_pt, all_knee_closest_pt_intervall = find_closest_pt(data_2_G,
                                                                          data_axis)

    "Make sure all the legs are in the same direction"
#    data_2_D = reverse_leg_if_required(data_2_D)
#    data_2_G = reverse_leg_if_required(data_2_G)

    "SHOW all legs"
    for knee_no in range(len(data_1)):
        print("knee_no", knee_no)
        print("number of points: ", len(data_1[knee_no][0][1]))
        show_knee(knee_no, data_2, all_knee_closest_pt, data_axis)


#    "Preprocessing"
#    train_x, train_y, test_x, test_y = link_data_and_onehot(data_2_D, data_2_G)
#    "Learning phase"
#    train_neural_network(x, train_x, train_y, test_x, test_y)
    return (data_2_D, data_2_G, all_kneeD_closest_pt, all_kneeG_closest_pt,
            all_knee_closest_pt_intervall, data_axis_D, data_axis_G)

(data_2_D, data_2_G, all_kneeD_closest_pt, all_kneeG_closest_pt,
 all_knee_closest_pt_intervall, data_axis_D, data_axis_G) = main()



import numpy as np
import matplotlib.pyplot as plt

pt_max_val, pt_min_val, max_R = zero_out_array(data_2_D+data_2_G)
all_knee_3D_D = []
all_knee_3D_G = []
all_knee_axis_height_D = []
all_knee_axis_height_G = []

# Convert the information to 2D images:
for knee_no in range(len(data_2_D)):
    "Right knee"
    new_knee_D = create_3D_matrix(data_2_D[knee_no], pt_max_val, pt_min_val, max_R,
                                  2, SHOW_CLOSEST_PT=False,
                                  knee_closest_pt=all_kneeD_closest_pt,
                                  knee_no=knee_no, show_data_axis=True,
                                  data_axis=data_axis_D)

    axis_kneeD_height = find_axis_height(data_2_D[knee_no], pt_max_val, pt_min_val,
                                         max_R, 2, all_kneeD_closest_pt,
                                         SHOW_CLOSEST_PT=False, knee_no=knee_no,
                                         show_data_axis=True, data_axis=data_axis_D)
    # knee_axis1 = np.sum(new_knee_D, axis=0)
    knee_axis2 = np.sum(new_knee_D, axis=1)
    knee_axis3 = np.sum(new_knee_D, axis=2)
    all_knee_3D_D.append(np.concatenate((knee_axis2, knee_axis3),axis=1))
    all_knee_axis_height_D.append(axis_kneeD_height)
    # all_knee_3D_D.append(knee_axis2)
for knee_no in range(len(data_2_G)):
    "Left Knee"
    new_knee_G  = create_3D_matrix(data_2_G[knee_no], pt_max_val, pt_min_val, max_R,
                                   2, SHOW_CLOSEST_PT=False,
                                  knee_closest_pt=all_kneeG_closest_pt,
                                  knee_no=knee_no, show_data_axis=True,
                                  data_axis=data_axis_G)

    axis_kneeG_height = find_axis_height(data_2_G[knee_no], pt_max_val, pt_min_val,
                                         max_R, 2, all_kneeG_closest_pt,
                                         SHOW_CLOSEST_PT=False, knee_no=knee_no,
                                         show_data_axis=True, data_axis=data_axis_D)

    # knee_axis1 = np.sum(new_knee_G, axis=0)
    knee_axis2 = np.sum(new_knee_G, axis=1)
    knee_axis3 = np.sum(new_knee_G, axis=2)
    all_knee_3D_G.append(np.concatenate((knee_axis2, knee_axis3),axis=1))
    all_knee_axis_height_G.append(axis_kneeG_height)
    # all_knee_3D_G.append(knee_axis2)

all_segmented_knee_3D_D = []
all_segmented_knee_3D_G = []
all_segmented_knee_3D_labels_D = []
all_segmented_knee_3D_labels_G = []
"Segment and prepare data for the CNN"
for knee_no in range(len(data_2_D)):
    (single_segmented_D,
     single_segmented_D_labels) = segment_knee(all_knee_3D_D, 120,
                                               all_knee_axis_height_D,
                                               knee_no,10)
    all_segmented_knee_3D_D.append(single_segmented_D)
    all_segmented_knee_3D_labels_D.append(single_segmented_D_labels)
for knee_no in range(len(data_2_G)):
    (single_segmented_G,
     single_segmented_G_labels) = segment_knee(all_knee_3D_D, 120,
                                               all_knee_axis_height_D, knee_no,10)
    all_segmented_knee_3D_G.append(single_segmented_G)
    all_segmented_knee_3D_labels_G.append(single_segmented_G_labels)

(train_x, train_y,
 test_x, test_y) = data_setup(all_segmented_knee_3D_D,
                              all_segmented_knee_3D_labels_D,
                              all_segmented_knee_3D_G,
                              all_segmented_knee_3D_labels_G)

# %%
import tensorflow as tf
from random import shuffle
import math

N_CLASSES = 2
BATCH_SIZE = 2100
SIZE_X = train_x[0].shape[0]
SIZE_Y = train_x[0].shape[1]
#y_pred = 0
LEARNING_RATE = 0.01
POOLING_SIZE = 4

S_X_AFTER_POOLING = math.ceil(math.ceil(SIZE_X/POOLING_SIZE)/POOLING_SIZE)
S_Y_AFTER_POOLING = math.ceil(math.ceil(SIZE_Y/POOLING_SIZE)/POOLING_SIZE)
NUM_FEATURES = 64
FINAL_SIZE = S_X_AFTER_POOLING * S_Y_AFTER_POOLING * NUM_FEATURES


x = tf.placeholder('float', shape=[None, SIZE_X, SIZE_Y] )
y = tf.placeholder('float', shape=[None, N_CLASSES]) # it's a one hot of [0,0]

KEEP_RATE = 0.9
keep_prob = tf.placeholder(tf.float32)

accuracy_list = []

def conv2d(x, W, stride=1):
    """ """
    return tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding='SAME')

# Pooling your are just trying to simplifie the image (diff de conv2 ou on veut
# extraire des features)
def maxpool2d(x, k=POOLING_SIZE): # k is the pooling_size
    """   """
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], # size of the window
                          strides=[1, k, k, 1], padding='SAME') # mvt of the window



def convolutional_neural_network(x):
    weights = {'wc1': tf.Variable(tf.random_normal([3, 3, 1, 32])),
               'wc2': tf.Variable(tf.random_normal([3, 3, 32, NUM_FEATURES])),
               'wfc': tf.Variable(tf.random_normal([FINAL_SIZE, 256])),
               'out': tf.Variable(tf.random_normal([256, N_CLASSES]))}

    biases = {'bc1': tf.Variable(tf.random_normal([32])),
              'bc2': tf.Variable(tf.random_normal([64])),
              'bfc': tf.Variable(tf.random_normal([256])),
              'out': tf.Variable(tf.random_normal([N_CLASSES]))}

    x = tf.reshape(x, shape=[-1, SIZE_X, SIZE_Y, 1])
    print(x.shape)

    conv1 = tf.nn.relu(conv2d(x, weights['wc1']) + biases['bc1'])
    print('conv1 before pooling: ', conv1.shape)
    conv1 = maxpool2d(conv1)
    print('conv1 afterPool', conv1.shape)

    conv2 = tf.nn.relu(conv2d(conv1, weights['wc2']) + biases['bc2'])
    print('conv2', conv2.shape)
    conv2 = maxpool2d(conv2)
    print('after max pool conv2', conv2.shape)

    fc = tf.reshape(conv2, [-1, FINAL_SIZE])
    fc = tf.nn.relu(tf.matmul(fc, weights['wfc']) + biases['bfc'])
    fc = tf.nn.dropout(fc, KEEP_RATE)

    output = tf.matmul(fc, weights['out']) + biases['out']
    return output


def train_neural_network(x):
    prediction = convolutional_neural_network(x)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction,
                                                                  labels=y))
    # TESTER d'autres fonction d'optimisation
    optimizer = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(cost)

    hm_epochs = 60

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        for epoch in range(hm_epochs):
            epoch_loss = 0
            i = 0
            print(i)
            if epoch % 2 == 0:
                if epoch != 0:
                    correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
                    accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
                    accuracy_val = accuracy.eval({x: test_x, y: test_y})
                    print('Accuracy:', accuracy_val)
                    accuracy_list.append(accuracy_val)

            while i < len(train_x):
                start = i
                end = i + BATCH_SIZE
                batch_x = np.array(train_x[start:end])
                batch_y = np.array(train_y[start:end])
#                 print(len(batch_x), len(batch_y))

                _, c = sess.run([optimizer, cost], feed_dict={x:batch_x, y:batch_y})
                epoch_loss += c

                i+=1;

            print('Epoch', epoch,
                  'completed out of', hm_epochs,
                  'loss:', epoch_loss)

    print(accuracy_list)

train_neural_network(x)
