# Merge request guidelines

1. Create a feature branch, branch away from master
2. Write code
3. If you have multiple commits please combine them into a few logically organized commits by squashing them
4. Submit a merge request (MR) to the master branch

# Merge request information

1. What does this MR do?
2. Are there points in the code the reviewer needs to double check?
3. Why was this MR needed?
4. What are the relevant issue numbers?
5. Screenshots (if relevant)