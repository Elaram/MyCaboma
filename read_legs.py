# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 22:14:00 2017

@author: Jonathan
"""
from importlib import reload
from basic_functions import find_dist_3D_btw_two_pts
import basic_functions
reload(basic_functions)

import math
import numpy as np
import os
from functools import reduce

from basic_functions import normalize_minus_one_to_one



#%% This class is a knee, it contains ID, POINTS (Coordinates), TURTOISITY,
# RADIUS, CURVUTURE, TANGENT NORMAL, BINORMAL, TORSION, Length, Type
class Knee:
    def __init__(self, iD, numPoints, points, length, tortuosity, radius, \
                 curvature, torsion, normal, tangent, binormal, kneeType, \
                 fileName, filePath, kneeAxisFile):

        self.ID = iD
        self.NumPoints = numPoints
        self.Points = points
        self.Length = length
        self.Tortuosity= tortuosity
        self.Radius= radius
        self.Curvature = curvature
        self.Torsion = torsion
        self.Tangent = tangent
        self.Normal= normal
        self.Binormal = binormal
        self.KneeType=kneeType
        self.FileName=fileName
        self.FilePath=filePath
        self.KneeAxisFile=kneeAxisFile

        self.MinX=self.getMinimum(self.Points[:,0])
        self.MinY=self.getMinimum(self.Points[:,1])
        self.MaxX=self.getMaximum(self.Points[:,0])
        self.MaxY=self.getMaximum(self.Points[:,1])

        self.MinRadius=self.getMinimum(self.Radius)
        self.MinCurvature=self.getMinimum(self.Curvature)
        self.MinTorsion=self.getMinimum(self.Torsion)

        self.MaxRadius=self.getMaximum(self.Radius)
        self.MaxCurvature=self.getMaximum(self.Curvature)
        self.MaxTorsion=self.getMaximum(self.Torsion)

        self.CenterOfMass=self.getCenterOfMass(self.Radius,self.Points,
                                               self.NumPoints)

        self.DerivativeRadius=self.derivative(self.Radius)
        self.DerivativeCurvature=self.derivative(self.Curvature)
        self.DerivativeTorsion=self.derivative(self.Torsion)

        self.DerivativeX=self.derivative(self.Points[:,0])
        self.DerivativeY=self.derivative(self.Points[:,1])
        self.DerivativeZ=self.derivative(self.Points[:,2])

        self.AverageX=self.getAverage(self.Points[:,0])
        self.AverageY=self.getAverage(self.Points[:,1])
        self.AverageZ=self.getAverage(self.Points[:,2])

        self.ClosestPointToAverageZ=self.getClosestPoint(self.Points[:,2],
                                                         self.AverageZ,1)
        self.ClosestPointToCOMZ=self.getClosestPoint(self.Points[:,2],
                                                     self.CenterOfMass[2],1)

        self.AvXBelowAvZ=self.getAverage(self.Points[:self.ClosestPointToAverageZ,0])
        self.AvYBelowAvZ=self.getAverage(self.Points[:self.ClosestPointToAverageZ,1])

        self.AvXAboveAvZ=self.getAverage(self.Points[self.ClosestPointToAverageZ:,0])
        self.AvYAboveAvZ=self.getAverage(self.Points[self.ClosestPointToAverageZ:,1])

        self.COMBelowZ= \
            self.getCenterOfMass(self.Radius[:self.ClosestPointToCOMZ],
                                 self.Points[:self.ClosestPointToCOMZ,:],
                                 len(self.Radius[:self.ClosestPointToCOMZ]))
        self.COMAboveZ= \
            self.getCenterOfMass(self.Radius[self.ClosestPointToCOMZ:],
                                 self.Points[self.ClosestPointToCOMZ:,:],
                                 len(self.Radius[self.ClosestPointToCOMZ:]))

        self.DifferenceY=self.AvYAboveAvZ-self.AvYBelowAvZ
        self.DifferenceCOMY=self.COMAboveZ[1]-self.COMBelowZ[1]
        self.DifferenceMaxMinY=self.getDifferenceMaxMinY()

        self.pt_min_radius_under_knee_ball:'[x:float, y:float, z:float]'= \
            self.get_CL_id_under_knee_ball_w_min_diam()

        self.axis_predicted_id = 0


    def getDifferenceMaxMinY(self):
        return ((self.Points[self.MaxY[1],2]-self.Points[self.MinY[1],2])*np.sign(self.Points[self.MaxY[1],2]-self.CenterOfMass[2])*np.sign(self.Points[self.MinY[1],2]-self.CenterOfMass[2]))


    def getMinimum(self, array):
        return (np.min(array), np.argmin(array))


    def getMaximum(self,array):
        return (np.max(array), np.argmax(array))

    def getCenterOfMass(self, radius, points, numPoints):
        sumR = np.sum(radius**2)
        cm_x = np.sum(np.reshape(points[:,0],(numPoints,1))*radius**2) / sumR
        cm_y = np.sum(np.reshape(points[:,1],(numPoints,1))*radius**2) / sumR
        cm_z = np.sum(np.reshape(points[:,2],(numPoints,1))*radius**2) / sumR

        return (cm_x, cm_y, cm_z)

    #def """ MAX DERIVATIVE(S) """
    def derivative(self, array):
        delta_radius, norm_points = [], []

        [norm_points.append(np.linalg.norm(self.Points[i+1]-self.Points[i]))\
         for i in range(self.NumPoints-1)]

        # calcul des dérivées discrètes pour chaque rayon d'une jambe
        [delta_radius.append((array[i+1]-array[i])/norm_points[i])\
         for i in range(self.NumPoints-1)]

        # dérivées maximale et minimale et leur position
        min_deriv = np.min(delta_radius)
        idx_min_deriv = np.argmin(delta_radius)

        max_deriv = np.max(delta_radius)
        idx_max_deriv =np.argmax(delta_radius)

        return min_deriv, idx_min_deriv, max_deriv, idx_max_deriv

    def getClosestPoint(
            self,
            array,
            point,
            dim):
        """
        Find the closest point from the knee axis to the centerline.

        Arguments
        ---------
        array :
        points :
        dim :
            dimension on which you want to find the closest point
        Returns
        -------
        index : int
            Index of the closest points on the centerline from the knee axis in a
            define dimension.
        """
        if dim==1:
            index=np.argmin(((array-point)**2)**(1/2.0))
        elif dim>=1:
            index=np.argmin((np.sum((array-point)**2,axis=1))**(1/2.0))
        return (index)

    def find_closest_pt_to_axis_3D(
            self,
            knee_axis):
        """
        Find the point of the leg axis which is the closest to the knee axis

        Arguments
        ---------
	        knee_axis :
	            list of two points that define the axis position in 3D
        Returns
        -------
        closest_pt_id : int
            The vector id of this point with the smallest distance
        closest_pt : [x, y, z]
            The point on the centerline that is the closest to the axis of the knee
        closest_pt_dist : float
            The 3D distance between the closest_point on the centerline and on the
            corresponding knee_axis
        """
        closest_pt_dist = 0
        closest_pt_id = 0
        for pt_id, pt in enumerate(self.Points):
            dist = basic_functions.Dist_point_line_3D(pt=pt, start=knee_axis[0],
                                                     end=knee_axis[1], ite=10)
            if pt_id == 0: # assign a closest pt distance one the first loop
                           # so that the dist doesn't stay stuck at zero
                closest_pt_dist = dist
            elif dist < closest_pt_dist: # if we find a new closest pt
                closest_pt_dist = dist
                closest_pt_id = pt_id
                closest_pt = pt

        return closest_pt_id, closest_pt, closest_pt_dist

    def get_CL_id_under_knee_ball_w_min_diam(
            self):
        """
        Find the point on the centerline just at the base of the knee ball where
        the radius is the smallest

        Returns
        -------
        pt_min_radius : [x:float, y:float, z:float]
            It is the point under the knee ball at which the radius is minimal, in
            other words its the intersection between the knee ball and the lower leg
        """
        # TODO: (ALEX) Il serait peut-etre plus efficace de prédire la position de l'axe du genou
        # avec les autres informations puis a partir de ce point descendre et monter jusqu'a trouver
        # la valeur minimal de rayon (CREUX) puis a l'ade de cette valeur rafaire la classification
        # de la position de l'axis
        CL = self.Points
        COM_below_z_id, _ = \
            basic_functions.find_CL_id_closest_to_random_3D_pt(CL, self.COMBelowZ)
        COM_above_z_id, _ = \
            basic_functions.find_CL_id_closest_to_random_3D_pt(CL, self.COMAboveZ)

        for pt_no, radius in enumerate(self.Radius):
            if  COM_below_z_id < pt_no < COM_above_z_id-20:                          # TODO: (ALEX) - Bellow and above semble inversé par rapport a leur définition
                # for the first time:
                # assign the min radius as the first radius encountered
                if 'min_radius' not in locals():
                    min_radius = radius
                    pt_min_radius = pt_no
                else:
                    # if we find an other min radius keep it and its id
                    if radius < min_radius:
                        min_radius = radius
                        pt_min_radius = pt_no

        return pt_min_radius

    def find_closest_pt_intervall(self):                                             #TODO: (ALEX) - This function come from the old file leg.py, so I need to make it work in this one
        """
        Find the values that are include in the acceptable 1cm intervall
        """
        # Therefore 0.5cm up and 0.5cm down
        self.closest_pt_intervall = [self.closest_pt_id]
        if self.closest_pt_id != 0:
            # up
            dist_up = 0
            closest_up = self.closest_pt_id
            while (dist_up <= 5):
                pt = self.POINTS[closest_up]
                closest_up += 1
                next_pt = self.POINTS[closest_up]
                dist_up += np.linalg.norm(np.array(pt) - np.array(next_pt))
                if dist_up <= 5:
                    self.closest_pt_intervall.append(closest_up)
            # down
            dist_down = 0
            closest_down = self.closest_pt_id
            while (dist_down <= 5):
                pt = self.POINTS[closest_down]
                closest_down -= 1
                next_pt = self.POINTS[closest_down]
                dist_down += np.linalg.norm(np.array(pt) - np.array(next_pt))
                if dist_up <= 5:
                    self.closest_pt_intervall.append(closest_down)

    def __len__(self):
        """Return the rounded length of the leg in mm based on the distance
        between all pts in the centerline"""
        leg_len = 0
        for pt, next_pt in zip(self.Points, self.Points[1:]):
            leg_len += find_dist_3D_btw_two_pts(pt, next_pt)
        return int(leg_len)


    def getAverage(self,array):
        return np.average(array)


    def getKneeAxis(self):
        self.KneeAxisPoints=readAxis(self.FilePath + self.KneeAxisFile)
        self.PtKneeAxis=self.getPointClosestToKneeAxis()


    def getPointClosestToKneeAxis(self):                                             # TODO: (ALEX) - Compare these values with the results of the method find_closest_pt_to_axis_3D above
        # find distance with cross product
        # distance=||(point-axispoint1)x(axisPoint2-axisPoint1)||/||axispoint2-axispoint1||
        return np.argmin((np.sum(((np.cross(self.Points-self.KneeAxisPoints[0,:],self.KneeAxisPoints[1,:]-self.KneeAxisPoints[0,:]))**2),axis=1))**(1/2.0)/((np.sum((self.KneeAxisPoints[1,:]-self.KneeAxisPoints[0,:])**2))*(1/2.0)))


    def get_tensor(self, do_normalize=False):
        """
        getTensor: Concatenate the knee caracteristics numpy array to
        arange them in a format that can be feed to a 1D neural network
        Return
        ------
        knee_tensor : numpy.array
        """

        knee_tensor = np.array([])
        if do_normalize:
            knee_tensor = np.concatenate((normalize_minus_one_to_one(self.Points),
                                          normalize_minus_one_to_one(self.Radius),
                                          normalize_minus_one_to_one(self.Curvature),
                                          normalize_minus_one_to_one(self.Torsion),
                                          normalize_minus_one_to_one(self.Normal),
                                          normalize_minus_one_to_one(self.Tangent),
                                          normalize_minus_one_to_one(self.Binormal)),
                                          axis=1)
        else:
            knee_tensor = np.concatenate((self.Points, self.Radius, self.Curvature,
                                          self.Torsion, self.Normal, self.Tangent,
                                          self.Binormal), axis=1)
        return knee_tensor


    def get_one_hot(self):
        j = 0
        onehot = np.zeros(self.NumPoints)
        for i in range(self.NumPoints):
            if j < len(self.closest_pt_intervall) and i == self.closest_pt_intervall[j] :
                onehot[i] = 1
                j = j+1
        return onehot

#%%
# function that takes a file, opens, reads and populates the object knee

def readKnee(file):
    # VARIABLES
    # file is the name of filename
    # knee is an object from class Knee that will be created and returned
    #----------------------------------------------------------------------------------------------------------
    # PARAMETERS EXTRACTED AND PASSED ON IN OBJECT KNEE
    iD=''
    points=[]
    numPoints=[]
    length=[]
    tortuosity=[]
    radius=[]
    curvature=[]
    torsion=[]
    tangent=[]
    normal=[]
    binormal=[]
    kneeType=''
    fileName=''
    filePath=''
    kneeAxisFile=''
    #--------------------------------------------------------------------------------------------------------
    #Lets find out of this file contains a left knee or right knee

    #First lets determine if this file has a path of from a Mac (/) or Windows (\)
    # we will use rfind function which allows to look for specific sub_string from the end of the string
    #string.rfind(string2) will return index of start of string 2 if found, and -1 if it did not found
    if file.rfind("\\")!=-1:
        #it is a windows computer, lets find the path and name
        filePath=file[:file.rfind("\\")+1]
        fileName=file[file.rfind("\\")+1:]


        #Lets determine if this file is Left or Right, we will just compare the charachter after the last \ to D,
        # or G, D=right G=left
        if file[file.rfind("\\")+1]=="D":
            # it is a right Leg, we write type as right
            kneeType='R'
        else:
            #it is a left leg, we write type as left
            kneeType='L'

    elif file.rfind("/")!=-1:
        #it is a mac computer, lets find the path and name
        filePath=file[:file.rfind("/")+1]
        fileName=file[file.rfind("/")+1:]

        #Lets determine if this file is Left or Right, we will just compare the charachter after the last / to D,
        # or G,D=right G=left
        if file[file.rfind("/")+1]=="D":
            # it is a right Leg, we write the type as right
            kneeType='R'
        else:
            #it is a left leg, we write the type as left
            kneeType='L'
    else:
        #We don't know which computer, but we now know the filename is in the same folder as this script
        fileName=file

        #Lets determine if this file is Left or Right, we will just compare the first character to D or G
        # D=right G=left
        if file[0]=="D":
            # it is a right Leg, we write the type as right
            kneeType='R'
        else:
            #it is a left leg, we write the type as Left
            kneeType='L'


    #lets just find the id, we will use the fileName to do so
    iD=fileName[:fileName.find('.')]

    #-----------------------------------------------------------------------------------------------------------
    #Lets determine if this file is a VTK or VTP or TIFF, this will affect the way we read thefile and write the new one
    #we will use rfind function, do determine if the file is a .VTK or .VTP
    if file.find(".vtk")!=-1:
        kneeAxisFile=iD+'.knee_axis.vtk'
        #-----------------------------------------------------------------------------------------------------------
        # We open the old file in read mode and the new file in write mode
        fKnee=open(file,"r")
        #-----------------------------------------VTK FILE------------------------------------------------------
        #----------------------------PATCH FOR WHEN POINTS ISN'T FIRST ARRAY---------------------------------
        # we will read the old file and look for the points, then close it and re open it
        for line in fKnee:
            if not line.find("POINTS")==-1:
                firstSpace=line.find(" ")
                secondSpace=line.find(" ",firstSpace+1)
                numPoints=int(line[firstSpace+1:secondSpace])
                #read rest of file
                fKnee.read()
        #close file
        fKnee.close()
        # open file for next step
        fKnee=open(file,"r")


        #we will read line by line untill end of file. we will be looking for specific substrings
        # the substring we will look for are "POINTS" , "length", "turtuosity", "Torsion", "Tangent","Normal","Binormal",
        #'radius', curvurture

        #we will use for line in fOld to read all the lines
        for line in fKnee:
            #------------------------COORDINATES, TANGENT, NORMAL, BINORMAL--------------------------------------------------------------
            if not line.find("POINTS")==-1 or not line.find("Tangent")==-1 or not line.find("Normal")==-1 or not line.find("Binormal")==-1:
                #this is the array format has 3 sets of points per line
                #lets save the line to identify the variable later
                tempLine=line
                tempData=[]
                for x in range(math.ceil(numPoints/3)):
                    tempStr=fKnee.readline()
                    #we put the add the line or points, we split when there are spaces, -2 because it spaces at
                    #end of line
                    tempData=tempData+[float(k) for k in tempStr[:-2].split(' ')]

                # we will reshape it now that we have all points (numPoints)*3
                # we need to identify which variable to assign it to

                if not tempLine.find("POINTS")==-1:
                    points=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Tangent")==-1:
                    tangent=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Normal")==-1:
                    normal=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Binormal")==-1:
                    binormal=np.reshape(tempData,(numPoints,3))

            #-------------------LENGTH--------------------------------------------------------------
            elif not line.find("Length")==-1:
                # we need to read the next line
                tempStr=fKnee.readline()
                #we add it to the variable
                length=float(tempStr[:-2])

            #----------------------TORTUOSITY--------------------------------------------------------------
            elif not line.find("Tortuosity")==-1:
                #we need to read the next line
                tempStr=fKnee.readline()
                #we add it to the variable
                tortuosity=float(tempStr[:-2])

            #----------------------RADIUS, CURVATURE------------------------------------------------------------------
            elif not line.find("Radius")==-1 or not line.find("Curvature")==-1 or not line.find("Torsion")==-1:
                #this array format has 9 points per line
                #lets save the line to identify the variable later
                tempLine=line
                tempData=[]
                for x in range(math.ceil(numPoints/9)):
                   tempStr=fKnee.readline()
                   #we put the add the line of points, we split when there are spaces, -2 because it spaces at
                   #end of line
                   tempData=tempData+[float(k) for k in tempStr[:-2].split(' ')]
                #we place in the variable
                if not tempLine.find("Radius")==-1:
                    radius=np.reshape(tempData,(numPoints,1))
                elif not tempLine.find("Curvature")==-1:
                    curvature=np.reshape(tempData,(numPoints,1))
                elif not tempLine.find("Torsion")==-1:
                    torsion=np.reshape(tempData,(numPoints,1))



        #We close the files because we are done
        fKnee.close()

    #---------------------------------------------------------------------------------------------------------
    elif file.find(".vtp")!=-1:
        kneeAxisFile=iD+'.knee_axis.vtp'
        #---------------------------VTP FILE------------------------------------------------------------------
        #-----------------------------------------------------------------------------------------------------------
        # We open the old file in read mode
        fKnee=open(file,"r")

        #----------------------------PATCH FOR WHEN POINTS ISN'T FIRST ARRAY---------------------------------
        # we will read the old file and look for the points, then close it and re open it
        for line in fKnee:
            if not line.find("NumberOfPoints")==-1:
                firstQuotation=line.find('"')
                secondQuotation=line.find('"',firstQuotation+1)
                numPoints=int(line[firstQuotation+1:secondQuotation])
                #read rest of file
                fKnee.read()
        #close file
        fKnee.close()
        # open file for next step
        fKnee=open(file,"r")



        #we will read line by line untill end of file. we will be looking for specific substrings
        # the substring we will look for are "POINTS" , "length", "turtuosity", "Torsion", "Tangent","Normal","Binormal",
        #'radius', curvurture

        for line in fKnee:
            #-----------------POINTS, TANGENT, NORMAL, BINORMAL----------------------------------------------
            if not line.find('Name="Points')==-1 or not line.find("Tangent")==-1 or not line.find("Normal")==-1 or not line.find("Binormal")==-1:
                #this is the array format has 2 sets of points per line
                #lets save the line to identify the variable later
                tempLine=line
                tempData=[]
                for x in range(math.ceil(numPoints/2)):
                    tempStr=fKnee.readline()
                    #we put the add the line or points, we split when there are spaces, -2 because it spaces at
                    #end of line
                    tempData=tempData+[float(k) for k in tempStr[10:].split(' ')]

                # we will reshape it now that we have all points (numPoints)*3
                # we need to identify which variable to assign it to
                if not tempLine.find('Name="Points')==-1:
                    points=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Tangent")==-1:
                    tangent=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Normal")==-1:
                    normal=np.reshape(tempData,(numPoints,3))
                elif not tempLine.find("Binormal")==-1:
                    binormal=np.reshape(tempData,(numPoints,3))
            #-------------------LENGTH--------------------------------------------------------------
            elif not line.find("Length")==-1:
                # we need to read the next line
                tempStr=fKnee.readline()
                #we add it to the variable
                length=float(tempStr[10:])
            #----------------------TORTUOSITY--------------------------------------------------------------
            elif not line.find("Tortuosity")==-1:
                #we need to read the next line
                tempStr=fKnee.readline()
                #we add it to the variable
                tortuosity=float(tempStr[10:])

            #----------------------RADIUS, CURVATURE------------------------------------------------------------------
            elif not line.find("Radius")==-1 or not line.find("Curvature")==-1 or not line.find("Torsion")==-1:
                #this array format has 6 points per line
                #lets save the line to identify the variable later
                tempLine=line
                tempData=[]
                for x in range(math.ceil(numPoints/6)):
                   tempStr=fKnee.readline()
                   #we put the add the line of points, we split when there are spaces, -2 because it spaces at
                   #end of line
                   tempData=tempData+[float(k) for k in tempStr[10:].split(' ')]
                #we place in the variable
                if not tempLine.find("Radius")==-1:
                    radius=np.reshape(tempData,(numPoints,1))
                elif not tempLine.find("Curvature")==-1:
                    curvature=np.reshape(tempData,(numPoints,1))
                elif not tempLine.find("Torsion")==-1:
                    torsion=np.reshape(tempData,(numPoints,1))



        #We close the files because we are done
        fKnee.close()
    if points[0,2]-points[numPoints-1,2]<0:
        #we flip the data
        points=np.flip(points,0)
        radius=np.flip(radius,0)
        curvature=np.flip(curvature,0)
        torsion=np.flip(torsion,0)
        normal=np.flip(normal,0)
        tangent=np.flip(tangent,0)
        binormal=np.flip(binormal,0)
    # we return the knee that we will create
    return Knee(iD, numPoints, points, length, tortuosity, radius, curvature,
                torsion, normal, tangent, binormal, kneeType, fileName, filePath,
                kneeAxisFile)
#%%-------------------------------------------------------------------------------------------
def readAxis(file):
    #check if file exists
    if os.path.isfile(file): # file exists
        #find out if vtk or vtp
        if 'vtk' in file:
            fAxis=open(file,'r')
            for line in fAxis:
                if "POINTS" in line:
                    tempStr=fAxis.readline()
                    axisPoints=[float(k) for k in tempStr[:-2].split(' ')]
                    axisPoints=np.reshape(axisPoints,(2,3))
            fAxis.close()
            return axisPoints

        elif 'vtp' in file:
            fAxis=open(file,'r')
            for line in fAxis:
                if 'Name=Points'in line:
                    tempStr=fAxis.readline()
                    axisPoints=[float(k) for k in tempStr[10:].split(' ')]
                    axisPoints=np.reshape(axisPoints,(2,3))
        return axisPoints

    else:
        return 'error'

#%%
def extractKnees(folder):
    #LETS FIND OUT IF MAC OR WINDOWS
    files=os.listdir(folder)
    knees=[]
    print(folder)
    if "\\" in folder:
        #it is a windows computer
        isWindows=True
        print(1)
    elif "/" in folder:
        isWindows=False
    for i, file in enumerate(files):
        if ".vtk" in file or ".vtp" in file:
            if not 'knee_axis'in file and not '.ini' in file and not '.log' in file and not '.pkl' in file : #ideally we would just have if 'centerline' in file
                file_path=''
                if isWindows:
                    file_path=folder+'\\'+file
                else:
                    file_path=folder+'/'+file
                print('reading file: '+ file_path)
                knee=readKnee(file_path)
                knees.append(knee)
    return knees

#%%
def getKneeAxis(knees):
    for i in range(len(knees)):
        print('getting knee axis for : ' + knees[i].ID)
        knees[i].getKneeAxis()
    return knees



#%%
def read_training_data(
        path=None):
    """
    Read the knee information with the the CLEAN reader.

    Parameter
    ---------
    path : str
        The computer file path of the knee dataset
    NOTE:
    ----
    - Only one of the two file paths needs to be enter in the parameter list
    - Only one file can be in the variable 'path_one_CL'

    Returns
    -------
    knees : list
        A list of all the knee object containing their info read from the files
    knees_axis : list
        A list of all the knee axis object containing their info read from the files
    """
    knee_files = os.listdir(path)
    # Sort files in a natural alphabetical and numerical way:
    knee_files.sort(key=basic_functions.natural_keys)
    file_path = path

    knees = []
    knees_axis = []
    for file in knee_files:     # ???: TEST [:100*3]
        # Read a knee file
        if "knee_axis" not in file:
            if ".stl_2." in file or ".aop_2." in file \
            or ".obj_2." in file or ".ply_2." in file:
                print(os.path.join(file_path, file))
                knees.append(readKnee(file_path + file))

        elif "knee_axis" in file:
            knees_axis.append(readAxis(file_path + file))

    return knees, knees_axis