# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 19:47:16 2017

@author: Jonathan
"""
import os
import shutil

def transferFiles(directories, final_directory, bool_Right, bool_Left):
    if not os.path.exists(final_directory):
        os.mkdir(final_directory)
  
    for i in range(len(directories)):
        files=os.listdir(directories[i])
        for j, file in enumerate(files):
            if bool_Right:
                if file[0]=='D' or file[0]=='R':
                    shutil.copy(directories[i]+file, final_directory)
            elif bool_Left:
                if file[0]=='G' or file[0]=='L':
                    shutil.copy(directories[i]+file, final_directory)


#%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MAIN~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                       
#directories=["\\\\QNAP-SERVER001\\Home Net Drive\\Jonathan\\School\\University\\Semester7\\Projet4\\Data\\Data_regression\\Clean\\", \
#             "\\\\QNAP-SERVER001\\Home Net Drive\\Jonathan\\School\\University\\Semester7\\Projet4\\Data\\Data_regression\\Reponse\\", \
#             "\\\\QNAP-SERVER001\\Home Net Drive\\Jonathan\\School\\University\\Semester7\\Projet4\\Data\\Data_regression\\Reponse\\Flipped\\", \
#             ]
def main():
    directories=["/Users/stephaniedolbec/Desktop/Clean/"]
    
    #final_directory="\\\\QNAP-SERVER001\\Home Net Drive\\Jonathan\\School\\University\\Semester7\\Projet4\\Data\\Data_regression\\Reponse\\Flipped\\FolderTest\\"           
    final_directory="/Users/stephaniedolbec/Desktop/Reponse/Flipped/"
    
    bool_Right=True
    bool_Left=False
    
    transferFiles(directories, final_directory, bool_Right, bool_Left)

if __name__ == "__main__":
    main()