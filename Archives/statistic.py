"STATISTIQUES "
import numpy as np
#  from matplotlib: from: https://matplotlib.org/examples/mplot3d/polys3d_demo.html
import matplotlib.pyplot as plt

"Show the leg radius distribution for all the legs"

def visualisation_statistic(data, all_knee_closest_pt):
#    RADIUS = 3
#    CURVATURE = 6
    TORSION = 7
    caract_visualize = TORSION
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for knee_no in range(30):
        knee_pt = np.arange(len(data[knee_no][caract_visualize][1]))
        knee_radius = data[knee_no][caract_visualize][1]
        
        # show the 6 point closest to the knee axis changing it to a radius 100
        for i in list(range(all_knee_closest_pt[knee_no]-3, all_knee_closest_pt[knee_no]+3)):
            knee_radius[i] = 0.4
            
        ax.bar(knee_pt, knee_radius, zs=knee_no, zdir='y', alpha=0.7)
        
    ax.set_xlabel('knee_pt')
    ax.set_ylabel('Knee_curvature')
    ax.set_zlabel('knee_no')
    
    plt.show()
    
def show_histogram(x, title):
    # Good to know : 
    x_mean = np.mean(x)
    x_std = np.std(x)
    print('Mean : ' + str(x_mean) + ' +/- ' + str(x_std))
    
    plt.figure()
    plt.hist(x, bins=50)
    plt.title(title)
    plt.show()
    
