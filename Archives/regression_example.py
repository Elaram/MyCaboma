#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 20:08:35 2017

@author: stephaniedolbec
"""

import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

# Load the diabetes dataset
diabetes = datasets.load_diabetes()

n_data = len(diabetes.data)
n_test = 20
n_features = 10
coeff, variance = [], []
for feature in range(n_features):
    # Use 1 feature
    diabetes_X = diabetes.data[:, np.newaxis, feature]
    
    # Split the data into training/testing sets
    diabetes_X_train = diabetes_X[:-n_test]
    diabetes_X_test = diabetes_X[-n_test:]
    
    # Split the targets into training/testing sets
    diabetes_y_train = diabetes.target[:-n_test]
    diabetes_y_test = diabetes.target[-n_test:]
    
    # Create linear regression object
    regr = linear_model.LinearRegression()
#    regr = linear_model.BayesianRidge()
#    regr = linear_model.HuberRegressor()	
    
    # Train the model using the training sets
    regr.fit(diabetes_X_train, diabetes_y_train)
    
    # Make predictions using the testing set
    diabetes_y_pred = regr.predict(diabetes_X_test)
    
    # The coefficients
    print('Coefficients: \n', regr.coef_)
    coeff.append(regr.coef_)

    # The mean squared error
    print("Mean squared error: %.2f"
          % mean_squared_error(diabetes_y_test, diabetes_y_pred))
    
    # Explained variance score: 1 is perfect prediction
    print('Variance score: %.2f' % r2_score(diabetes_y_test, diabetes_y_pred))
    variance.append(r2_score(diabetes_y_test, diabetes_y_pred))
    
#    # Plot outputs
#    plt.scatter(diabetes_X_test, diabetes_y_test,  color='black')
#    plt.plot(diabetes_X_test, diabetes_y_pred, color='blue', linewidth=1)
#    plt.xticks(())
#    plt.yticks(())
#    plt.show()
    
print("\n Résumé : ")
coeff = np.array(coeff)   
plt.plot(coeff)
plt.title("Coefficients")
plt.show()

plt.plot(variance)
plt.title("Variance (1 = prédiction parfaite)")
plt.show()

