#%%  Find the point of the leg axis which is the closest to the knee axis
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import sys
import random

from tensorflow.examples.tutorials.mnist import input_data

import tensorflow as tf              
import os
import numpy as np

"""A very simple MNIST classifier.
See extensive documentation at
https://www.tensorflow.org/get_started/mnist/beginners
"""

def find_maxpoint_number(data):
    max_size = 0
    for i in range(len(data)):
        if len(data[i][0][1]) > max_size:
            max_size = len(data[i][0][1])
    return max_size
        
def Data_IO_Create(Data):
    single_answer = [0] *2
    Input = [0] * (len(Data[1])+len(Data[3]))
    Output = [0] * (len(Data[1])+len(Data[3]))
    Ordered_Data = [0] * (len(Data[1])+len(Data[3]))
    index = random.sample(range(0, len(Data[1])+len(Data[3])), len(Data[1])+len(Data[3])+1)
    for i in range(0,(len(Data[1])+len(Data[3]))):
        if i < len(Data[1]):
            j = index[i]
            Ordered_Data[j] = Data[1][i][0]
            single_answer = [1,0]
            Output[j] = single_answer
        if i > len(Data[1]):
            j = index[i]
            Ordered_Data[j] = Data[3][i-len(Data[1])][0]
            single_answer = [0,1]
            Output[j] = single_answer
    return Ordered_Data
    max_size = 0
    for i in range((len(Data[1])+len(Data[3]))):
        print(i)
        if len(Ordered_Data[i][1]) > max_size:
            max_size = len(Ordered_Data[i][1])
#    for i in range(len(Ordered_Data)):
#        single_Data = [0] * (max_size *4)
#        for j in range(0, (len(Ordered_Data[i][0][1])*4)-1, 4):
#            ind = int(j/4)
#            single_Data[j] = Ordered_Data[i-1][0][1][ind][0]
#            single_Data[j+1] = Ordered_Data[i-1][0][1][ind][1]
#            single_Data[j+2] = Ordered_Data[i-1][0][1][ind][2]
#            single_Data[j+3] = Ordered_Data[i-1][3][1][ind]
#        Input[i-1]=single_Data
#    return [np.array(Input), np.array(Output)]
    
        
def Data_Answer(answer, data, maxpoint_number):
    data_KNEEID = [0] * len(answer)
    for i in range(len(answer)):
        single_Data = [0] * maxpoint_number
        single_Data[answer[i-1]] = 1.00000
        data_KNEEID[i-1]=single_Data
    a = np.array(np.transpose(data_KNEEID)).astype(np.float64)
    return a

def Data_Input(data, maxpoint_number):
    data_POINTS = [0] * len(data)
    for i in range(len(data)):
        single_Data = [0] * maxpoint_number * 4
        for j in range(0, (len(data[i-1][0][1])*4)-1, 4):
            ind = int(j/4)
            single_Data[j] = data[i-1][0][1][ind][0]
            single_Data[j+1] = data[i-1][0][1][ind][1]
            single_Data[j+2] = data[i-1][0][1][ind][2]
            single_Data[j+3] = data[i-1][3][1][ind]
        data_POINTS[i-1]=single_Data
    return np.array(data_POINTS)

#%%  Dee network
#path = r"C:\Users\Jonathan\Google Drive\Projet_GBM4900\04_RESSOURCES\DATA\dataset_AI"
path = r"C:\Users\alma\Desktop\files\POLY\trimestre 7\projet 4\dataset_AI"
    
data = Data_get(path)

a = Data_IO_Create(data)
#def main(_):
#  # Import data
#  data = Data_get(_)
#  data_1 = data[0]
#  all_knee_closest_pt = data[2]
#  
#  Input = Data_Input(data_1,max_pts)
#  Input = Input.astype(np.float32)
#  
#  Answer = Data_Answer(all_knee_closest_pt,Input,max_pts)
#  Answer = np.transpose(Answer.astype(np.float64))
#  
#  I_test = Input[151:264,]
#  A_test = Answer[151:264,]
#  # Create the model
#  x = tf.placeholder(tf.float32, [None, 2628])
#  W = tf.Variable(tf.zeros([2628, 657]))
#  b = tf.Variable(tf.zeros([657]))
#  y = tf.matmul(x, W) + b
#
#  # Define loss and optimizer
#  y_ = tf.placeholder(tf.float32, [None, 657])
#
#  # The raw formulation of cross-entropy,
#  #
#  #   tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(tf.nn.softmax(y)),
#  #                                 reduction_indices=[1]))
#  #
#  # can be numerically unstable.
#  #
#  # So here we use tf.nn.softmax_cross_entropy_with_logits on the raw
#  # outputs of 'y', and then average across the batch.
#  cross_entropy = tf.reduce_mean(
#      tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
#  train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
#
#  sess = tf.InteractiveSession()
#  tf.global_variables_initializer().run()
#  # Train
#  for i in range(1,15):
#      batch_xs = Input[(i*10)-1:(i*10)-1,]
#      batch_ys = Answer[(i*10)-1:(i*10)-1,]
#      sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
#
#  # Test trained model
#  correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
#  accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
#  print(sess.run(accuracy, feed_dict={x:I_test,
#                                      y_: A_test}))
#
#if __name__ == '__main__':
#  parser = argparse.ArgumentParser()
#  parser.add_argument('--data_dir', type=str, default='/tmp/tensorflow/mnist/input_data',
#                      help='Directory for storing input data')
#  FLAGS, unparsed = parser.parse_known_args()
#  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)