#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  9 19:49:20 2017

@author: stephaniedolbec
"""
import numpy as np
import matplotlib.pyplot as plt
import math
import os
import sklearn
from importlib import reload
import basic_function
import knee


#%% Data extraction
from knee import Knee, Knees, Knee_axis

base_path = "/Users/stephaniedolbec/Desktop/dataset_AI"
files = os.listdir(base_path) 
# Sort files in a natural alphabetical and numerical way: 
files.sort(key=basic_function.natural_keys)

all_knees = Knees()
all_knees_axis = []
# Read all the files (knee and knee_axis)
for i, file in enumerate(files):
    if "knee_axis" in file:
        knee_axis = Knee_axis()
        knee_axis.fill_data(file, base_path)
        all_knees_axis.append(knee_axis) 
    elif ".stl_2." in file or ".aop_2." in file or ".obj_2." in file or ".ply_2." in file:
        knee = Knee()
        knee.fill_data(file, base_path)
        all_knees.legs.append(knee)

all_knee_D = Knees()
all_knee_G = Knees()
for knee_no, knee in enumerate(all_knees.legs): 
    if all_knees_axis[knee_no].patient_no == knee.patient_no: # this knee axis correspond to this knee
        knee.axis = all_knees_axis[knee_no]        
        knee.find_closest_pt() 
    else : 
        print('****Warning there is a mismatch between the knee axis and the knee****')   #TODO verifier pourquoi dans la dernière boucle passe par ici
    knee.find_closest_pt_intervall() 
#        qui est apparue lors du changement de structure du code
    if knee.side == 'D':
        all_knee_D.legs.append(knee)
    elif knee.side == 'G': 
        all_knee_G.legs.append(knee)

#%% Features extraction
#%%        
""" MAX/MIN """      
for knee_no, knee in enumerate(all_knees.legs):
    #TODO associer les valeurs calculées aux jambes correspondantes (classe Knee)
    #TODO calculer min/max pour TortuosityArray, RadiusArray(?), CurvatureArray, TorsionArray
        min_value = min(knee.CurvatureArray)
        idx_max = knee.CurvatureArray.index(min_value)
        max_value = max(knee.CurvatureArray)
        idx_max = knee.CurvatureArray.index(max_value)

 #%%   
""" CENTER OF MASS """

def center_of_mass(points, radius):
    x, y, z = [], [], []
    radius = np.array(radius)
    Rtot = sum(radius)
    for i in range(len(points)):
        x.append(points[i][0])
        y.append(points[i][1])
        z.append(points[i][2])
    
    cm_x = sum(np.array(x)*radius) / Rtot
    cm_y = sum(np.array(y)*radius) / Rtot
    cm_z = sum(np.array(z)*radius) / Rtot
    
    return [cm_x, cm_y, cm_z]
    
    
for knee_no, knee in enumerate(all_knees.legs):
#TODO associer le CM à chaque jambe correspondante (classe Knee)
    if knee_no == 25:   # ex. pour une jambe seulement
        knee_points = np.array(knee.POINTS)
        knee_radius = np.array(knee.RadiusArray)
        half = int(len(knee.POINTS)/2)
        points_1 = knee.POINTS[:half]
        points_2 = knee.POINTS[half:]
        radius_1 = knee.RadiusArray[:half]
        radius_2 = knee.RadiusArray[half:]
        
        if len(points_1) is not len(radius_1) or len(points_2) is not len(radius_2): 
            print("Error: Mismatch between half vectors lengths")
            
        # Center of mass calculation : cm_i = sum(i*R)/sum(R)
        cm_1 = center_of_mass(points_1, radius_1)
        cm_2 = center_of_mass(points_2, radius_2)   
        cm_global = center_of_mass(knee_points, knee_radius)
#        print(cm_1, cm_2, cm_global)
        
#        Knee.show_knee(knee)


#%%
""" MAX RADIUS DERIVATIVE(S) """
def delta_radius(knee_points, knee_radius):
    delta_radius, norm_points = [], []
    
    [norm_points.append(np.linalg.norm(knee_points[i+1]-knee_points[i])) \
         for i in range(len(knee_points)-1)]
    
    # calcul des dérivées discrètes pour chaque rayon d'une jambe
    [delta_radius.append((knee_radius[i+1]-knee_radius[i])/norm_points[i])\
     for i in range(len(knee_points)-1)]

    # dérivées maximale et minimale et leur position
    min_deriv = min(delta_radius)
    idx_min_deriv = delta_radius.index(min_deriv)
    
    max_deriv = max(delta_radius)
    idx_max_deriv = delta_radius.index(max_deriv)
    
    return min_deriv, idx_min_deriv, max_deriv, idx_max_deriv
    

for knee_no, knee in enumerate(all_knees.legs):
#TODO associer chaque dérivée max/min à chaque jambe (classe Knee)
    if knee_no == 30 :   # ex. pour une jambe seulement
        knee_pts = np.array(knee.POINTS)
        knee_rds = np.array(knee.RadiusArray)
        min_d, idx_min_d, max_d, idx_max_d = delta_radius(knee_pts, knee_rds)
        
        print("length of knee_points vector:", len(knee_points))
        print("min :", min_d, "| index :", idx_min_d)
        print("max :", max_d, "| index :", idx_max_d, "\n")

        




