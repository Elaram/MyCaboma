# -*- coding: utf-8 -*-
"""
Created on Sat Oct  7 23:30:25 2017

@author: Jonathan
"""

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

# classe gaussienne avec 3 types de gaussiennes, isotropes, diagonale et full, réference au matrices de covariances

class gauss_mv: 

    def __init__(self,n_dims,cov_type="full"): # initialisation
        self.n_dims = n_dims
        self.mu = np.zeros((1,n_dims))
        self.cov_type = cov_type
        if cov_type=="isotropique":
            self.sigma_sq = 1.0
        elif cov_type=="diagonale":
            self.sigma_sq = np.ones(n_dims)
        elif cov_type=="full":
            self.cov = np.ones((n_dims,n_dims))

    # Pour un ensemble d'entrainement, la fonction devrait calculer l'estimateur par MV de la moyenne et de la matrice de covariance
    def train(self, train_data):
        self.mu = np.mean(train_data, axis=0)
        if self.cov_type == "isotropique":
            self.sigma_sq = np.sum((train_data - self.mu)**2.0) / (self.n_dims * train_data.shape[0])
        elif self.cov_type == "diagonale":
            self.sigma_sq =  np.sum((train_data - self.mu) ** 2.0, axis = 0) / train_data.shape[0] 
        elif self.cov_type == "full":
            self.cov = np.cov(np.transpose(train_data))
        pass
    
    # Retourne un vecteur de taille nb. ex. de test contenant les log
    # probabilités de chaque exemple de test sous le modèle.    
    def compute_predictions(self, test_data):

        if self.cov_type == "isotropique":
            # log(constante de normalisation)
            c = -self.n_dims * np.log(2*np.pi)/2.0 - self.n_dims*np.log(self.sigma_sq)/2.0
            # il faut calculer la valeur de la log-probabilite de chaque exemple
            # de test sous le modele determine par mu et sigma_sq. le vecteur
            # des probabilites est/sera log_prob
            log_prob = c - np.sum((test_data -  self.mu)**2.0,axis=1) / (2.0 * self.sigma_sq)
        elif self.cov_type == "diagonale":
            # on prend le produit du vecteur représentant la diagonale (np.prod(self.sigma)
            c = -self.n_dims * np.log(2*np.pi)/2.0 - np.log(np.prod(self.sigma_sq))/2.0
            # on somme sur l'axe 1 après avoir divisé par sigma puisque celui ci aussi est
            # de dimension d
            log_prob = c - np.sum((test_data -  self.mu)**2.0/ (2.0 * self.sigma_sq),axis=1)
        elif self.cov_type == "full":
            c = -self.n_dims * np.log(2.0*np.pi)/2.0
            det = np.linalg.det(self.cov)
            c += np.log(det)/2.0
            
            dmu = test_data-self.mu
            inv = np.linalg.inv(self.cov)
            
            dxs = np.dot(dmu,inv)
            dxsx = np.sum(dxs*dmu,axis=1)
            log_prob = c - dxsx
        return log_prob
    
    
    
# pour faire taire les underflow
np.seterr(under='ignore')
# classificateur de bayes
class classif_bayes:

    def __init__(self,modeles_mv, priors):
        self.modeles_mv = modeles_mv
        self.priors = priors
        if len(self.modeles_mv) != len(self.priors):
            print ('Le nombre de modeles MV doit etre egale au nombre de priors!')
        
        self.n_classes = len(self.modeles_mv)
                                                            
    # Retourne une matrice de taille nb. ex. de test x nombre de classes contenant les log
    # probabilités de chaque exemple de test sous chaque modèle MV. 
    def compute_predictions(self, test_data, eval_by_group=False):
        log_pred = np.empty((test_data.shape[0],self.n_classes))

        for i in range(self.n_classes):
            # ici il va falloir utiliser modeles_mv[i] et priors pour remplir
            # chaque colonne de log_pred (c'est plus efficace de faire tout une
            # colonne a la fois)
            
            log_pred[:,i] = self.modeles_mv[i].compute_predictions(test_data) +  self.priors[i]

        return log_pred






##data processing
results=np.full((1,len(data_2)),2)
average_Point=[]; # tableau de point moyen en z 
directions_above_average=np.zeros((len(data_2),2)) #direction en x et y de la jambe au dessus du point moyen
directions_below_average=np.zeros((len(data_2),2)) #direction en x et y de la jambe en dessous du point moyen 
average_below=np.zeros((len(data_2),2)) #moyenne des coordonnes x et y en dessous du point moyen
average_above=np.zeros((len(data_2),2)) #moyenne des coordonnes x et y au dessus de point moyen
average=np.zeros((len(data_2),3)) # moyenne coordonnes x,y z
torsion=np.zeros((len(data_2),3)) # torsion des genous au point moyene

for j in range(0,len(data_2)): # boucle for pour chaque genous
    x=np.empty(len(data_2[j][0][1])) #coordonnes x
    y=np.empty(len(data_2[j][0][1])) #coordonnes y
    z=np.empty(len(data_2[j][0][1])) #coordonnes z
   
    for i in range(0,len(data_2[j][0][1])):
        x[i]=data_2[j][0][1][i][0] #affection coordonnes x
        y[i]=data_2[j][0][1][i][1] #affection coordonnes y
        z[i]=data_2[j][0][1][i][2] #affection coordonnes z

    direction_correction=1; #si les points commence par le haut =1, si commence par le bas =-1
    average[j,:]=[sum(x)/len(data_2[j][0][1]),sum(y)/len(data_2[j][0][1]),sum(z)/len(data_2[j][0][1])] # moyennes des coordonnés
    direction=np.empty((len(data_2[j][0][1])-1,3)) # matrice de directions de chaque points. différence entre le chaque point consécutif, 1 si positif, -1 si negatif
    for i in range(0,len(data_2[j][0][1])-1):
        if x[i+1]-x[i]<0:
            direction[i,0]=-1
        else:
            direction[i,0]=1
        if y[i+1]-y[i]<0:
            direction[i,1]=-1
        else:
            direction[i,1]=1
        if z[i+1]-z[i]<0:
            direction[i,2]=-1
        else:
            direction[i,2]=1
        if z[i]>average[j,2] and z[i+1]<average[j,2]: #ici, on cherche le point qui correspond a la moyenne
            average_Point.append(i+1)
            torsion[j,0]=np.average(data_2[j][7][1][:average_Point[j]]) #moyenne des torsions au dessus du point moyen
            torsion[j,2]=np.average(data_2[j][7][1][average_Point[j]:]) #moyenne des torsions en dessous du point moyen
        elif z[i]<average[j,2] and z[i+1]>average[j,2]: #ici on fait meme chose mais on apprend qu'on les point sont de bas en haut
             average_Point.append(i)
             torsion[j,0]=np.average(data_2[j][7][1][average_Point[j]:]) #torsion au dessus du point moyen
             torsion[j,2]=np.average(data_2[j][7][1][:average_Point[j]]) #torsion en dessous du point moyen
             direction_correction=-1 # correction direction pcq les points commence du bas
    
    # calcule des directions moyennes
    if direction_correction==1: #reading from top
        directions_above_average[j,:]=[sum(direction[:average_Point[j],0])*direction_correction,sum(direction[:average_Point[j],1])*direction_correction]
        directions_below_average[j,:]=[sum(direction[average_Point[j]:,0])*direction_correction,sum(direction[average_Point[j]:,1])*direction_correction]
        average_above[j,:]=[np.average(x[:average_Point[j]]),np.average(y[:average_Point[j]])]
        average_below[j,:]=[np.average(x[average_Point[j]:]),np.average(y[average_Point[j]:])]
    else:
        directions_above_average[j,:]=[sum(direction[average_Point[j]:,0])*direction_correction,sum(direction[average_Point[j]:,1])*direction_correction]
        directions_below_average[j,:]=[sum(direction[:average_Point[j],0])*direction_correction,sum(direction[:average_Point[j],1])*direction_correction]
        average_above[j,:]=[np.average(x[average_Point[j]:]),np.average(y[average_Point[j]:])]
        average_below[j,:]=[np.average(x[:average_Point[j]]),np.average(y[:average_Point[j]])]
    torsion[j,1]=data_2[j][7][1][average_Point[j]] #torsion au point moyen
    

diff1=average_above-average[:,1:2] #difference en x et y, de moyenne au dessus point moyen et de toute la jambe
diff2=average[:,1:2]-average_below #difference en x et y de la moyenne de la jambe et de la moyenne en dessous du point moyen
diff3=average_above-average_below #difference en x et y de au dessus et en dessous du point moyen
diff4=average[:,1:2]-diff3 # diff en x et y entre moyenne de la jambe et diff 3

test_points=np.concatenate((diff1,diff2,diff3,diff4,torsion[:,[0,2]]),axis=1) # mets tous les features ensemble
#test_points=test_points[:,[0]] # isotropique 174, 90, %34
#test_points=test_points[:,[1]] # isotropique 236,28, % 10
#test_points=test_points[:,[3]] # isotropique 235,29, % 11
#test_points=test_points[:,[5]] # isotropique 235,28, % 10
#test_points=test_points[:,[1,3]] # isotropique 235,28, % 10
#test_points=test_points[:,[1,5]] # isotropique 235,29, % 11
#test_points=test_points[:,[1,8]] # isotropique 235,29, % 11
#test_points=test_points[:,[1,9]] # isotropique 235,29, % 11
#test_points=test_points[:,[3,5]] # isotropique 235,29, % 11
#test_points=test_points[:,[3,8]] # isotropique 236,28, % 10
#test_points=test_points[:,[3,9]] # isotropique 236,28, % 10
#test_points=test_points[:,[5,8]] # isotropique 235,29, % 11
#test_points=test_points[:,[5,9]] # isotropique 236,28, % 10
#test_points=test_points[:,[1,3,5]] # isotropique 235,29, % 11
#test_points=test_points[:,[1,3,8]] # isotropique 235,29, % 11
#test_points=test_points[:,[1,3,9]] # isotropique 235,29, % 11
#test_points=test_points[:,[1,5,8]] # isotropique 235,29, % 11
#test_points=test_points[:,[1,5,9]] # isotropique 235,29, % 11
#test_points=test_points[:,[3,5,8]] # isotropique 236,28, % 10
#test_points=test_points[:,[3,5,9]] # isotropique 236,28, % 10
#test_points=test_points[:,[3,8,9]] # isotropique 236,28, % 10
#test_points=test_points[:,[5,8,9]] # isotropique 235,29, % 11
#test_points=test_points[:,[1,3,5,8]] # isotropique 236,28, % 10
#test_points=test_points[:,[1,3,5,9]] # isotropique 236,28, % 10
#test_points=test_points[:,[1,3,8,9]] # isotropique 236,28, % 10
#test_points=test_points[:,[1,5,8,9]] # isotropique 235,29, % 11
#test_points=test_points[:,[3,5,8,9]] # isotropique 236,28, % 10
#test_points=test_points[:,[1,3,5,8,9]] # isotropique 236,28, % 10


#test_points=test_points[:,[1,8]] # diagonale 240,24, % 9
#test_points=test_points[:,[1,9]] # diagonale 238,26, % 9.8
#test_points=test_points[:,[3,8]] # diagonale 240,24, % 9
#test_points=test_points[:,[3,9]] # diagonale 240,24, % 9
#test_points=test_points[:,[5,8]] # diagonale 239,25, % 9.5
#test_points=test_points[:,[5,9]] # diagonale 240,24, % 9
#test_points=test_points[:,[1,2,8]] # diagonale 242,22, % 8
#test_points=test_points[:,[1,8,9]] # diagonale 244,20, % 7.5
#test_points=test_points[:,[0,2,4,7,9]] # diagonale 242,22, % 8



#test_points=test_points[:,[3,7,8]] # full 244,20, % 7.5
#test_points=test_points[:,[0,1,7,8]] # full 244,20, % 7.5
#test_points=test_points[:,[0,1,7,9]] # full 245,19, % 7
#test_points=test_points[:,[0,3,7,8]] # full 245,19, % 7
#test_points=test_points[:,[0,5,7,8]] # full 245,19, % 7
#test_points=test_points[:,[0,5,7,9]] # full 245,19, % 7
#test_points=test_points[:,[1,2,6,8]] # full 246,18, % 6.8
#test_points=test_points[:,[2,3,6,8,9]] # full 246,18, % 6.8
test_points=test_points[:,[2,5,6,8,9]] # full 246,18, % 6.8 Meilleur results [diff 2(x), diff3(y),diff4(x),torsion(x,y)

# On cree un modele par classe (par maximum de vraissemblance)
model_classe1=gauss_mv(test_points.shape[1])
model_classe2=gauss_mv(test_points.shape[1])
model_classe1.train(test_points[:149,:]) #classe Droite
model_classe2.train(test_points[149:,:]) #classe Gauche

# On cree une liste de tous nos modeles
# On fait la meme chose pour les priors
# Les priors sont calcules ici de facon exact car on connait le nombre 
# de representants par classes.
modele_mv=[model_classe1,model_classe2] 
priors=[149/264,(1-149/264)]

# On cree notre classifieur avec notre liste de modeles gaussien et nos priors
classifieur=classif_bayes(modele_mv,priors)

#on peut maintenant calculer les logs-probabilites selon nos modeles
log_prob=classifieur.compute_predictions(test_points)

# on calcule le maximum par classe pour la classification
classesPred = log_prob.argmax(1)+1


#print ("Taux d'erreur %.2f%%") % ((1-(classesPred==LorR_2.mean())*100.0)
found=0;
error=0;
notFound=0;
for i in range(0,264):
    if classesPred[i]==LorR_2[i]+1:
        found=found+1
    else:
        error=error+1
print('Found: ',found,'\nError: ',error, '\nPercentage ',error/264*100)
