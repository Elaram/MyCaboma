# -*- coding: utf-8 -*-
"""
Created on Sat Oct  7 08:20:52 2017

@author: alma
"""

# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================


#%% Function:
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import sys
import random

from tensorflow.examples.tutorials.mnist import input_data

import tensorflow as tf              
import os
import numpy as np

def Data_get(path):
    def fill_caract_list(file_path, caract_list, data_array):
                point_added = 0
                point_to_add = 0
                category_no = 0
                add_value = False
                with open(file_path) as f:
                    # Read the vtk files line by line
                    for line_no, line in enumerate(f):  
                        # stop at the end of the category we specified
                        if category_no == len(caract_list): 
                            break
                        
                        line_split = line.split()
                        # Continue the execution on the next loop if there is a paragraph jump
                        if line_split == []:
                            continue
    
                        if line_split[0] == caract_list[category_no][0]:
                            if caract_list[category_no][0] == 'POINTS':
                                group_size = 3
                                point_to_add = float(line_split[1]) * group_size   # because by group 0f 3 coordinate
                            else: 
                                group_size = int(line_split[1])
                                point_to_add = float(line_split[2]) * group_size
                            # initialize variables
                            point_added = 0
                            add_value = True
                            continue  
                        
                        for number in line.split():
                            if add_value:
                                caract_list[category_no][1].append(float(number))
                                point_added += 1
                                # Add values until all value are added based on the 
                                # title tag of the category
                                if point_added == point_to_add: 
                                    add_value = False
                                    # Split the data for each category in the specified format
                                    if group_size > 1: 
                                        data = caract_list[category_no][1] 
                                        caract_list[category_no][1] = 
                                        [data[pos:pos+group_size] for pos in range(0, len(data), group_size)]
                                    category_no += 1  # Go to the next category
    
                    # Append the information for every patient one after the other
                    data_array.append(caract_list)

    files=os.listdir(path) 
    
    # Create to separate array to split the 
    data_axis=[]
    data_1_D=[]
    data_2_D=[]
    data_1_G=[]
    data_2_G=[]
    
    for i, file in enumerate(files):
        file_path = path + '\\' + file
        print(i, file)  
        # Create a variable base on the information format in the knee axis files
        if "knee_axis" in file:
            caract_list = [ ['Angle', []], ['Offset', []], ['Normal', []], ['Binormal', []], 
                           ['POINTS', []] ]
            fill_caract_list(file_path, caract_list, data_axis)
            
        else :
            caract_list = [ ['POINTS' ,[]] , ['LengthArray', []], ['TortuosityArray', []],
                       ['RadiusArray', []], ['EdgeArray', []], ['EdgePCoordArray', []],
                       ['CurvatureArray', []], ['TorsionArray', []],  ['TangentArray', []],
                       ['NormalArray', []], ['BinormalArray', []] ]
            
            # append the information read into the file to the the right type of array
            if ".stl." in file or ".aop." in file or ".obj." in file or ".ply." in file:
                if "D" in file:
                    fill_caract_list(file_path, caract_list, data_1_D)  
                    print (1, file)
                if "G" in file:
                    fill_caract_list(file_path, caract_list, data_1_G)  
                    print (1, file)
            if ".stl_2." in file or ".aop_2." in file or ".obj_2." in file or ".ply_2." in file:
                if "D" in file:
                    fill_caract_list(file_path, caract_list, data_2_D)  
                    print (1, file)
                if "G" in file:
                    fill_caract_list(file_path, caract_list, data_2_G)  
                    print (1, file)
    
    return [data_1_D, data_2_D,data_1_G, data_2_G]

#%%  Find the point of the leg axis which is the closest to the knee axis
"""A very simple MNIST classifier.
See extensive documentation at
https://www.tensorflow.org/get_started/mnist/beginners
"""

def Data_IO_Create(Data):
    single_answer = [0] *2
    Input = [0] * ((len(Data[0])+len(Data[2])))      #NN input vector
    Output = [0] * ((len(Data[0])+len(Data[2])))    #Answer values 0,1 for right knee and opposite for left
    position = [0] * ((len(Data[0])+len(Data[2])))  #Position values (x,y,z)
    radius = [0] * ((len(Data[0])+len(Data[2])))    #Radius values
    index = random.sample(range(0, len(Data[0])+len(Data[2])),len(Data[0])+len(Data[2])) #Random vector of indices to allow for random input of all knee vectors
    for i in range(0,(len(Data[0])+len(Data[2]))):  #Go through all the knees
        if i < (len(Data[0])-1):                    #Start with the right knees
            j = index[i]                            #Get index for the knee
            position[j] = Data[0][i][0][1]          #Get position and radius
            radius[j] = Data[0][i][3][1]
            single_answer = [1,0]                   #Give and set right kne identifier (onehot)
            Output[j] = single_answer
        if i >= (len(Data[1])-1):                   #Left knees identical to right
            j = index[i]
            position[j] = Data[2][i-len(Data[0])][0][1]
            radius[j] = Data[2][i-len(Data[0])][3][1]
            single_answer = [0,1]
            Output[j] = single_answer
    
    max_size = 0                                    #Get max knee size (points) for zero padding
    for i in range((len(Data[0])+len(Data[2]))):
        if len(position[i]) > max_size:
            max_size = len(position[i])
            
    for i in range(len(position)):                  #Make input vector for NN
        single_Data = [0] * (max_size *4)   
        for j in range(0, ((len(radius[i])-1)*4), 4):
            ind = int(j/4)
            single_Data[j] = position[i][ind][0]    #Use position and radius
            single_Data[j+1] = position[i][ind][1]
            single_Data[j+2] = position[i][ind][2]
            single_Data[j+3] = radius[i][ind]
        Input[i-1]=single_Data
    return [np.array(Input), np.array(np.transpose(Output))]
    

#%%  Dee network
#path = r"C:\Users\Jonathan\Google Drive\Projet_GBM4900\04_RESSOURCES\DATA\dataset_AI"
IO_created = Data_IO_Create(data)
def main(_):
  # Import data
  
  path = r"C:\Users\alma\Desktop\files\POLY\trimestre 7\projet 4\dataset_AI"
  data = Data_get(path)
  IO_created = Data_IO_Create(data)
  
  Input = IO_created[0]
  Input = Input.astype(np.float32)
  
  Answer = IO_created[1]
  Answer = np.transpose(Answer.astype(np.float64))
  
  I_test = Input[201:263,]
  A_test = Answer[201:263,]
  # Create the model
  x = tf.placeholder(tf.float32, [None, 2628])
  W = tf.Variable(tf.zeros([2628, 2]))
  b = tf.Variable(tf.zeros([2]))
  y = tf.matmul(x, W) + b

  # Define loss and optimizer
  y_ = tf.placeholder(tf.float32, [None, 2])

  # The raw formulation of cross-entropy,
  #
  #   tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(tf.nn.softmax(y)),
  #                                 reduction_indices=[1]))
  #
  # can be numerically unstable.
  #
  # So here we use tf.nn.softmax_cross_entropy_with_logits on the raw
  # outputs of 'y', and then average across the batch.
  cross_entropy = tf.reduce_mean(
      tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
  train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
  sess = tf.InteractiveSession()
  tf.global_variables_initializer().run()
  # Train
  for i in range(1,10):
      batch_xs = Input[((i-1)*10):(i*10)]
      batch_ys = Answer[((i-1)*10):(i*10)]
      sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
      
  # Test trained model
  correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
  accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
  print(sess.run(accuracy, feed_dict={x:I_test,
                                      y_: A_test}))

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--data_dir', type=str, default='/tmp/tensorflow/mnist/input_data',
                      help='Directory for storing input data')
  FLAGS, unparsed = parser.parse_known_args()
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)