# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 22:48:41 2017

@author: Jonathan
"""
from read_VTK_VTP import *
from flip_VTK_VTP_TIFF import *
from sklearn import neighbors
import matplotlib.pyplot as plt
import random
import numpy as np
from sklearn.externals import joblib

def trainModel(path, testModelFile, showClusters):
    #takes all files in path and creates model, files must be identified as left or right
    print('Loading training set')
    knees=extractKnees(path)
    #extract select fearures
    print('Extracting features')
    data=np.zeros((len(knees),3))
    for i in range(len(knees)):
        if knees[i].KneeType=='R':
            data[i,:]=[knees[i].DifferenceMaxMinY,knees[i].DifferenceCOMY,0]
        else:
            data[i,:]=[knees[i].DifferenceMaxMinY,knees[i].DifferenceCOMY,1]
    #show clusters
    
    
    
    if showClusters:
        print("Computing clusters")
        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        print("Clusters:")
        for i in range(np.shape(data)[0]):
            if data[i,2]:
                ax1.scatter(x[:4], y[:4], s=10, c='b', marker="s", label='Left legs')
            else:
                ax1.scatter(x[40:],y[40:], s=10, c='r', marker="o", label='Right legs')
        plt.legend(loc='upper left');
        plt.show()
    #prepare validation matrix
    print('Training model')
    index = [[i] for i in range(len(knees))]
    MaxTests=100 # 100 cross validations/ for small data (we can change this based on data set size)
    MaxNeighbours=100 # we can change this base on data set
    results=np.zeros((MaxTests, MaxNeighbours))
    percentTrain=0.80 #80% for training, 20% validation
    for t in range(MaxTests):
        random.shuffle(index) #randomize data set
        indexCut=int(len(knees)*percentTrain) #80% for training, 20% validation
        indTrain=np.array(index[:indexCut]) # indexes for trainig
        indTest=np.array(index[indexCut:]) #index for validation
        trainingSet = data[indTrain[:,0],:2] #training set
        trainingTarget = data[indTrain[:,0],2] #trainging target
        testSet=data[indTest[:,0],:2] #test set
        testTarget=data[indTest[:,0],2] #test target
        for n in range(MaxNeighbours): 
            if n:
                clf=neighbors.KNeighborsClassifier(n,weights='distance') #create model
                clf.fit(trainingSet,trainingTarget) #train
                predictions=clf.predict(testSet) #predict
                successRate=(len(predictions)-sum(abs(testTarget-predictions)))/len(predictions) #calculate succes rate
                results[t,n]=successRate #store in resluts matrix
    AverageR=np.average(results,axis=0) #calculate average
    N=np.argmax(AverageR) #find best knn
    #recreate model and keep
    clf=neighbors.KNeighborsClassifier(N,weights='distance')
    clf.fit(data[:,:2],data[:,2])
    joblib.dump(clf,testModelFile)
    print('Training complete')

def classifyKnees(path,testModelFile):
    print('Loading test set')
    knees=extractKnees(path)
    #extract select fearures
    print('Extracting features')
    data=np.zeros((len(knees),2))
    for i in range(len(knees)):
        if knees[i].KneeType=='R':
            data[i,:]=[knees[i].DifferenceMaxMinY,knees[i].DifferenceCOMY]
        else:
            data[i,:]=[knees[i].DifferenceMaxMinY,knees[i].DifferenceCOMY]
    #load model
    print('Loading model')
    clf=joblib.load(testModelFile)
    predictions=clf.predict(data) #predict
    results=[]
    for i in range(len(knees)):
        results.append([knees[i].FilePath+knees[i].FileName,knees[i].ID,str(predictions[i]),knees[i].FilePath+knees[i].KneeAxisFile])
    print('Prediction complete')
    return results

def analyzeResults(predictions,flipLeftKnees,flipRightKnees,logFile):
    print("Starting analysis of predictions")
    logF=open(logFile,'w')
    for i in range(len(predictions)):
        if float(predictions[i][2]):
            logF.write(predictions[i][1] + ' is a left knee\n')
            if flipLeftKnees:
                flipKnee(predictions[i][0])
                flipKnee(predictions[i][3])
                logF.write(predictions[i][1] + ' has been flipped\n')
        else:
            logF.write(predictions[i][1]+ ' is a right knee\n')
            if flipRightKnees:
                flipKnee(predictions[i][0])
                flipKnee(predictions[i][3])
                logF.write(predictions[i][1]+' has been flipped\n')
    logF.close()
    print('Analysis complete')