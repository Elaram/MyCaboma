# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 19:40:34 2017

@author: Jonathan
"""
from classifierFunctions import *
    
trainingSetFolder=r"\\QNAP-SERVER001\Home Net Drive\Jonathan\School\University\Semester7\Projet4\Data\Data_regression\Clean"
ModelFile=r"\\QNAP-SERVER001\Home Net Drive\Jonathan\School\University\Semester7\Projet4\Data\Data_regression\Clean\classifierLR.pkl"
testSetFolder=r"\\QNAP-SERVER001\Home Net Drive\Jonathan\School\University\Semester7\Projet4\Data\Data_regression\Reponse"
boolTrainModel=True
flipRightKnees=True
flipLeftKnees=True
logFile=r"\\QNAP-SERVER001\Home Net Drive\Jonathan\School\University\Semester7\Projet4\Data\Data_regression\Reponse\logFileClassifierLR.log"




if boolTrainModel:
    trainModel(trainingSetFolder,ModelFile)
predictions=classifyKnees(testSetFolder,ModelFile)
analyzeResults(predictions,flipLeftKnees,flipRightKnees,logFile)


