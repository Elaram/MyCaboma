# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 14:24:55 2017

@author: L.G
"""
#this code tests prediction with linear regression
#from : http://scikit-learn.org/stable/auto_examples/linear_model/plot_ols.html#sphx-glr-auto-examples-linear-model-plot-ols-py
#-----------------------------------------------------------------------------
print(__doc__)


# Code source: Jaques Grobler
# License: BSD 3 clause


import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

diabetes_xtrain = []
diabetes_xtest = []
# Load the diabetes dataset
diabetes = datasets.load_diabetes()


# Use 2  feature, look at size of diabetes_X, diabetes_X_train, diabetes_X_test
diabetes_X = diabetes.data[:, 2:4]
#diabetes_X2 = diabetes.data[:,np.newaxis, 3]

# Split the data into training/testing sets
diabetes_X_train = diabetes_X[:-20]
#diabetes_X2_train = diabetes_X2[:-20]
#diabetes_xtrain[:,0] = diabetes_X_train[][0]
#diabetes_xtrain[:,1] = diabetes_X2_train[][0]

diabetes_X_test = diabetes_X[-20:]
#diabetes_X2_test = diabetes_X2[-20:]
#diabetes_xtest[0] = diabetes_X_test[:,0]
#diabetes_xtest[1] = diabetes_X2_test[:,0]

# Split the targets into training/testing sets
diabetes_y_train = diabetes.target[:-20]
diabetes_y_test = diabetes.target[-20:]

# Create linear regression object
regr = linear_model.LinearRegression()

# Train the model using the training sets
regr.fit(diabetes_X_train,diabetes_y_train)

# Make predictions using the testing set
diabetes_y_pred = regr.predict(diabetes_X_test)

# The coefficients
print('Coefficients: \n', regr.coef_)
# The mean squared error
print("Mean squared error: %.2f"
      % mean_squared_error(diabetes_y_test, diabetes_y_pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % r2_score(diabetes_y_test, diabetes_y_pred))

# Plot outputs
plt.scatter(diabetes_X_test, diabetes_y_test,  color='black')
plt.plot(diabetes_X_test, diabetes_y_pred, color='blue', linewidth=3)

plt.xticks(())
plt.yticks(())

plt.show()