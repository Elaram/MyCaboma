# -*- coding: utf-8 -*-
"""
Created on Fri Dec 29 02:00:14 2017

@author: Alexandre Marcotte
"""
from importlib import reload
import read_legs
import basic_functions
reload(read_legs)
reload(basic_functions)
from basic_functions import normalize_minus_one_to_one

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


#%%
path = r"C:\Users\alexa\Documents\CODING_A2017_POLY\Projet_4\Poly-AI\dataset_AI/"
legs, legs_axis = read_legs.read_training_data(path)

#%%
all_closest_pt = [leg.find_closest_pt_to_axis_3D(leg_axis)[0]
              for leg, leg_axis in zip(legs, legs_axis)]

#%%
def create_features(SAMPLE_BEGIN=0, SAMPLE_END=100):
    all_pt_curvature = []
    all_pt_radius = []
    all_pt_torsion = []
    all_pt_normal = []
    all_pt_tangente = []
    smallest_rad_under_knee = []
    pt_class = []
    leg_num = []
    pt_num_array = []
    pt_num = 0

    for leg_no, (leg, closest_pt) in \
        enumerate(zip(legs[SAMPLE_BEGIN:SAMPLE_END],
                      all_closest_pt[SAMPLE_BEGIN:SAMPLE_END])):
        # Normalize:
        normalized_curvature = normalize_minus_one_to_one(leg.Curvature)
        normalized_radius = normalize_minus_one_to_one(leg.Radius)
        normalized_torsion = normalize_minus_one_to_one(leg.Torsion)

        # Select all the value for the curvatures for all points
        for pt_no, curvature in enumerate(normalized_curvature):
            all_pt_curvature.append(curvature[0])
            leg_num.append(leg_no)
            # Classify the points base on their position
            if pt_no <= closest_pt: # - 15:
                pt_class.append(0)
            elif pt_no > closest_pt: # + 15:
                pt_class.append(1)
#             else: # points in the center of the leg
#                 pt_class.append(2)

            pt_num_array.append(pt_no)
            pt_num += 1

        # Select all the value for the radius for all points
        for radius in normalized_radius:
            all_pt_radius.append(radius[0])
        # Select all the value for the torsion for all points
        for torsion in normalized_torsion:
            all_pt_torsion.append(torsion[0])
        # Select all the value for the normal for all points
        for normal in leg.Normal:
            all_pt_normal.append(normal)
        # Select all the value for the normal for all points
        for tangente in leg.Tangent:
            all_pt_tangente.append(tangente)
        # Find the distance with the pt_min_radius_under_knee_ball:
        for pt in leg.Points:
            smallest_rad_under_knee.append(
                basic_functions.find_dist_3D_btw_two_pts(
                pt, leg.Points[leg.pt_min_radius_under_knee_ball]))

    # Create a pandas dataframe (better for visualization)
    data = np.vstack((np.array(all_pt_curvature),
                      np.array(all_pt_radius),
                      np.array(all_pt_torsion),
                      np.array(all_pt_normal)[:, 0],
                      np.array(all_pt_normal)[:, 1],
                      np.array(all_pt_normal)[:, 2],
                      np.array(all_pt_tangente)[:, 0],
                      np.array(all_pt_tangente)[:, 1],
                      np.array(all_pt_tangente)[:, 2],
                      np.array(smallest_rad_under_knee),
                      np.array(pt_num_array),
                      np.array(pt_class)))

    all_pt_df = pd.DataFrame(data.T)

    return all_pt_df


# %%
train_test_split_pt = 213
all_pt_df = create_features(0, train_test_split_pt)

# %% Add a name to the colums of the data frame
df_cols_name = ['curvature',
                'radius',
                'torsion',
                'normal_x',
                'normal_y',
                'normal_z',
                'tangente_x',
                'tangente_y',
                'tangente_z',
                'smallest_rad_under_knee',
                'pt_num_array',

                'pt_class']

all_pt_df.columns = df_cols_name


# %% Create the features (X) and the test set (y)
# from the dataframe
selected_features = df_cols_name[:-1]
X = all_pt_df[selected_features]

y = all_pt_df['pt_class']


#%% KNN:
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors=40)
knn.fit(X, y)


#%% TEST the model on test set
SHOW_INDIV_CLASSIFICATION = True
delta_z_pred_to_target = []
dist_under_5_cm = 0
TOLERATED_INTERVALL = 2.5

for leg_no in range(train_test_split_pt, 263):
    print('LEG: ', leg_no)

    all_pt_df = create_features(leg_no, leg_no+1)
    # Add a name to the colums of the data frame
    df_cols_name = ['curvature',
                    'radius',
                    'torsion',
                    'normal_x',
                    'normal_y',
                    'normal_z',
                    'tangente_x',
                    'tangente_y',
                    'tangente_z',
                    'smallest_rad_under_knee',
                    'pt_num_array',

                    'pt_class']

    all_pt_df.columns = df_cols_name
    # Selecting the test and training values
    features_test = np.array(all_pt_df[selected_features])

    target_test = np.array(all_pt_df['pt_class'])

    ##### TODO: Trouver une moyen de ne pas avoir a convertir to np.array()

    # Initialize
    test_prediction = []
    pred_threshold = np.zeros(target_test.shape)
    print(target_test.shape)
    print(legs[leg_no].Points.shape)
    one_predicted = False
    first_target_encounter = False

    for pt_no, (feature, target) in enumerate(zip(features_test,
                                                  target_test)):
        # PRED value for each point
        pred = knn.predict([feature])
        #pred = logreg.predict([feature])
        #pred = svm_clf.predict([feature])
        #pred  = sdg_clf.predict([feature])

        test_prediction.append(pred[0])
        if pred == 1 and not one_predicted:
            one_predicted = True
            pred_threshold[pt_no:] = 0.97
            predicted_id = pt_no
        if target == 1 and not first_target_encounter:
            first_target_encounter = True
            target_id = pt_no

    # Z pos of predicted and target value
    z_pos_target = legs[leg_no].Points[target_id][2]
    z_pos_pred = legs[leg_no].Points[predicted_id][2]
    delta_z_pred_to_target.append((z_pos_target - z_pos_pred)/10)

    # Add the predicted ID to the leg class:
    legs[leg_no].axis_predicted_id = predicted_id

    if SHOW_INDIV_CLASSIFICATION:
        dist_btw_real_predicted = (z_pos_target - z_pos_pred)/10
        print('target_id: ', target_id, z_pos_target, '\n',
              'predictied_id: ', predicted_id, z_pos_pred, '\n',
              'id_diff: ', abs(target_id - predicted_id), '\n',
              'dist between real and predicted (z axis): ',
              dist_btw_real_predicted, 'cm')
        if abs(dist_btw_real_predicted) < TOLERATED_INTERVALL:
            dist_under_5_cm += 1

        fig, ax = plt.subplots()
        ax.plot(target_test, label='target_test')
        ax.plot(test_prediction, label='test_prediction')
        ax.plot(pred_threshold, label='threshold_array')
        legend = ax.legend(loc='lower right')
        plt.xlabel('id')
        plt.show()

num_of_test_legs = len(delta_z_pred_to_target)
xs = list(range(num_of_test_legs))
plt.scatter(xs, delta_z_pred_to_target)
# Tolerance intervall
print('\n\nTolerance intervall')
plt.plot(xs, TOLERATED_INTERVALL*np.ones(num_of_test_legs), color='r')
plt.plot(xs, -TOLERATED_INTERVALL*np.ones(num_of_test_legs), color='r')
plt.title('Distance error on test legs')
plt.xlabel('leg number')
plt.ylabel('Diference between real and pred axis (cm)')
plt.grid(which='both')

dist_z_sum = 0
for dist in delta_z_pred_to_target:
    dist_z_sum += abs(dist)
print('average distance: +-', dist_z_sum/num_of_test_legs, 'cm')

plt.show()

print(dist_under_5_cm * 2, f'% < {TOLERATED_INTERVALL} cm')