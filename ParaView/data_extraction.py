#%% Function: 
    
def fill_caract_list(file_path, caract_list, data_array):
            point_added = 0
            point_to_add = 0
            category_no = 0
            add_value = False
            with open(file_path) as f:
                # Read the vtk files line by line
                for line_no, line in enumerate(f):  
                    # stop at the end of the category we specified
                    if category_no == len(caract_list): 
                        break
                    
                    line_split = line.split()
                    # Continue the execution on the next loop if there is a paragraph jump
                    if line_split == []:
                        continue

                    if line_split[0] == caract_list[category_no][0]:
                        if caract_list[category_no][0] == 'POINTS':
                            group_size = 3
                            point_to_add = float(line_split[1]) * group_size   # because by group 0f 3 coordinate
                        else: 
                            group_size = int(line_split[1])
                            point_to_add = float(line_split[2]) * group_size
                        # initialize variables
                        point_added = 0
                        add_value = True
                        continue  
                    
                    for number in line.split():
                        if add_value:
                            caract_list[category_no][1].append(float(number))
                            point_added += 1
                            # Add values until all value are added based on the 
                            # title tag of the category
                            if point_added == point_to_add: 
                                add_value = False
                                # Split the data for each category in the specified format
                                if group_size > 1: 
                                    data = caract_list[category_no][1] 
                                    caract_list[category_no][1] = [data[pos:pos+group_size] for pos in range(0, len(data), group_size)]
                                category_no += 1  # Go to the next category

                # Append the information for every patient one after the other
                data_array.append(caract_list)

            
#%%

import os
path = r"C:\Users\Jonathan\Google Drive\Projet_GBM4900\04_RESSOURCES\DATA\dataset_AI"
#path = r"C:\Users\alexa\Google Drive\Poly_A2017\Projet4\dataset_AI"
#path = r"C:\Users\Alexandre\Google Drive\Poly_A2017\Projet4\dataset_AI"
#path = "/Users/stephaniedolbec/Desktop/dataset_AI"
#path = r"C:\Users\Mathilde\Google Drive\01_POLYTECHNIQUE\4eAnnee\GBM4900\Projet_GBM4900\04_RESSOURCES\DATA\dataset_AI"

files=os.listdir(path) 

# Create to separate array to split the 
data_axis=[]
data_1=[]
data_2=[]
LorR_2=[] ##left if 1
for i, file in enumerate(files):
    file_path = path + '\\' + file
    # file_path = path + '/' + file
    print(i, file)  
    # Create a variable base on the information format in the knee axis files
    if "knee_axis" in file:
        caract_list = [ ['Angle', []], ['Offset', []], ['Normal', []], ['Binormal', []], 
                       ['POINTS', []] ]
        fill_caract_list(file_path, caract_list, data_axis)
        
    else :
        caract_list = [ ['POINTS' ,[]] , ['LengthArray', []], ['TortuosityArray', []],
                   ['RadiusArray', []], ['EdgeArray', []], ['EdgePCoordArray', []],
                   ['CurvatureArray', []], ['TorsionArray', []],  ['TangentArray', []],
                   ['NormalArray', []], ['BinormalArray', []] ]
        
        # append the information read into the file to the the right type of array
        if ".stl." in file or ".aop." in file or ".obj." in file or ".ply." in file:
            fill_caract_list(file_path, caract_list, data_1)  
            print (1, file)
        if ".stl_2." in file or ".aop_2." in file or ".obj_2." in file or ".ply_2." in file:
            fill_caract_list(file_path, caract_list, data_2)
            if file[0]=='G':
                LorR_2.append(1)
            else:
                LorR_2.append(0)
            print(2, file)
            
#%% Make sure that the legs start at 0 from the foot (ie put the smallest region at 0)
def inverse_leg_if_required(data):
    for knee_no in range(len(data)):
        if data[knee_no][3][1][0] > data[knee_no][3][1][-1]:
            data[knee_no][3][1] =  list(reversed(data[knee_no][3][1]))
            
inverse_leg_if_required(data_1)
inverse_leg_if_required(data_2)
    
  
#%% Calcul the distance between a 3D line (knee_axis) and a point (points on the leg axis)
import numpy as np

def Dist_point_line_3D(pt, start, end, ite):  
    # Convert to array
    pt = np.array(pt)
    start = np.array(start)
    end = np.array(end)
    
    vect_line = end - start
    vect_line = vect_line/ite
    
    for i in range(ite + 1):
        pt_on_line = start + i*vect_line
        # Distance between two points
        dist = np.linalg.norm(pt_on_line-pt)
        # Initialize the minimum distance
        if i == 0: 
            min_dist = dist
        elif dist < min_dist:
            min_dist = dist
            #print(min_dist)
    
    return min_dist
                
    
#%%  Find the point of the leg axis which is the closest to the knee axis (FOR DATA_1 only)
all_knee_closest_pt = []    #ON DEVRAIT SUREMENT FAIRE DES CLASSES!!!

for knee_no in range(len(data_1)):
    leg_axis = data_1[knee_no][0][1]
    knee_axis_pt = data_axis[knee_no][4][1]
    
    closest_pt_ind = 0
    for i, point in enumerate(leg_axis):
        # Calcul the distance from the current point to the line
        dist = Dist_point_line_3D(point, knee_axis_pt[0], knee_axis_pt[1], 10)
        if i == 0: 
            closest_pt_dist = dist
        elif dist < closest_pt_dist:
            closest_pt_dist = dist
            closest_pt_ind = i
            
    all_knee_closest_pt.append(closest_pt_ind)
               
    print("knee_no: ", knee_no,
          "closest_pt_ind: ", closest_pt_ind, 
          "closest_pt_dist: ", closest_pt_dist)
    
        
#%% Show the knees in a matplotlib plot
    
def show_knee(knee_no):
        # INITIALIZATION: 
    fig = plt.figure()
#    ax = fig.add_subplot(111, projection='3D')
    ax = Axes3D(fig)
    
    ax.set_aspect('equal')
    
    # Take one caracteristic list from the complete caracteristic list array: 
    caract_list = data_1[knee_no]
    # Create the leg axis: 
    " To have an equal aspect ratio of the axis "    
    x_val = [item[0] for item in caract_list[0][1]]   
    y_val = [item[1] for item in caract_list[0][1]]
    z_val = [item[2] for item in caract_list[0][1]] 
    # Chose the axis with the biggest span: 
    min_val = min(z_val)
    max_val = max(z_val)
    ax.set_xlim(min_val, max_val)
    ax.set_ylim(min_val, max_val)
    ax.set_zlim(min_val, max_val) 
    
    radius_list = caract_list[3][1]
    ax.scatter(x_val, y_val, z_val, s=np.array(radius_list)*2)
        
    # Create the knee axis
    knee_axis = data_axis[knee_no]
    x_val = [item[0] for item in knee_axis[4][1]]
    y_val = [item[1] for item in knee_axis[4][1]]
    z_val = [item[2] for item in knee_axis[4][1]]
    
    ax.scatter(x_val, y_val, z_val)
    
    # Plot the point closest to the knee axis
    cp = all_knee_closest_pt[knee_no]
    ax.scatter(caract_list[0][1][cp][0], caract_list[0][1][cp][1],
               caract_list[0][1][cp][2], s=240)
    
    plt.show()
    
    
#%% PLOT THE KNEE 
#FROM THE EXTRACTED INFORMATION:
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

#initialization of the graphic showing the variation of the radius for all the legs
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

for knee_no in range(len(data_1)):
    print("knee_no", knee_no)
    print("number of points: ", len(data_1[knee_no][0][1]))
    print("closest pt: ",  all_knee_closest_pt[knee_no])
    
    show_knee(knee_no)
    
    
#%% Show the leg radius distribution for all the legs
#  from matplotlib: from: https://matplotlib.org/examples/mplot3d/polys3d_demo.html
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt



RADIUS = 3
CURVATURE = 6
TORSION = 7
caract_visualize = TORSION

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for knee_no in range(30):
    knee_pt = np.arange(len(data_1[knee_no][caract_visualize][1]))
    knee_radius = data_1[knee_no][caract_visualize][1]
    
    # show the 6 point closest to the knee axis changing it to a radius 100
    for i in list(range(all_knee_closest_pt[knee_no]-3, all_knee_closest_pt[knee_no]+3)):
        knee_radius[i] = 0.4
        
    ax.bar(knee_pt, knee_radius, zs=knee_no, zdir='y', alpha=0.7)
    
ax.set_xlabel('knee_pt')
ax.set_ylabel('Knee_curvature')
ax.set_zlabel('knee_no')

plt.show()


#%% Statistique
axis_pos_normalize = []
for knee_no in range(len(data_1)):
    axis_pos_normalize.append(all_knee_closest_pt[knee_no]/len(data_1[knee_no][0][1]))
 
    
    
    
    
